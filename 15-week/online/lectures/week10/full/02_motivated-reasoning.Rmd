---
title: "Motivated Reasoning"
author: "PSYC/PHIL 279, Winget"
output:
  xaringan::moon_reader:
    css: xaringan-themer.css
    lib_dir: libs
    nature:
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: false
---

```{r xaringan-themer, include=FALSE, warning=FALSE}
library(tidyverse)
library(xaringanthemer)

style_mono_dark(
  base_color = "#4C837A",
  text_color = "#E1DDBF",
  background_color = "#04263C",
  title_slide_text_color = "#FFFFFF",
  text_slide_number_color = "#04263C",
  colors = c(
  red = "#FF0000",
  blue = "#00FFFF",
  purple = "#FF00FF"
  ),
  extra_css = list(
    ".large-text" = list("font-size" = "150%"),
    ".small-text" = list("font-size" = "75%")
  )
)
```

#  Reasons for reasons 

+ Reasoning: actively and consciously considering past representations (premises) in the formation of new representations


+ How we reason
  + Dual process models


+ Why we reason
  + Make better decisions
  + To deal with novelty/anticipate the future
  + To allow us to explain and learn from others

???

+ People believe what they want to believe...especially when it comes to beliefs that have to do with the self
+ We are often unrealistically positive when it comes to assessments of our own abilities, traits, etc.
+ This idea isn't new...even confirmation bias, which we have talked about a lot in the last week is an example of “believing what we want to believe”
+ For example in that study with death penalty beliefs, people who were in favor of the death penalty were much more likely to believe that the articles they read either supported their beliefs or that the ones that didn't represented bad, poorly done science
+ But in order to understand Motivated Reasoning, its worth talking through First REASONING and then Motivation in turn
+ 
+ So let's start with Reasoning...what it is and why we do it
+ If inferences are where we form new mental representations on the basis of previously held representations (i.e. we infer whether a penguin is a bird based on our past representation of birds), then reasoning is a special kind of inference where you are actively and consciously considering your past representations (or premises) and your new representation
  + The premises are seen as providing REASONS to accept conclusions 
+ So let's think through how we reason and why we reason...
+ We've actually talked a lot about reasoning in this class, and how we make judgment, and much of that discussion has been framed around dual process models...which is, as we know, used to explain how we sometimes make fairly basic errors even in mathematical reasoning
+ Why? 
  + Well largely we reasoning improve knowledge and make better decision...yet we know we don't always make better decisions through the reasoning process, so another justification can be to deal with novelty, anticipate the future
  + And, many times, other people have information or know about things that we do not...so it helps us explain to and learn from others as well

---
#  Motivation vs. cognition 

+ Are cognitive explanations more parsimonious?


+ False comparison: Our self-serving biases likely result from both motivation and cognition

???

+ As we've discussed it, reasoning seems like a fairly cognitive process, but we are going to talk about the distinction between that and more motivated forms of reasoning
+ Your reading for today notes that in the debate between whether reasoning is motivated or purely cognitive, people who favor the cognitive side of that divide talk about that explanation as being more parsimonious
+ Basically they argue that since any apparent demonstration of a motivational bias can be explained solely in terms of dispassionate cognitive processes, we should not invoke motivational mechanisms to explain these phenomena
+ **Basically, your reading rightfully says that it depends on what we emphasize as our more primary system**...Given a model in which the motivational system is more fundamental, motivational explanations would be more parsimonious
+ Both work in tandem...you can imagine, for example, if it was only our motivation in the drivers seat, we would believe in wild fantasy completely divested from reality
+ But since we don't it is likely that our motivations influence how we cognitively process information relative to a given belief
+ **Basically, there are boundary conditions**...we can only drum up a motivated interpretation if there is any evidence that can be seen to support it
+ So reality constrains motivated reasoning, but not completely

---
#  Different goals drive reasoning 

<br>
.pull-left[
.center[**.purple[Accuracy Goals]**]
+ More cognitive effort on issue related reasoning
+ Attend to relevant info more carefully (and process more deeply)
+ Use more complex decision making rules
]
 
.pull-right[
.center[**.purple[Directional Goals]**]
+ Search memory for supporting beliefs/rules
+ Creatively combine knowledge to create new beliefs to support the position
+ Assess only a subset of things in memory
]

???

+ The motivated reasoning phenomena we are going to talk about fall into two major categories
  1. Those in which the motive is to arrive at an accurate conclusion, whatever it may be
  1. And those in which the motive is to arrive at a particular, directional conclusion
+ The 2 categories are often discussed in the same breath because they are both indicative of motivated reasoning...but it is important to distinguish between them because there is no reason to believe that both involve the same kinds of mechanism
+ Basically both kinds of goals affect reasoning by influencing the choice of beliefs and strategies applied to a given problem
+ Accuracy goals lead to the use of those beliefs and strategies that are considered most appropriate, whereas directional goals lead to the use of those that are considered most likely to yield the desired conclusion 
  + e.g. see examples of different motivated info processing on this slide

---
##  Motivation to maintain positive <br> self-concept

<br>
.center[
<iframe width="560" height="315" src="https://www.youtube.com/embed/X9FJiDFVoOo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
]

???

+ Motivation comes in because reasoning can serve to explain the traits or behaviors of ourselves or others in ways that maintain our own positive self concept and bolster or support our existing beliefs, as you can see from this clip from the Big Chill
+ Big chill clip
+ So let's talk through some examples of motivated reasoning

---
###  Better-than-average effect <br> (Alicke et al., 2001; Taylor & Brown, 1988)

+ People evaluate themselves more favorably relative to an average peer


.center[
.center[<img src="assets/img/image27.png" width=65%>]
<img src="assets/img/image28.png" height=325>
<img src="assets/img/image29.png" height=325>
]

???

+ As we've mentioned, some of the best-known findings with regard to motivated reasoning, have to do with how we think about the SELF
+ And one of the best examples of motivated reasoning with regard to the self is the better than average effect...also called the above average effect or “illusory superiority” or a number of other names
+ Research on this effect finds that:
  + The majority of the public believes that they are more attractive, more intelligent, and less biased than the average person...which is a statistical impossibility
  + We think we are more competent in handling money, better at driving etc. 
+ One of the first studies that investigated the effect of illusory superiority focused on a domain relevant to the scope of our study
+ This study was carried out in 1976 by the American College Board, and it involved attaching a survey to the SAT exams (taken by approximately one million students per year) that asked the student to rate themselves relative to the median of the sample (rather than the average peer) on a number of vague positive characteristics
+ In ratings of leadership ability, 70% of the students scored themselves above the median
+ In ability to get on well with others, 85% scored themselves above the median
+ You can see for example in this study (Alicke et al 1995), that in rating themselves relative to the average student on a large number of different traits, people are consistently rate themselves higher on positive traits and lower on negative traits relative to the average student
+ ERRORS at this are very important because getting it wrong means we don't seek out help or advice when we might actually need it

---
###  Better-than-average effect <br> (Alicke et al., 2001; Taylor & Brown, 1988)

+ People compare themselves to especially lower targets (Perloff & Fetzer , 1986)


+ People selectively consider contexts in which they do better than others (Weinstein, 1980)


+ Focalism


+ People employ a heuristic that aggregates self knowledge with idealized traits (Alicke et al., 2001)

???

+ All sorts of reasons for this effect
  + We might compare ourselfs to lower targets (e.g., )
  + Other reasons might be due to the fact that we're selectively considering situations we do better than others (e.g., )
  + We might even employ focalism (like we talked about during our unit on affective forecasting), and only focus on our own positive traits
+ But Alicke and his colleagues suggested this may be a heuristic where people essentially create some sort of average between their self-knowledge and ideal characteristics

---
###  Better-than-average effect <br> (Alicke et al., 2001; Taylor & Brown, 1988)

+ When is it reduced?
  + Dimensions that are NOT ambiguous/subjectively defined
  + When we have personal contact with the comparison target (Alicke et al., 1995)
  + When we ARE better than average (Dunning, 2011)
  + In other cultures (Western cultures show this effect more than Eastern cultures)
  + BTAE increases with positive, controllable traits and decreases with negative, uncontrollable traits


???

+ This effect emerges across domains...soccer referees, for example, regardless of their level, believe they are better than other referees
+ And it is strongest on ambiguous or subjectively defined dimensions...people are the most self serving when they have more latitude to construe comparisons in ways that emphasize their own superiority
+ Cultural differences...this is more true of people from western cultures...and differences in gender as well
  + Individualist vs. collectivist
+ BTAE increases with positive/controllable traits and decreases with negative/uncontrollable traits
+ This last point can be taken as evidence for the motivational nature of this phenomenon
  + This result shows that people are most self-aggrandizing when they feel responsible for their positive characteristics and least self-aggrandizing when they feel fate is responsible for their negative characteristics

---
##  Better-than-average effect <br> (Dunning, 2011) 

.center[<img src="assets/img/image22.png" width=100%>]

???

+ In one study, researchers gave participants an exam and asked them to estimate their performance
+ As you can see in this comparison of actual test scores and perceived test score, those who do the worst have the greatest bias...they think they did much better than their performance actually suggests
+ However, the top performers tended to have a more accurately calibrated sense of their performance

---
##  Above average at home <br> (Ross & Sicoly, 1979) 

+ 37 heterosexual married couples were recruited

.center[<img src="assets/img/image21.png" width=650>]

+ Results:
  + Overestimation occurred: Across couples, at least one or more member inflated their housework contributions
  + People provided more examples of their own contributions (vs. spouse)

???

+ Even though some of the boundary conditions suggest that close comparison targets should reduce the better and average effect, we see that it still sometimes persists when the person is the most close to you
+ In one study looking at egocentric biases among married couples in their evaluation of the housework
+ And in a questionnaire packet each member of the married couple addressed 20 different arenas of housework (e.g. dishes, making breakfast etc) and was given a response scale like this (which was a 150 mm line, that they had to put a slash through, like a likert scale, to denote who they thought was doing more of the work in their house on that task
+ And then later come up with examples of ways they or their spouse enacted those tasks (implicates the availability heuristic)
+ Results:
  + Overestimation occurred: Across couples, at least one or more member inflated their housework contributions
  + People provided more examples of their own contributions (vs. spouse)

---
##  Using the BTA effect to nudge policy 

```{r gph1, echo=FALSE, fig.align='center', fig.height=6.5}

palette <- c("#E69F00", "#56B4E9")
cond <- c("Nudge", "Control")
value <- c(.37, .54)
gph1 <- data.frame(cond, value)

gph1 %>% 
  ggplot(aes(factor(cond, levels = c("Nudge", "Control")), value, fill = cond)) +
  scale_fill_manual(values = palette) +
  geom_col(position = "dodge") +
  expand_limits(y = c(0, 0.6)) +
  labs(
    title = "",
    x = "",
    y = "Preference for immediate gratification \n"
  ) +
  theme_xaringan() +
  theme(
    legend.position = "none",
    axis.text = element_text(size = 20),
    title = element_text(size = 24)
  )
```
.right[*Pietroni & Huges (2016)*]

???

+ **Researchers interested in policy have wondered if you could use this BTA effect to nudge people in the support of certain policies...Basically using one bias to combat the other**
+ We talked in a past class about retirement savings and the idea that people have a hard time saving for retirement because they consider their future self to be a different person
+ Now in THIS study, the researchers started from the past research that self-empowerment was actually associated with more strong feelings of connection to one's future self...so they wanted to test if they could move that dial with the BTAE, and then produce greater connection to the future self and in turn greater saving for the future
+ Basically trying to capitalize on the fact that most of us show a BTAE....and in doing so, help us save money
+ Had participants rate themselves and the average peer on a number of dimensions related to social skills...an outlet for the BTAE to emerge (as people did consistently rate themselves higher)
+ Then, some got an added nudge where they viewed a celebrity message endorsing the importance of SOCIAL SKILLS (at which participants believe they are better) for one's future, while the control condition did not receive that information
+ Finally they participated in a temporal discounting (or savings) task where they had to make a lot of decisions between small immediate rewards and larger long-term payouts
+ Results:
  + They found that not only did the nudge increase feelings of connectedness with one's future self, but it also increased temporal discounting
  + That is, people who didn't receive the nudge were more likely to give into immediate temptation
  + And people who DID receive the nudge (which acted on their BTAE) were able to wait for a longer term reward

---
#  Politically motivated cognition 

<br>
<br>
.pull-left[<img src="assets/img/image23.jpg">]
.pull-right[<img src="assets/img/image24.png">]

???

+ People tend to believe that THEIR candidate won a given debate, especially if the “winner” is ambiguous or not decisively clear
+ Your readings refer to a study of the public reactions to the Kennedy-Nixon debates in the 1960s...showing that those who were pro-Kennedy thought that he had won the debates and those that were pro-Nixon thought he had won
+ But this is a phenomenon we see even up to this day...you can see it in news sources that may have partisan leanings, presenting polling data of who won the final Clinton–Trump debates
  + Same debate, wildly different interpretations
+ And this motivated cognition extends beyond just performance appraisal, to evaluations of how much everyone else is in favor of your candidate
+ Voters tend to exaggerate how much their candidate is favored by others and so overestimate their candidate's chances of winning

---
###  Motivated reasoning on the Supreme Court

.center[<img src="assets/img/image30.png" width=95%>]

???

+ You can even see this politically motivated reasoning among those most strongly charged with being objective and purely cognitive in their reasoning
+ This graphic is taken from one study that analyzed 4,519 votes in 516 cases from 1953 to 2011
+ Results: 
  + While liberal justices are over all more supportive of free speech claims than conservative justices,” the study found, “the votes of both liberal and conservative justices tend to reflect their preferences toward the ideological groupings of the speaker.”
+ https://www.nytimes.com/2014/05/06/us/politics/in-justices-votes-free-speech-often-means-speech-i-agree-with.html?smid=fb-share&smv1&_r=0

---
#  Motivated reasoning in the brain

.center[<img src="assets/img/image31.png" width=90%>]

???

+ Neuroscience research has even been done to demonstrate motivated reasoning as distinct from other reasoning where motivation and affect are less strong
+ In one study, researchers recruited 15 democrats and 15 republicans and they presented information to them about Republican George Bush, Democrat John Kerry, and a politically neutral male figure (baseball player Hank Aaron)
+ specifically about these targets, participants were shown an initial statement they supposedly made, followed by a contradictory statement (suggesting that the target's words and actions didn't align or was being inconsistent)
+ And participants were asked to think about whether the person's words and actions were consistent with each other
+ As you can see from their behavioral data, (where the DV = perceived contradiction), when bush contradicts himself, democrats are much more likely to rate him as contradictory than are Republicans, but the reverse is true when Kerry contradicts himself
+ And both groups are equal in their evaluations of Hank Aaron
+ Taken together this behavioral data shows strong evidence of politically motivated reasoning
+ However, even more exciting is that the did this while people were in an fMRI machine scanning their brain!

---
#  Motivated reasoning in the brain

+ Areas of the brain associated with emotion are activated in the context of politically motivated reasoning

<br>
<br>
.center[<img src="assets/img/image32.png">]

???

+ What they found is that when participants processed threatening or contradictory information about one's preferred candidate...**areas of the brain associated with emotion and with emotionally motivated reasoning** were more active, relative to the same kind of info about a neutral target (Hank Aaron)
+ This shows that motivated reasoning is not only leading to different judgments, but that the **actual process** is a DIFFERENT FORM of reasoning compared to reasoning in general

---
#  Motivated visual perception 

<br>
.center[<img src="assets/img/image25.png" width=80%>]

???

+ These effects can even extend into the realm of perception...not only do we belief what we want to believe but we also see what we want to see
+ For example, what do you see when I show you this figure?
  + Some people see a duck, some people see a rabbit

---
#  Motivated visual perception 

.center[<img src="assets/img/image26.png" width=45%>]

???

+ Now what if I showed you this figure?
+ One interpretation is a horse...the other is a seal (sea animal, farm animal)
+ So different interpretations of this ambiguous figure are possible, but what happens when motivation gets involved?
+ Researchers did a study where they presented participants with this ambiguous image that could be interpreted as either a horse or a seal
  + They told participants that if they, in this study, saw more farm animals they would have to drink a fresh glass of orange juice, if they saw more sea animals would have to drink a gross veggie smoothie
  + Then showed them equal numbers of sea and farm animals until they finally got to this one
  + What do you think they found?
+ People were much more likely to see the animal as representative of the category associated with a pleasant vs. gross outcome...people told they would have to drink a gross smoothie if they saw sea animals were much more likely to see this as a horse, and the reverse
  + This even persists if, right after they see it, experimenters fake a computer crash and switch which kind of animal means which consequences
  + They still will report it being most like the category of animal they were initially given...suggesting they aren't LYING to get their desired outcome, but rather really SAW the image as more like one thing or the other based on the initial reward

---
#  Motivated visual perception 

.center[<img src="assets/img/image33.png" width=75%>]

.right[*Hastorf & Cantril (1954)*]

???

+ This motivated visual perception can even extend to group-based contexts
+ For example, in a classic study called “They Saw a game” by Hastorf and Cantril, the researchers took students from Princeton and from Dartmouth and showed them a video of a recent football game between their schools
+ They were asked to fill out a questionnaire at the end about the number of rule infractions they saw in the game
+ When Princeton students looked at the movie of the game, they saw the Dartmouth team make over twice as many infractions as their own team made
  + And they saw the Dartmouth team make over twice as many infractions as were seen by Dartmouth students
+ When Dartmouth students looked at the movie of the game they saw both teams make about the same number of infractions
  + And they saw their own team make only half the number of infractions the Princeton students saw them make
+ The interaction here is that potentially the Dartmouth team was objectively more aggressive, in that by the end of the game they had been penalized 70 yards total while Princeton had only been penalized 25
+ But as you can see, both sides interpreted (or “saw”) the game in ways that supported their collective or group-based esteem

---
#  Motivated visual perception

<br>
.center[
<iframe width="560" height="315" src="https://www.youtube.com/embed/QeIrdqU0o9s?start=101" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
]

???

+ Recommended: from 1:41–4:18
+ Now, much like biases in our judgments and cognitions that stem from our motivations, biases in visual perception that is motivated might in some ways be advantageous
+ In the video, she goes on to talk about research finding that more physically fit people literally see the finish line as closer...potentially this motivated perception facilitates their exercises...(let's them see the goal as attainable)
+ So selectively focused or narrowed attention serves us

---
#  Wrap-up

+ We believe what we want to believe


+ Motivated reasoning serves a function (maintains self-image, etc.) and in this way may be beneficial, but can also be harmful


+ This motivated reasoning extends into visual perception

<br>
<br>
.right[<img src="assets/img/image34.png" width=35%>]

???

+ May be beneficial because the resulting illusions promote mental health...unrealistically positive views of oneself and the world are often adaptive
+ This seems true for illusory beliefs that do not serve as the basis for important action
+ But motivated reasoning can be dangerous when it is used to guide behavior and decisions, especially in those cases in which objective reasoning should facilitate more adaptive behavior...can you think of examples?
  + For example, people who play down the seriousness of early symptoms of severe diseases such as skin cancer and people who see only weaknesses in research pointing to the dangers of drugs such as caffeine or of behaviors such as drunken driving may avoid life saving treatment or continue harmful habits
