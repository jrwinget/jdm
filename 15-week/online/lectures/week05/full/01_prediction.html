<!DOCTYPE html>
<html lang="" xml:lang="">
  <head>
    <title>Prediction</title>
    <meta charset="utf-8" />
    <meta name="author" content="PSYC/PHIL 279, Winget" />
    <link rel="stylesheet" href="xaringan-themer.css" type="text/css" />
  </head>
  <body>
    <textarea id="source">
class: center, middle, inverse, title-slide

# Prediction
### PSYC/PHIL 279, Winget

---




#  Prediction

.center[&lt;img src="assets/img/image06.png" width=80%&gt;]

???

+ We've already addressed prediction in a way during the affective forecasting lecture
+ In that case, we were concerned with how people predict their future feelings
+ Here we look at other outcomes with a focus on a comparison between the accuracy of clinical or expert predictions and statistical prediction based on some formula that combines multiple pieces of information
+ For example, consider a situation in which a psychiatric patient presents ambiguous symptoms
  + Would this condition best be treated by psychotherapy alone or might it also require antipsychotic medication that may have dangerous or unwanted side effects?
+ Or, imagine a parole board determining whether or not to allow out an offender out on parole after serving 2 years in prison
  + What factors should they use to determine if the person is likely to reoffend?
+ These brief scenarios highlight a few of the many situations in which experts are asked to diagnose conditions or predict important human outcomes

---
##  Clinical intuition vs. statistical prediction

.pull-left[.full-width[.center[
Traditional
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
&lt;img src="assets/img/image01.png" height=300&gt;
]]]

.pull-right[.full-width[.center[
Sabermetrics
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
&lt;img src="assets/img/image02.png" height=300&gt;
]]]

???

+ Or consider how baseball teams choose players to fill out their rosters:
+ The traditional method involves having...
  + A scout form an intuitive impression of whether a player is going to perform well, based on different aspects of the player’s performance, reputation, and physical appearance
+ Sabermetrics, on the other hand, is an entirely different approach
  + Here, an enormous database is analyzed to determine which traits are the **best** predictors of major-league success
+ Debate continues over which method is superior
  + Something to be said for experience and wisdom, but we also know how imperfect our experience can be when making judgments and decisions
  + Statistics are less likely to be influenced by these factors
+ Similar debates exist in different domains...like criminal profiling and clinical diagnosis

---
#  Moneyball

.center[
&lt;iframe width="460" height="215" src="https://www.youtube.com/embed/TpBcwGOvO80" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen&gt;&lt;/iframe&gt;
&lt;iframe width="460" height="215" src="https://www.youtube.com/embed/rMObWsKaIls" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen&gt;&lt;/iframe&gt;
]

???

+ A great real-life example comes from Billy Beane and the Oakland A's 2002 baseball team
  + If any of you have seen the movie Moneyball, you might already be familiar with this idea
  + Movie based on the real-life story about how the Oakland A's baseball team changed the game
  + Before the 2000s, baseball used to rely on the collective wisdom of insiders to pick players and manage teams
  + Billy Beane, then manager of the Oakland A's challenged this approach by using statistics to make judgment and decisions about how to best run their team
+ In this first scene, Jonah Hill plays the statistician meeting the Oakland A's manager for the first time and tells him all of their approaches have been wrong so far
+ In this second clip, they talk more about the logic of what they're doing

---
##  Clinical intuition vs. statistical prediction

+ Clinical intuition
  + People with experience and expertise in the domain make intuitive predictions for individual cases
  + The expert decides, based on intuition, which features of the case are most important for making the prediction
  + Dawes, 1994; Meehl, 1954/1996


+ Statistical prediction
  + Predictions about a particular case are made solely on the basis of empirical evidence and/or a statistical comparison to data drawn from a large sample
  + Dawes, 1994; Dawes, Faust, &amp; Meehl, 1989; Meehl, 1954/1996

???

+ These examples highlight the distinction between two different approaches to prediction
+ Clinical intuition generally refers to all approaches in which people with experience and expertise make intuitive predictions for individual cases
  + Experts consider the information available and make the decision themselves
  + Idea is that the expert is able to understand the dynamics of the individual case (e.g., how different aspects of it fit together)
  + Once the expert understands the case, the expert should be able to make more accurate predictions about it
  + In other words, the expert decides, based on intuition, what features of the case are most important for making the prediction
+ In contrast, statistical prediction refers to approaches in which predictions about a particular case are made solely on the basis of empirical evidence and/or a statistical comparison to data drawn from a large, representative sample
  + Intuitive reasoning takes a back seat to statistical analysis or formula used to compute predictions based on available data
  + Idea is that the most accurate predictions, on average, are made from empirical data

---
##  Clinical intuition vs. statistical prediction

+ Many studies demonstrate the superiority of statistical prediction
	+ Psychiatric diagnosis (Goldberg, 1965)
	+ Military training success
	+ Academic success
	+ Parole violation
	+ Business bankruptcy


+ Unstructured interviews lack validity in predicting much of anything

.center[&lt;img src="assets/img/image08.jpg" width=500&gt;]

???

+ When making these comparisons, it's critical the experts and the statistical models use the same information when evaluating the accuracy of their predictions
+ Typically, this information includes test scores or grades, criminal record, psychological or physical symptoms
+ But some studies include observer ratings of specific attributes (judged creativity, or whatever you can imagine)
+ One area in which it has been incredibly difficult to convince people that intuitive prediction lack validity is the unstructured interview
  + You may have already or surely will in the near future have to go through one of these
  + This is where you sit down and are asked questions by someone from HR or your potential future boss
  + There is no evidence that such interviews yield important information beyond that of past behavior—except whether the interviewer likes the interviewee, which may be important in some contexts (like mentor--mentee relationships)

---
#  Bankruptcy

+ Libby (1976) asked 43 bank loan officers to predict which 30 of 60 firms would go bankrupt within three years of a financial report
	+ Bank officers were 75% correct
	+ A regression analysis based on the same data was 82% accurate


+ However, this research has received some criticism (Goldberg, 1976)

&lt;br&gt;
.center[&lt;img src="assets/img/image09.jpg" width=400&gt;]

???

+ Now, one study that challenges the idea that statistical models outperform human judgment comes from Libby (1976)
  + Libby had 43 bank loan officers predict which firms would go bankrupt within 3 years of a financial report
  + Bank loan officers were given 60 firms and were asked to predict which half would go bankrupt
  + Loan officers requested and were provided with various financial ratios (e.g., the ratio of liquid assets to total assets) in order to make their predictions
  + Loan officers were almost as good as the statistical models when making this predictions: 75% vs. 82%
  + Libby used these results to argue that these experts had a great deal of task-relevant expertise, which is why they performed so well
+ However, there has been some criticism of this research
  + Goldberg argued that some of the data were skewed
  + After normalizing the data, the simple linear models outperformed experts by nearly 72%

---
##  Why do statistical models beat human judgment?

+ In most cases, variables have main effects or linear relationships with outcomes
	+ Statistical models ensure that variables contribute to conclusions based on their actual predictive power and relation to outcome
	+ People have a difficult time attending to two or more non-comparable aspects of a stimulus or situation at once

.center[&lt;img src="assets/img/image10.png" width=55%&gt;]

???

+ Why is it that these simple linear models predict better than expertise? 
+ There are few situations where variables interact in complex ways, so linear models usually are quite good at approximating reality
+ Statistical models only include cues that actually predict the outcome...people often have difficulty distinguishing valid and invalid cues
  + And people rarely receive feedback about the accuracy of their judgments, making it difficult to calibrate future judgments
+ People often shift their attention from one cue to another and rarely combine the different cues in any meaningful way...unlike linear models
+ Take the experience of evaluating academic applicants
  + Often they anchor their judgment on some salient cue or piece of information, such a really high or really low test scores, and then adjust slightly as they view new information (GPA)
  + Rarely do they integrate the various pieces of information in any meaningful way
+ Statistical models will use all the independent information provided, combine it meaningfully, which leads to more accurate outcomes

---
#  Possible solutions

+ Domain experts can use a combination of the two approaches
  + They can see what the statistical prediction method has generated, but still have the final say


+ In medicine, computer-based decision support systems can be used to compare a patient’s characteristics and symptoms against past cases in a database (Hunt, Haynes, Hanna, &amp; Smith, 1998)

.center[&lt;img src="assets/img/image11.jpg" width=73%&gt;]

???

+ Although it’s tempting to argue one approach is better than the other, a more practical solution is to use a combination of the two
+ Each approach has strengths and weaknesses, but a combination leverages the strengths of both
+ In this way, the experts can see what the statistical prediction method generates, but still retains the final decision on what the course of action will be
+ For example, in medicine, computer-based decision support systems are software tools designed to compare a patient’s characteristics and symptoms against past cases in the database
  + The decision support system provides a recommendation and/or diagnosis for that particular patient
  + And doctors can consider this when making their own judgments
  + Diagram is a simplified example of this for asthma management (not necessarily diagnosis here, but the logic is similar)

---
##  Forecasting, expertise, and feedback

+ Clinicians do not always become more accurate with experience


+ Corrective feedback that ultimately directs the clinician’s attention away from invalid predictors and toward valid ones is critical (Einhorn &amp; Hogarth, 1978)


+ In weather forecasting, feedback is consistently given
  + After a prediction is made, we have only to wait a short time to see how the weather turns out

.center[&lt;img src="assets/img/image12.jpg" width=73%&gt;]

???

+ One somewhat surprising finding in the literature is that clinicians do not always become more accurate with experience
  + In fact, some research suggests there is generally no relationship between clinical experience and validity of predictions
+ Researchers suggest a major reason is that feedback on performance is really necessary for learning to occur
  + Especially corrective feedback that directs the clinician’s attention away from invalid predictors and toward valid ones
  + E.g., after seeing some cases of strep throat co-occurring with soreness of the throat, a medical student might erroneous conclude throat soreness is a necessary predictor of strep
      + If the student’s supervisor denies this is the case or if the student experiences a contradictory case in which a patient tests positive for strep without a sore throat, this corrective feedback might lead the student to revise their view
      + But if the student gain experience without receiving such corrective feedback, the student will not be able to improve
  + Contrast this with the domain of weather forecasting
      + Here, experience does lead to more accurate predictions
      + However, in weather forecasting, feedback is frequently, consistently, and relentlessly given: After a prediction is made, we only have to wait to see how the weather turns out
      + Weather forecasters also have access to years of base rate information

---
#  End-of-life decision-making

+ Living will, or instructional advance directive
  + A legal document in which a person expresses their preferences for end-of-life medical care


+ *Benefits*
  + Can lead to:
      + Reduced hospitalizations and machine intervention
      + Increased use of palliative care

&lt;br&gt;
.center[&lt;img src="assets/img/image13.jpg" width=50%&gt;]

???

+ One context that dramatically alters judgment and decision making process is during the end of one's life
  + Pain, suffering, medications, and countless other things can steer one away from the typical decisions they might make in the absence of these things
+ Some have suggested end-of-life decision-making can be aided by having each patient make a living will or instructional advance directive
  + Legal documents in which people express their preferences for end-of-life medical care
  + Lets doctors and caregivers know how you want to be medically treated in case you are not able to to express this yourself (e.g, unconscious)
+ Research suggests net positive of living wills on quality of care
+ These documents can lead to...
  + Reduced hospitalizations and machine intervention
  + Increased chances end-of-life was carried out in line with patient wishes
  + Increased use of palliative care (care focusing on effective stress, pain, and symptom management, focus on quality of life) 

---
#  End-of-life decision-making

+ *Challenges*
  + Not clear whether we can expect a person’s choices to remain consistent over time
  + Given the literature on affective forecasting, it seems likely that when making a living will, we may not know how we will react to different treatment options in the future (Ditto et al., 2005)
  + The peak-end rule (Redelmeier &amp; Kahneman, 1996) might lead people to select a longer, more painful treatment they have experienced over a shorter, less painful treatment if the peak-end average of the longer treatment is lower


+ Could computerized decision support systems help?

???

+ However, there are major decision-making challenges with creating living wills
  + Not clear whether we can expect people’s choices to remain consistent over time 
  + Given what we know about affective forecasting, when making a living will, we might not always know the intensity we will react to different treatment options...might change mind down the road
  + Other research suggests when remembering an episode of pain, we think of how much it will hurt at the peak of the episode and how much it will hurt at the end
      + The average of these 2 points is a very robust predictor of people’s decisions about future actions
      + Called the peak-end rule
+ Potential approach to addressing end-of-life decision-making is to create computerized decision support systems for patients, families, and doctors
  + Simplified approach: poll people of a wide range of demographics for their treatment preferences, and make predictions for any given patient based on such demographic variables 
  + Would also draw upon large medical databases for common major medical conditions, tracking the likelihood of success for each possible treatment path
  + Lots of limitations though, most significant being $

---
##  Investing for the future: &lt;br&gt; Money and health

+ Intemporal choice
  + Choose immediate outcome (e.g. spend money now) or future outcome (save for later)?


+ Temporal discounting
  + The farther away the future gains, the less value people place on them (Green, Myerson, &amp; McFadden, 1997)
  + People find it difficult to make current sacrifices for future gains
  + Appears to a lesser degree with increased age (Green, Fry, &amp; Myerson, 1994)

.center[
&lt;img src="assets/img/image14.jpg" height=200&gt;
&lt;img src="assets/img/image15.jpg" height=200&gt;
]

???

+ Both end-of-life decisions and money management decisions are decisions of intertemporal choice
  + Intertemporal choice refers to choosing between an immediate outcomes (e.g., spending your paycheck now so you can enjoy some new clothes) and one in the future (e.g., saving your paycheck for retirement, or maybe even fancier new clothes to be worn in a few months)
+ Research has shown the farther away the future gains, the less value people place on those gains
  + Phenomenon call temporal discounting
  + Because people disproportionately undervalue future resource relative to those that can be use now, they generally find it challenging to make current sacrifices for future gains (e.g., eating less sugary foods now to promote greater health in the future)
  + Appears across lifespan but less so as people get older

---
##  Investing for the future: &lt;br&gt; Money and health

+ Hershfield et al. (2011) demonstrated people who had been shown an avatar of their older self allocated more money to a hypothetical retirement fund than when shown same-age avatars
  + Having seen the avatar, they felt more as though the money being saved was still for themselves, rather than for some hypothetical and disconnected older person

&lt;br&gt;
.center[&lt;img src="assets/img/image16.jpg" width=40%&gt;]

???

+ One intervention aimed at reducing temporal discounting tried to get young adults to save more money for their future retirement
+ Generally, people have a hard time seeing their future selves as themselves, and instead view their future self as a separate, distinct person
+ So the researchers though by showing the young adults an avatar of themselves aged 50 years might make the young adults feel more connected to their future selves
+ Across several studies, Hershfield and colleagues (2011) demonstrated people who were shown digitally aged avatars of themselves allocated more money to a hypothetical retirement fund than when they were shown same-age avatars of themselves
+ The idea is that when they decided how much money to save for their futures, they felt a little more as though it was still really themselves, not for some hypothetical older person somewhat tenuously connected to themselves

---
#  The planning fallacy

+ How long would it take you to finish your senior thesis?


+ If everything went as well as it could, how long would it take?


+ If everything went as poorly as it could, how long would it take?

&lt;br&gt;
.center[&lt;img src="assets/img/image17.png" width=65%&gt;]

???

+ A common situation that arises when preparing for the future involves the planning fallacy
+ In one study, students estimated how long it would take to finish their senior thesis
  + 3 conditions...different question (on slide)
  + Averages: 1) 33.9 days, 2) 27.4 days, 3) 55.5 days
+ How long did it actually take?
  + Well, only 30% of students completed their thesis in the amount of time they predicted (regardless of which question they were asked)
+ The basic idea, here, is that predictions about how much time will be needed to complete a future task display an optimism bias, and therefore people underestimate how long it will take to complete most tasks

---
#  Kahneman on the planning fallacy

&lt;br&gt;
.center[
&lt;a href="https://www.inc.com/daniel-kahneman/idea-lab-daniel-kahneman-the-planning-fallacy.html"&gt;&lt;img src="https://www.incimages.com/uploaded_files/image/970x450/kahneman-planning-fallacy-pan_28764.jpg" width=600 height=300 /&gt;
&lt;/a&gt;
]

???

+ Show Kahneman video here

---
#  The planning fallacy

+ People typically generate forecasts that are too optimistic


+ This is not limited to individuals, but applies to major construction projects as well


+ The Sydney Opera House

&lt;br&gt;
.center[&lt;img src="assets/img/image04.png" width=80%&gt;]

???

+ People know that they frequently underestimate the time it takes to complete a task
  + They have some self-knowledge, at a general level, that should make them less optimistic and more pessimistic about future predictions
+ Yet, when people make specific predictions about their futures, they seem to throw caution to the wind
+ Take the Sydney Opera House
  + The original estimates in 1957 predicted it would be completed by early 1964 for $7 million
  + A smaller, and less grand, version of the opera house finally opened in 1973 at a cost of $102 million
+ Despite numerous other examples of this sort, current planners remain overly optimistic about their own projects
+ For example, the builders of the Channel tunnel connecting Britain and France predicted that passenger trains would run between London and Paris in June 1993 and cost around 4.9 billion pounds
  + The real cost was closer to 10 billion pounds, and the channel wasn’t opened to passenger traffic until November 1994

---
#  The planning fallacy 

+ The signature of the planning fallacy is not so much that planners are optimistic in their predictions, but that they maintain their optimism about the current project in the face of historical (or personal) evidence to the contrary

&lt;br&gt;
.center[&lt;img src="assets/img/image05.png" width=60%&gt;]

???

+ Here’s a more formal definition or explanation of the planning fallacy
  + It's not that planners are optimistic in their predictions, but that they maintain their optimism about the current project in the face of historical (or personal) evidence to the contrary
+ Said another way, the planning fallacy refers to the conviction that a current project will go as well as planned...even though most projects from a relevant set of comparison projects have failed to fulfill their planned outcomes

---
#  The planning fallacy

+ Not only are people overly optimistic in their predictions, they are quite confident that they will meet their predictions


+ This indicates that people’s forecasts are not wishful thinking, but rather they see them as realistic estimates of how long it will take them to complete a task

&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
.right[&lt;img src="assets/img/image18.jpeg" width=40%&gt;]

???

+ Another characteristic of the planning fallacy is that people are confident they will fulfill their predictions
+ So not only are they overly optimistic about their predictions, they're also overconfident in them
+ This suggests that people's predictions actually isn't about wishful thinking
  + Instead, people seem to think they are setting realistic estimates of how long it will take them to finish a task

---
#  The planning fallacy

+ Why do people underestimate their completion times?
	+ Unique case versus similar to other tasks
	+ Self-serving bias
	+ Focalism

&lt;br&gt;
.center[&lt;img src="assets/img/image20.jpg" width=65%&gt;]

???

+ As with most robust psychological phenomena, the planning fallacy is determined by several factors
+ The cognitive processes people use to generate their predictions may be an important determinant of the degree of accuracy or bias
  + When making forecasts people can treat the task as unique or singular, or they may compare the present task to past projects
      + Remember that despite having a wealth of information about optimistic predictions not panning out, people seem to ignore this information
      + The reason is that people treat new tasks as unique, consider the unique features of the task, and ignore how the present task is similar to past tasks
  + The self-serving bias explanation suggests that people interpret their past performance in the most positive light, taking credit for when tasks went well, and blaming delays on outside influences
  + Another explanations, focalism, suggests that people fall prey to the planning fallacy because they only focus on the future task, and fail to consider similar tasks of the past that took longer to complete than expected or they fail to consider all the other things that may happened while they try to complete the task
      + When thinking about how long it will take to write a term paper, we fail to consider that our friends will invite us to a party that weekend, etc.

---
#  The planning fallacy

+ How to counteract the planning fallacy?
	+ Implementation intentions are concrete plans that specify how, when, and where one will act
	+ Creating such implementation intentions helps people overcome the planning fallacy
	+ When formed, people begin tasks sooner, experience fewer interruptions, and show less optimistic predictions

&lt;br&gt;
.center[&lt;img src="assets/img/image19.jpg" width=60%&gt;]

???

+ Despite the robustness of the planning fallacy, the best way to counteract its effects is with implementation intentions
+ Implementation intentions are basically “if, then” plans...concrete plans that specific how to act
+ Implementation intentions are effective because they link anticipated opportunities with effective goal-directed behavior
+ In this case, when we form implementation intentions, we specify the exact time and place that we would do a task
+ And when this is done, we tend to being tasks sooner, experiences less interruptions, and have less optimistic predictions

---
#  Wrap-up

+ Statistical prediction techniques have generally been found to be superior or equal to the intuitions of expert judges


+ Whether judges become more accurate with experience depends upon whether they receive corrective feedback on their predictions


+ When people make plans for the the future, they tend to be overly optimistic and overconfident in their predictions

.right[&lt;img src="assets/img/image07.png" width=35%&gt;]

???

+ Summarize slide
    </textarea>
<style data-target="print-only">@media screen {.remark-slide-container{display:block;}.remark-slide-scaler{box-shadow:none;}}</style>
<script src="https://remarkjs.com/downloads/remark-latest.min.js"></script>
<script>var slideshow = remark.create({
"highlightStyle": "github",
"highlightLines": true,
"countIncrementalSlides": false
});
if (window.HTMLWidgets) slideshow.on('afterShowSlide', function (slide) {
  window.dispatchEvent(new Event('resize'));
});
(function(d) {
  var s = d.createElement("style"), r = d.querySelector(".remark-slide-scaler");
  if (!r) return;
  s.type = "text/css"; s.innerHTML = "@page {size: " + r.style.width + " " + r.style.height +"; }";
  d.head.appendChild(s);
})(document);

(function(d) {
  var el = d.getElementsByClassName("remark-slides-area");
  if (!el) return;
  var slide, slides = slideshow.getSlides(), els = el[0].children;
  for (var i = 1; i < slides.length; i++) {
    slide = slides[i];
    if (slide.properties.continued === "true" || slide.properties.count === "false") {
      els[i - 1].className += ' has-continuation';
    }
  }
  var s = d.createElement("style");
  s.type = "text/css"; s.innerHTML = "@media print { .has-continuation { display: none; } }";
  d.head.appendChild(s);
})(document);
// delete the temporary CSS (for displaying all slides initially) when the user
// starts to view slides
(function() {
  var deleted = false;
  slideshow.on('beforeShowSlide', function(slide) {
    if (deleted) return;
    var sheets = document.styleSheets, node;
    for (var i = 0; i < sheets.length; i++) {
      node = sheets[i].ownerNode;
      if (node.dataset["target"] !== "print-only") continue;
      node.parentNode.removeChild(node);
    }
    deleted = true;
  });
})();
(function() {
  "use strict"
  // Replace <script> tags in slides area to make them executable
  var scripts = document.querySelectorAll(
    '.remark-slides-area .remark-slide-container script'
  );
  if (!scripts.length) return;
  for (var i = 0; i < scripts.length; i++) {
    var s = document.createElement('script');
    var code = document.createTextNode(scripts[i].textContent);
    s.appendChild(code);
    var scriptAttrs = scripts[i].attributes;
    for (var j = 0; j < scriptAttrs.length; j++) {
      s.setAttribute(scriptAttrs[j].name, scriptAttrs[j].value);
    }
    scripts[i].parentElement.replaceChild(s, scripts[i]);
  }
})();
(function() {
  var links = document.getElementsByTagName('a');
  for (var i = 0; i < links.length; i++) {
    if (/^(https?:)?\/\//.test(links[i].getAttribute('href'))) {
      links[i].target = '_blank';
    }
  }
})();
// adds .remark-code-has-line-highlighted class to <pre> parent elements
// of code chunks containing highlighted lines with class .remark-code-line-highlighted
(function(d) {
  const hlines = d.querySelectorAll('.remark-code-line-highlighted');
  const preParents = [];
  const findPreParent = function(line, p = 0) {
    if (p > 1) return null; // traverse up no further than grandparent
    const el = line.parentElement;
    return el.tagName === "PRE" ? el : findPreParent(el, ++p);
  };

  for (let line of hlines) {
    let pre = findPreParent(line);
    if (pre && !preParents.includes(pre)) preParents.push(pre);
  }
  preParents.forEach(p => p.classList.add("remark-code-has-line-highlighted"));
})(document);</script>

<script>
slideshow._releaseMath = function(el) {
  var i, text, code, codes = el.getElementsByTagName('code');
  for (i = 0; i < codes.length;) {
    code = codes[i];
    if (code.parentNode.tagName !== 'PRE' && code.childElementCount === 0) {
      text = code.textContent;
      if (/^\\\((.|\s)+\\\)$/.test(text) || /^\\\[(.|\s)+\\\]$/.test(text) ||
          /^\$\$(.|\s)+\$\$$/.test(text) ||
          /^\\begin\{([^}]+)\}(.|\s)+\\end\{[^}]+\}$/.test(text)) {
        code.outerHTML = code.innerHTML;  // remove <code></code>
        continue;
      }
    }
    i++;
  }
};
slideshow._releaseMath(document);
</script>
<!-- dynamically load mathjax for compatibility with self-contained -->
<script>
(function () {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src  = 'https://mathjax.rstudio.com/latest/MathJax.js?config=TeX-MML-AM_CHTML';
  if (location.protocol !== 'file:' && /^https?:/.test(script.src))
    script.src  = script.src.replace(/^https?:/, '');
  document.getElementsByTagName('head')[0].appendChild(script);
})();
</script>
  </body>
</html>
