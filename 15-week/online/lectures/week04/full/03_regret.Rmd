---
title: "Regret"
author: "PSYC/PHIL 279, Winget"
output:
  xaringan::moon_reader:
    css: xaringan-themer.css
    lib_dir: libs
    nature:
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: false
---

```{r xaringan-themer, include=FALSE, warning=FALSE}
library(tidyverse)
library(xaringanthemer)

style_mono_dark(
  base_color = "#4C837A",
  text_color = "#E1DDBF",
  background_color = "#04263C",
  title_slide_text_color = "#FFFFFF",
  text_slide_number_color = "#04263C",
  colors = c(
  red = "#FF0000",
  blue = "#00FFFF",
  purple = "#FF00FF"
  ),
  extra_css = list(
    ".large-text" = list("font-size" = "150%"),
    ".small-text" = list("font-size" = "75%")
  )
)
```

##  Misprediction of regret <br>(Gilbert et al., 2004) 

+ Regret is a combination of disappointment and self-blame
	+ We base many of our decisions on anticipated regret (Zeelenberg et al., 1998)
	+ “What if I find a better deal on this car?”
	+ “If I don’t study I will regret it”

???

+ 

---
##  Misprediction of regret <br> (Gilbert et al., 2004) 

+ We expect regret to be increased by factors that highlight our personal responsibility for a negative outcome
	+ Missing an airplane by a few minutes, or a gold medal by a few milliseconds

<br>
.center[
<img src="assets/img/image11.png" height=250 width=48%>
<img src="assets/img/image10.png" height=250 width=51%>
]

???

+ This is why bronze medalists are often happier than silver medalists; they smile more on the medal platform
+ The reason is that it is easier for the silver medalist to imagine that they could have won the gold if only they had done x, y, or z
+ It is easier for them to come up with a counterfactual situation in which they would have won, whereas the bronze medalist has a much more difficult time doing so because they were not as close to winning

---
#  Misprediction of regret 

+ People expect to blame themselves more when they fail by a narrow margin than a wide margin
	+ If so, people should expect to feel more regret in the former than latter situation
	+ But, we tend to rationalize or avoid self-blame quite easily
	+ If so, margin should have little impact on actual experience of regret

???

+ 

---
#  Misprediction of regret 

.pull-left[<img src="assets/img/image15.png"height=450>]
.right[
<img src="assets/img/image13.png" height=225 width=325>
<img src="assets/img/image14.png" height=225 width=325>
]

???

+ 

---
#  Regret on the subway 

+ Ps were approached as they entered the track at a subway station
	+ Those that arrived 30-90s after the train left told that they had missed it by 1min (narrow)
	+ Those that arrived 3.5-6min after train left were told they missed by 5min (wide)
	+ Forecasters asked to predict how they would feel if they missed the train by 1min or 5min

???

+ 

---
#  Regret 

+ Size of margin influenced forecasts of regret, with a narrow margin believed to produce more regret than a wide margin
	+ Size of margin did not influence actual experience of regret
	+ Forecasters also overestimated how disappointed they would be (margin had no influence on this, as disappointment doesn’t involve self-blame

???

+ 
