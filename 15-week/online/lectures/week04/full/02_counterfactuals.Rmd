---
title: "Counterfactuals"
author: "PSYC/PHIL 279, Winget"
output:
  xaringan::moon_reader:
    css: xaringan-themer.css
    lib_dir: libs
    nature:
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: false
---

```{r xaringan-themer, include=FALSE, warning=FALSE}
library(tidyverse)
library(xaringanthemer)

style_mono_dark(
  base_color = "#4C837A",
  text_color = "#E1DDBF",
  background_color = "#04263C",
  title_slide_text_color = "#FFFFFF",
  text_slide_number_color = "#04263C",
  colors = c(
  red = "#FF0000",
  blue = "#00FFFF",
  purple = "#FF00FF"
  ),
  extra_css = list(
    ".large-text" = list("font-size" = "150%"),
    ".small-text" = list("font-size" = "75%")
  )
)
```

##  Counterfactual thinking (Roese, 1997) 

+ “What might have been”
	+ Counterfactual thoughts are mental representations of alternatives to past events, actions, or states

<br>
<br>
.center[<img src="assets/img/image5.png" width=100%>]

???

+ In everyday life, an individual’s counterfactual musings often take the form of a conditional proposition, in which the antecedent corresponds to an action and the consequent corresponds to an outcome (e.g., “If only I had studied, I would have passed the exam”)
+ Counterfactual thinking emerges quite early in development, with 2 year-olds entertaining ideas of “if only.”
+ Crucially, counterfactual thoughts are often evaluative, specifying alternatives that are in some tangible way better or worse than actuality. Better alternatives are termed upward counterfactuals...worse alternatives are termed downward counterfactuals
+ **Explain table**

---
##  Time-course of counterfactual thinking 

+ According to Roese (1997) counterfactual thinking occurs in two steps or stages.
	+ **.blue[Activation]** of counterfactual thought (often caused by negative affect)
	+ **.blue[Content]** of counterfactual thought (determined by normality or unusualness of event, etc.)

<br>
.center[<img src="assets/img/image6.png" width=80%>]

???

+ Activation refers to whether the process of Counterfactual generation is initially switched on or off (e.g., Bargh, 1996; Higgins, 1996), whereas content refers to the specific makeup of the resulting Counterfactual thought
+ For example, Violet has just failed an exam
+ Whether she wonders at all if she might have performed better or instead focuses only on what was reflects the issue of mere activation
+ If counterfactual processing is indeed activated, she might then muse that' 'If only I had studied harder, I would have passed.
+ The content of this particular Counterfactual focuses on an alteration to her study habits; but once activated, counterfactual content may take many forms
+ Violet may as easily have wondered whether curtailing her drinking or moving in with her aunt might have resulted in a better exam score
+ Activation and content are related but conceptually distinct aspects of counterfactual generation
+ Temporal order is an important aspect of this distinction
+ Initial activation is a necessary condition for any content effects to occur but not vice versa
+ Moreover, different variables influence these two stages of activation and content.
+ In general, affect is the main determinant of activation, whereas "normality" (i.e., whether circumstances surrounding the outcome are "normal" or unusual) is the main determinant of content

---
#  Counterfactual thinking 

+ When do we engage in counterfactuals?
	+ Exceptional or rare events
	+ Negative outcomes
	+ Closeness of outcome

.center[<img src="assets/img/image7.png" width=65%>]

???

+ So, when do we actually use counterfactual thinking?
+ Well, there are a few situations that make counterfactuals more likely
+ The first is when there's some sort of rare or exceptional event...for example, if you see 2 cars almost crash into one another...you might think "wow, if that car was one foot to the right, or if that person didn't swerve in time, they both would have been toast!"
+ Another situation where we're prone to using counterfactuals is when there is some sort of negative outcome....for example, if someone close to you passes, you might say something like "if only he had quit smoking, he would still be here today"
+ A third situation is outcome closeness...Outcome closeness refers to the perceived nearness of achieving a goal
+ A clear example of this is when we just missed our train or a flight
+ When this happens, thoughts of “if only I had not stopped for coffee on the way to the airport, or if I only woke up on time, etc” pop into our heads

---
##  What good is counterfactual thinking? 

+ They are functional (Epstude & Roese , 2008)

<br>
<br>
<br>
.center[<img src="assets/img/image8.png" width=100%>]

???

+ So far, we've talked about counterfactual thinking as this thought process we engage in that doesn't really seem to be too productive right?
+ We start thinking about all these "what ifs" but the event has passed, right...so what good is thinking like this if we can't change something that's already happened?
+ Well according to the functional view, counterfactual thinking may be seen primarily as a useful, beneficial, and utterly necessary component of behavior regulation
+ Counterfactual thoughts are closely connected to goal cognitions
+ Counterfactual thoughts are typically activated by a failed goal, and they specify what one might have been done to have achieved that goal (Markman et al., 1993; Roese, Hur, & Pennington, 1999)
+ By imagining what might have been when something bad happens or we fail to achieve a goal, we can learn what we should do if we want to achieve it in the future or not have the bad thing happen again
+ I should have gotten up on time, I should have set my alarm earlier; I should not have started smoking; I should have taken my medication, etc
+ All of these thoughts help us develop the intention to achieve a goal or change our behavior

---
##  Counterfactual reasoning and harmful events 

+ In the context of harmful events, counterfactual reasoning entails thinking about how deleterious outcomes could have been avoided
	+ Research suggests that in considering alternatives to unfortunate outcomes, observers focus on unusual, unexpected, or exceptional circumstances (Kahneman & Miller, 1986)

<br>
.right[<img src="assets/img/image42.png" width=40%>]

???

+ Another way counterfactual thinking can be helpful is when it comes to harmful events
+ In the context of harmful events, counterfactuals let us thinking about how bad outcomes could have been avoided
+ The idea is that when we think about unfortunate outcomes, we tend to focus on unusual or unexpected features of the situation
+ For example, if we see someone driving erratically...we might focus on the fact that the driver is elderly...so the counterfactual reasoning would suggest something like "elderly drivers are dangerous...therefore, if we take away elderly drivers licenses, we would have less erratic drivers on the road"
+ As another example...consider the following experiment:

---
##  Counterfactual reasoning and harmful events 

+ Miller and McFarland (1986) examined the role of counterfactual reasoning in a legal setting
	+ Participants learned that a man was shot and injured while visiting his usual convenience store or a different one (mutable feature)
	+ Results showed that more compensation was recommended when the man was shot in the unusual store
	+ “If only the actor had gone to his usual store, he wouldn’t have been shot”

???

+ An early study by Miller and McFarland (1986, Study 1) exemplifies counterfactual reasoning explanations of harmful events
+ Participants in this experiment learned that a man was shot and injured while visiting his usual convenience store or a different one
+ The primary response measure was the amount of money participants awarded to the victim
+ Results showed that more compensation was recommended when the man was shot in the unusual than in the usual store
+ The counterfactual reasoning explanation of these results can be understood in terms of Roese’s two-stage process
+ First, harmful events evoke negative affect and instigate counterfactual investigations
+ In Miller and McFarland’s scenario, the innocent victim’s ordeal activates the counterfactual reasoning process
+ Second, the content of counterfactual reasoning is determined by the event’s most mutable features
+ The distinguishing mutable feature in these scenarios is the victim’s decision to visit a different convenience store
+ In this condition, counterfactual reasoning is expressed in ruminations such as “If only the actor had gone to his usual store, he wouldn’t have been shot.”
+ Counterfactual thoughts presumably lead observers to sympathize with the victim and to award him more compensation

---
##  Counterfactual thinking: Outcomes 

+ When upward counterfactuals focus on personal choice, the resulting emotion is termed **.purple[regret]**

<br>
.center[<img src="assets/img/image9.png" width=80%>]

???

+ I should have taken the job with higher salary vs. Other people with my qualifications make much less than I do
+ We'll talk more about regret in the next video...but this is a good time to point out how regret is connected to counterfactual reasoning
