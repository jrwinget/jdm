---
title: "Confirmation Bias"
author: "PSYC 279, Winget"
output:
  xaringan::moon_reader:
    css: xaringan-themer.css
    lib_dir: libs
    nature:
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: false
---

```{r xaringan-themer, include=FALSE, warning=FALSE}
library(tidyverse)
library(xaringanthemer)

style_mono_dark(
  base_color = "#4C837A",
  text_color = "#E1DDBF",
  background_color = "#04263C",
  title_slide_text_color = "#FFFFFF",
  text_slide_number_color = "#04263C",
  colors = c(
  red = "#FF0000",
  blue = "#00FFFF",
  purple = "#FF00FF"
  ),
  extra_css = list(
    ".large-text" = list("font-size" = "150%"),
    ".small-text" = list("font-size" = "75%")
  )
)
```

#  Confirmation bias 

+ A *positive test strategy* is one in which “you test a hypothesis by examining instances in which the property or event is .red[expected to occur] (to see if it does occur), or by examining instances in which it is .red[known to have occurred] (to see if the hypothesized conditions prevail)”
  + Klayman and Ha (1987), p. 212

???

+ In the last video, we talked about the process of hypothesis testing
+ We covered the difference between confirmatory evidence and disconfirmatory evidence...and mentioned how even though disconfirmatory evidence is often more informative, people have a tendency to use a positive test strategy and seek out confirmatory evidence
+ In this video, we'll pick back up on this discussion and relate these ideas to one of the most notorious cognitive biases around...the confirmation bias
+ 
+ Most notably, the confirmation bias is a heuristic that is frequently used when people are testing a hypothesis
+ Can be thought of as taking a positive test strategy, in which you test a hypothesis by examining the cases where the property or event is expected to occur...here, you're basically examining the cases where the event is likely to occur because you want to see if it does, indeed, occur as expected
+ It can also emerge in situations where you examine the cases in which the event or property is known to have ALREADY occurred...here you're basically examining the cases where the event has previously occurred in order to see if the hypothesized conditions prevail...if the things you think lead to the event happening happened prior to the event in this case

---
#  Confirmation bias 

+ In testing the hypothesis that all ravens are black, actively looking for black ravens would constitute a positive test strategy
  + A positive test strategy may be implemented without the researcher's awareness
  + It can also appear in the reinterpretation of evidence to fit the hypothesis (Nickerson, 1998)


+ A positive test strategy can be contrasted with a negative test strategy, in which you instead try to run tests or find cases that might rule out or disconfirm your working hypothesis
  + e.g., trying to find a raven that is any color other than black

???

+ In testing the hypothesis that all ravens are black, actively looking for black ravens would constitute a positive test strategy
  + This strategy may be implemented without the researcher's awareness
      + Fail to consider alternative explanations
  + It can also appear in the reinterpretation of evidence to fit the hypothesis
      + If using smiles as a measure of happiness, maybe misinterpret an ambiguous grin
+ A positive test strategy can be contrasted with a negative test strategy, in which you instead try to run tests or find cases that might rule out or disconfirm your working hypothesis
  + E.g., looking for a raven that is any color other than black

---
##  Seminal work on the confirmation bias 

+ Wason (1971) selection task


+ **.blue[Hypothesis:]** *Every card containing a vowel must have an even number on the other side*


+ **.purple[Goal:]** *Test this hypothesis while turning over only the cards necessary to do so*

<br>
<br>
.center[<img src="assets/img/image40.png" width=650>]

???

+ Classic studies on the confirmation bias clearly illustrate the contrast between adopting a positive test strategy and a negative test strategy in hypothesis testing
+ One of the most well-known laboratory studies on the positive test strategy/confirmation bias was produced by Wason's (1971) selection task
+ In the study, participants were given the hypothesis that every card containing a vowel must have an even number on the other side
+ They were also asked to test this hypothesis while turning over only the cards necessary to do so
+ Ask the class what they think
  + If someone gets it right, have them explain their logic
+ Explain to class:
  + Most people turn over E and 4 (46%)

---
##  Seminal work on the confirmation bias 

+ Wason (1971) selection task


+ **.blue[Hypothesis:]** *Every card containing a vowel must have an even number on the other side*


+ **.purple[Goal:]** *Test this hypothesis while turning over only the cards necessary to do so*

<br>
<br>
.center[<img src="assets/img/image41.png" width=650>]

???

+ 

---
##  Seminal work on the confirmation bias 

+ Wason (1971) selection task


+ **.blue[Hypothesis:]** *Every card containing a vowel must have an even number on the other side*


+ **.purple[Goal:]** *Test this hypothesis while turning over only the cards necessary to do so*

<br>
<br>
.center[<img src="assets/img/image42.png" width=650>]

???

+ So this confirms out hypothesis, right?
+ Example of positive test strategy and confirmation bias
  + All confirmatory evidence
+ But, there's actually no need to turn over 4 since nothing is said of consonants anyways (if we turn 4 over and its a C, this doesn't give useful info to test hypothesis)

---
##  Seminal work on the confirmation bias 

+ Wason (1971) selection task


+ **.blue[Hypothesis:]** *Every card containing a vowel must have an even number on the other side*


+ **.purple[Goal:]** *Test this hypothesis while turning over only the cards necessary to do so*

<br>
<br>
.center[<img src="assets/img/image43.png" width=650>]

???

+ Need to turn over E and 7 (only 4% did this in the study)
+ Having a vowel (e.g., U) on the back of the 7 disconfirms the hypothesis

---
##  Confirmation bias: <br> Research in the sciences 

+ The consistency fallacy occurs when one misinterprets data that merely confirm a hypothesis without simultaneously disconfirming an alternative hypothesis, claiming that such data show the hypothesis to be true (Mole & Klein, 2010)

???

+ In scientific inquiry, seeking truth in the world is generally the main goal
+ So, science provides one of the clearest cases for striving to avoid the confirmation bias
+ Yet, researchers have also observed what's been called the consistency fallacy
  + The consistency fallacy occurs when a person misinterprets data that merely confirm a hypothesis without simultaneously disconfirming an alternative hypothesis, claiming that such data show the hypothesis to be true
  + Instead of confirming the hypothesis, what researchers should really be saying is that the data are consistent with the hypothesis
      + Example of attending church and well-being...maybe it's not church per se, but rather a sense of community or social interactions with family and friends
  + In order to show stronger support for a hypothesis, confirmatory evidence (e.g., observing effects in line with a hypothesis) must be paired with evidence disconfirming an alternative hypothesis

---
##  Confirmation bias: <br> Issues in the law 

.pull-left[
+ Observers' prior expectations can influence how evidence is collected and interpreted in a criminal case


+ Kassin et al. (2013) dubbed this general effect the forensic confirmation bias
]

.pull-right[.center[<img src="assets/img/image44.gif" width=57%>]]

???

+ Criminal cases also involve hypothesis testing and evidence seeking, so confirmation bias also has an influence in these situations
+ For example, in the Mayfield case, a fingerprint was found on a bag of detonators after the Madrid terrorist attacks in 2004
  + The FBI identified this print as belonging to American attorney Brandon Mayfield
  + Mayfield happened to be Muslim and worked on a child custody case as a representative of a parent who was also a convicted terrorist
  + 3 different American fingerprinting experts ID'd print as Mayfield's, he was arrested (no other evidence linking him to bombings)
  + Spanish National Police instead ID'd print belonging to an Algerian national
  + FBI said they made a mistake and agreed with SNP's analysis
+ In a declassified review of the case, the Office of the Inspector General (OIG) reported confirmation bias played a role
  + Started with coincidence: Mayfield's print was similar to the one on the bag, but not identical
  + Then confirmation bias set in: After finding these initial similarities, experts “found” additional features that were not really there
  + Rather, these additional features were the result of biased interpretations of ambiguous features of the prints
+ Kassin and colleagues (2013) have called this effect the forensic confirmation bias
+ This effect has been demonstrated across a number of studies and has been found to influence eyewitness testimony and the judgments of jurors, judges, interrogators, and forensic experts

---
##  Confirmation bias: <br> Issues in the law 

+ Although fingerprint expertise is highly respected in forensic science, studies of practicing fingerprint experts have shown their identifications can be influenced by background knowledge they are told about the case
  + e.g., the Mayfield case (Dror & Charlton, 2006)


+ The linear procedure was added in 2011 to the FBI's standard operating procedures in direct response to the errors made in the Mayfield case (OIG, 2011)

<br>
.right[<img src="assets/img/image45.png" width=18%>]

???

+ For example, although fingerprint expertise is highly respected in forensic science, studies of practicing fingerprint experts have shown their identifications can be influenced by background knowledge they are told about the case
  + Like the Mayfield case we just discussed
+ In a truly objective analysis, background knowledge about whether the suspect confessed or whether the suspect had an alibit should not influence the systematic visual judgments that a fingerprint expert makes when matching ridge patterns between 2 prints
  + Yet fingerprint experts in the Mayfield case were biased toward Mr. Mayfield in their interpretation of the prints
+ Due to these research findings, a linear (as opposed to circular) processing of crime scenes has been added to the FBI's standard operating procedures
  + For example, in linear processing, fingerprinting experts would first analyze and document every aspect of the print found at the crime scene
  + After fully completing that task, the experts would then evaluate the degree of match between the print at the scene and the suspect's print more objectively
  + E.g., no going back and reinterpreting any ambiguities to fit what is already known about the suspects print

---
##  Confirmation bias: <br> Issues in medicine  

+ In trying to make a diagnosis, clinicians may first come up with a hypothesized diagnosis based on an initial evaluation and then search in a confirmatory manner for the features that would support that diagnosis (Reason, 1995)

???

+ Clinicians may also reason about their patients much like fingerprinting experts reason in a circular manner by using specific features of the suspect's print to hunt for corresponding features in the crime scene print
+ In trying to figure out what is wrong with a human system (i.e., making a diagnosis), clinicians may first come up with a hypothesized diagnosis based on an initial evaluation and then search in a confirmatory manner for the features that would support that diagnosis

---
##  Confirmation bias: <br> Issues in medicine  

+ Especially when trying to diagnose an ambiguous or difficult medical case, finding any confirmatory evidence for one's working hypothesis feels rewarding to the clinician (Croskerry, 2002)


+ Disconfirmatory evidence for that hypothesis indicates that putting in more mental effort is going to be necessary, or even that one must start from scratch in reasoning through the case

???

+ Especially when trying to diagnose an ambiguous or difficult medical case, finding any confirmatory evidence for one's working hypothesis feels rewarding to the clinician
+ Disconfirmatory evidence for that hypothesis, in contrast, indicates that putting in more mental effort is going to be necessary, or even that one must start from the beginning in reasoning through the case
  + Doesn't feel as good...disincentive
+ In addition, the confirmation bias in medical diagnosis may be particularly harmful when paired with an anchoring effect (chapter 3 of textbook)
  + E.g., a weak hypothesis formulated too early in the evidence-gathering stage may be misleadingly bolstered by a confirmatory search for additional supportive information

---
##  Confirmation bias: <br> Issues in medicine  

+ The only time clinicians may be chastised for susceptibility to confirmation bias is when they turn out to have been pursuing the wrong hypothesis


+ At other times, the exact same reasoning process may be praised for having been quite effective (Norman & Eva, 2010)

???

+ However, per our earlier discussion on positive and negative test strategies, the confirmation bias does not always lead to error
+ A clinician may even use a confirmatory strategy most of the time
  + And as long as the initial hypothesis is correct (which is reasonably likely, especially when starting with the most common diagnosis), there should be no problem with this approach
+ The only time a clinician may be chastised for susceptibility to confirmation bias is when they turn out to have been pursuing the wrong hypothesis
+ At other times, the exact same reasoning process (confirmatory strategy) may be praised for having been quite effective
+ As Tversky and Kahneman (1974) noted, heuristics are in general employed for efficiency and overall effectiveness, although under certain conditions they can produce error

---
##  Combating extreme confirmation bias: An approach to ideological extremism 

+ The cognitive bias most relevant to ideological extremism may be the confirmation bias (Lilienfeld et al., 2009)
  + General tendency to seek out information in confirmation of one's working hypothesis
  + Tendency to downplay or reinterpret evidence in disconfirmation of it


+ They argued that although much attention has been paid to documenting the existence of biases, relatively little attention has been paid to uncovering and understanding effective methods of debiasing

???

+ Some researchers argue the most deadly political movements worldwide in recent decades can be traced to ideological extremism
+ Of particular interest for our purposes is their proposal that the single cognitive bias most relevant to ideological extremism may be the confirmation bias
  + They cited the general tendency to seek out information in confirmation of one's working hypothesis
  + And the tendency to downplay or reinterpret evidence in disconfirmation of it
+ When political regimes attempt to encourage confirmation bias by presenting only information that supports a single point of view while actively suppressing and eliminating evidence for all other points of view, ideological extremism readily emerges
+ If this analysis is correct, then is makes sense for researchers to work toward the goal of learning how to counteract the confirmation bias and related biases
+ They argued although much attention has been paid to documenting the existence of biases, relatively little attention has been paid to uncovering and understanding effective methods of debiasing

---
#  Debiasing 

+ The confirmation bias can be attenuated when one is asked to come up with an alternative point of view, counter to the view that one has already chosen (e.g., Koriat, Lichtenstein, & Fischhoff, 1980)


+ Asking people to delay making a decision can reduce their susceptibility to the confirmation bias, presumably because they have been given more time to think critically and perhaps have more opportunities to encounter and consider alternative views (Spengler, Strohmer, Dixon, & Shivy, 1995)

???

+ So far, the most promising debiasing attempts have been designed to shift people from System 1 to System 2 processing using deliberate strategies to pull oneself away from the confirmation bias
+ For example, the confirmation bias can be attenuated when one is asked to come up with an alternative point of view, counter to the view that one has already chosen
+ Similarly, asking people to delay making a decision can reduce their susceptibility to the confirmation bias, presumably because they have been given more time to think critically and perhaps have more opportunities to encounter and consider alternative views

---
#  Bias blind spot 

+ The tendency to believe others are more prone to bias than oneself <br> (Pronin, Lin, & Ross, 2002)

<br>
.center[<img src="assets/img/image46.png" width=40%>]

???

+ That said, others have suggested that teaching people deliberate strategies is generally unhelpful
  + In part, because people very often have no idea they are biased when they are biased
+ In a similar attempt, Pronin and colleagues (2002) proposed people have a bias blind spot, where they tend to believe others are more prone to biases than themselves
+ In one study, participants were asked to read about 8 different biases
  + For each bias, they were asked to judge how much they themselves had the bias and how much the average person had of that bias
  + Half of the participants made judgments of themselves first, and the other half made judgments for average person first
  + Overall, participants rated themselves less susceptible to the bias than the average person
