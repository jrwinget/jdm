---
title: "Interpersonal Judgment & Attribution"
author: "PSYC/PHIL 279, Winget"
output:
  xaringan::moon_reader:
    css: xaringan-themer.css
    lib_dir: libs
    nature:
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: false
---

```{r xaringan-themer, include=FALSE, warning=FALSE}
library(tidyverse)
library(xaringanthemer)

style_mono_dark(
  base_color = "#4C837A",
  text_color = "#E1DDBF",
  background_color = "#04263C",
  title_slide_text_color = "#FFFFFF",
  text_slide_number_color = "#04263C",
  colors = c(
  red = "#FF0000",
  blue = "#00FFFF",
  purple = "#FF00FF"
  ),
  extra_css = list(
    ".large-text" = list("font-size" = "150%"),
    ".small-text" = list("font-size" = "75%")
  )
)
```

##  Inferring dispositions from behavior 

+ We care a lot about why other people do what they do


+ The problem is that a person's inner self is hidden from view


+ Therefore, we are forced to infer motives and intentions from people's words and actions

<br>
<br>
<br>
.center[*One will seldom go wrong if one attributes extreme actions to vanity, average ones to habit, and petty ones to fear*]


.right[*- Friedrich Nietzsche (1886/1984, p. 59)*]

???

+ Today, we turn our attention to interpersonal attribution...or the process of inferring a person's character from their behavior
+ We spend a ton of our time engaging in interpersonal attribution
+ It's something lay people and philosophers alike have focused their energy and attention on
+ As Nietzsche once said...
+ 
+ We care a lot about why other people do what they do
+ But, people care less about what others do than about why they do it
+ Two equally rambunctious nephews may break two equally expensive vases as Aunt Sofia's house
+ But the one who did so by accident gets the reprimand and the one who did so by design gets severally punished
+ The problem, of course, is that a person's inner self is hidden from view
+ Character, motive, belief, desire, and intention play leading roles in people's construal of others, and yet none of these constructs can actually be observed
+ As such, people are forced into the difficult business of inferring these intangibles from that which is, in fact, observable...other people's words and deeds

---
#  From acts to dispositions 

+ We want to know what's inside other people feelings, motives, dispositions


+ How do we figure this out?


+ Attribution theory!

???

+ How do we go from what a person does to who they are...personality-wise?
+ If an acquaintance says, "It's great to see you!" do they really mean it? 
+ They could be acting more thrilled than they really feel out of politeness...OR they could be lying and can't stand you
+ The point is that even though nonverbal communication is sometimes easy to decode and our IMPLICIT PERSONALITY THEORIES CAN STREAMLINE the way we form impressions...there is still substantial AMBIGUITY AS TO WHAT A PERSON'S BEHAVIOR REALLY MEANS
+ SOMEONE KNOCKS INTO YOU AND YOU DROP YOUR BOOKS
  + WHY DID SHE RUN IN TO YOU?...Intentional? Accidental?
+ To answer this "why" question, we use IMMEDIATE OBSERVATIONS to form complex INFERENCES about what people are really like....what motivates them to act as they do
+ How we go about answering these questions is the focus of ATTRIBUTION THEORY...the study of how we infer the causes of other people's behavior

---
#  Social perception 

+ Heider & Simmel animation

.center[
<video width="700" height="400" controls>
    <source src="assets/heider.mp4" type="video/mp4">
</video>
]

???

+ Watch animation and tell me what you see
+ So what did you make of it? (get several reactions)
+ At some level, you're all wrong
+ ISN'T IT TRUE THAT AT A LITERAL LEVEL WE JUST SAW SHAPES GOING IN AND OUT OF A BOX?
+ Purpose of demonstration is to show how we attribute feelings and explanations to everyday things...a bunch of objects
+ It looks at how much we are WILLING TO GO BEYOND THE INFORMATION WE ARE GIVEN
+ We assume people do what they do b/c of internal nature rather than context or situationally driven Behavior

---
##  Attribution theory: <br> The all important "why" question 

+ How do we explain the causes of other people's behavior?


+ Why is it important to do so?


+ Attributions to **.blue[internal]** versus **.blue[external]** factors

.center[
<img src="assets/img/image36.jpeg" height=300>
<img src="assets/img/image37.jpeg" height=300>
]

???

+ Fritz Heider...the father of attribution theory...discussed "COMMONSENSE" psychology
+ People were like AMATEUR SCIENTISTS, trying to understand other 's behavior by piecing together info until they arrived at a reasonable explanation or cause
+ One of Heider's most valuable contributions is a simple dichotomy
+ When trying to decide WHY people behave as they do...EX why a father has just yelled at his daughter...we can make 1 of 2 attributions
  + 1) INTERNAL ATTRIBUTION, decide the cause of the father's behavior was SOMETHING ABOUT HIM—disposition, personality, attitudes, character
      + assigns the causes of his behavior internally
      + Ex: decide that the father has poor parenting skills
  + 2) EXTERNAL ATTRIBUTION, decide something in the SITUATON, not in the father's personality or attitudes, caused behavior
      + Conclude he yelled because his daughter just stepped into the street without looking, we would be making an external attribution for his behavior
+ HOW WE ANSWER THIS QUESTION DETERMINES HOW WE RESPOND

---
#  Kelley's attribution theory

+ "Why did Joe kick Fido?"


+ **.blue[Consensus Information:]** To what extent do other people behave the same way toward the same stimulus?
  + *Do other people kick Fido?*

<br>
.center[
<img src="assets/img/image38.png" height=200>
<img src="assets/img/image41.png" height=200>
]

.center[]

???

+ Let's say we observe someone, let's call them Joe, kick a dog named Fido
+ When trying to decide if Joe's behavior is something about JOE...say his personality...we make an internal attribution
+ If we decision that it's something about FIDO or the situation, we make an external attribution
+ But, to make one attribution or the other, according to Kelley's attribution theory, we look at 3 kinds of information...(should seem familiar)
+ First, we consider consensus information...do other people behave the same way towards Fido?
+ SO WE HOLD STIMULUS (FIDO) CONSTANT
+ If everyone kicks Fido = high consensus			
+ But if no one else kicks Fido = low consensus
+ Other example: you ask your friend to lend you her car and she says no...Why?
+ Does everyone else refuse to lend you their car?...If yes, HIGH consensus
+ Or is she the only one?...If yes, LOW consensus

---
#  Kelley's attribution theory 

+ **.blue[Distinctiveness Information:]** To what extent does the actor behave the same way toward other stimuli?
  + *Does Joe kick other dogs?*

.pull-right[.center[<img src="assets/img/image43.jpeg" height=200>]]

.center[
.pull-left[<img src="assets/img/image44.jpeg" width=80%>]
.pull-right[<img src="assets/img/image38.png" height=200>]
]

???

+ Next we consider distinctiveness information...does Joe kick other dogs?
+ NOW WE HOLD ACTOR (JOE) CONSTANT AND SEE HOW HIS Behavior VARIES ACROSS OTHER DOGS
+ If Joe does kick all types of dogs = low distinctiveness		
+ But, if Joe only kicks Fido = high distinctiveness
+ For the car example, we'd ask....Does your friend refuse to lend her car to everyone else (low distinctiveness), or just to you (high distinctiveness)?

---
#  Kelley's attribution theory 

+ **.blue[Consistency Information:]** Does the actor behave in the same way toward that stimulus, at different times and in different situations?
  + *Does Joe always kick Fido?*

<br>
<br>
<br>
.center[<img src="assets/img/image66.png" width=100%>]

???

+ The last type of information we consider is consistency information...does Joe always kick Fido, even at different times and in different situations?
+ Now we hold actor (Joe) and stimulus (Fido) constant and see how it varies ACROSS SITUATIONS
+ DOES Joe ONLY KICK FIDO WHEN HE IS ON HIS WAY TO THE BATHROOM IN THE MIDDLE OF THE NIGHT? (if so, time changes the behavior)
+ DOES Joe KICK FIDO ONLY WHEN THEY DON'T HAVE COMPANY? (if so, the situation changes the behavior)
+ So, if Joe only kicks Fido at night or alone = LOW consistency
+ But if Joe kicks Fido in all of the situations all the time = HIGH consistency 
+ For the car example, we'd ask...does your friend always refuse to lend you her car, or does she ONLY do so when it's SNOWING AND DARK?

---
#  Internal vs. external attributions 

+ Internal attribution if...
  + *Low* Consensus ("no one else kicks Fido")
  + *Low* Distinctiveness ("Joe kicks all dogs")
  + *High* Consistency ("Joe always kicks Fido")


+ External attribution if...
  + *High* Consensus ("Everyone kicks Fido")
  + *High* Distinctiveness ("Joe kicks only Fido")
  + *High* Consistency ("Joe always kicks Fido")

???

+ This is a very LOGICAL process of attributions
+ Requires that people have a lot of information
+ THESE 2 PATTERNS HAVE TO OCCUR IN ORDER TO DEVELOP A CLEARLY INTERNAL OR EXTERNAL ATTRIBUTION
+ Read slide for Joe example
+ IT'S ABOUT YOUR FRIEND—INTERNAL to her 
  + Low (no one else refuses to lend you their car)
  + Low (your friend refuses to lend others her car)
  + High (your friend refuses to lend you things every chance she gets)
+ OR EXTERNAL
  + High (everyone else refuses to lend you their car)
  + High (your friend lends others her car)
  + High (your friend refuses to lend you things every chance she gets)

---
#  Kelley's attribution theory 

+ What if consistency is Low?
  + Situational attribution
      + Unique occurrence is difficult to classify
      + External Attribution often made to the particular circumstances

<br>
.right[<img src="assets/img/image46.jpeg" width=35%>]

???

+ If we don't have enough information about consistency (whether it's constant across situations), then we likely make a special kind of external attribution...a situational attribution 
+ These cases are difficult to classify because something unique in the situation is occurring
+ So, we typically make an external attribution about these cases that assumes SOMETHING STRANGE IS GOING ON in the situation
+ e.g., let's say this morning I saw Joe pet Fido...he only kicked him in the afternoon
+ It's possible in the afternoon that Fido thought Joe was the mail carrier and attacked....Joe kicked in self-defense
+ Here, then we have a A CASE OF MISTAKEN IDENTITY...perhaps due to the time Joe arrived in the afternoon...(maybe it was around the time the mail normally arrived)
+ Or for the car example...let's say it's the first time that your friend refused to lend you her car...maybe there are problems at home or her car is broken

---
#  Correspondent inference theory 

+ Logically, there must be some necessary precursors before internal dispositional inferences can be made
  + Free choice, No social desirability


+ Correspondence bias (FAE)
  + Tendency to infer that people's behavior *matches* their disposition (personality)

<br>
.center[<img src="assets/img/image47.jpeg" width=50%>]

???

+ Logically, there must be some necessary precursors before internal dispositional inferences can be made
  + e.g., people need to be able to make their own choices to guide their behavior
  + They also need to act the way they due without pressure from others...no social desirability
+ When these precursors are present, we're likely to engage in the correspondence bias
+ The correspondence bias is the tendency to infer that people's behavior matches their dispositon (or personality)
+ The correspondence bias is so pervasive that many social psychologists call it the fundamental attribution error
+ We CAN'T SEE THE SITUATION, so we ignore its importance
+ PEOPLE, not the situation, have PERCEPTUAL SALIENCE for us
+ And this perceptual salience is 1 reason for correspondence bias
  + The seeming importance of info that is the focus of attention
+ We pay attention to PEOPLE, and tend to think that they alone cause their behavior
+ The culprit is one of the mental shortcuts we discussed in chapter 3: 
  + the anchoring and adjustment heuristic
+ The correspondence bias is another byproduct of this shortcut.
+ When making attributions, people USE THE FOCUS OF THEIR ATTENTION AS A STARTING POINT

---
#  Correspondent inference theory

+ Participants given essays to read that had been written by another student (Jones & Harris, 1967)
  + Essay was anti- or pro-Castro
  + People told the author either had *choice* or *no-choice* on which position to take


+ Asked to guess author's true attitude toward Castro

.right[<img src="assets/img/image48.png" width=25%>]

???

+ We tend to determine personality from Behavior
+ In one study that tested this idea, participants were given essays to read that had been written by another student (Jones & Harris, 1967)
  + Essay was anti- or pro-Castro
  + Ps told the author either had *choice* or *no-choice* on which position to take
+ Asked to guess author's true attitude toward Castro

---
#  Results of Jones & Harris (1967)

.center[<img src="assets/img/image49.png" width=100%>]

???

+ Results are for guesses about Author's Attitude
+ This was easy to do in CHOICE condition...In this condition, Ps were told the authors chose their position, so they must believe it
+ But this was more difficult in no choice condition 
+ Rationally, we should remember that the POSITION THEY ADVOCATE TELLS US NOTHING ABOUT THEIR ACTUAL POSITION b/c they were FORCED to write about it
+ But...In no choice condition, ASSUMED WRITER HELD ATTITUDE THAT THEY EXPRESSED
+ We assume that people do things based on who they are or what they feel EVEN IF THEIR ACTIONS ARE CONSTRAINED

---
#  Correspondence bias 

+ People demonstrate a clear bias to perceive a correspondence between behavior and attitudes


+ We expect that one's values, attitudes, beliefs, and opinions will determine behavior
  + She acts friendly therefore she IS friendly


+ "Causal misattribution"

.center[
<img src="assets/img/image50.png" height=250>
<img src="assets/img/image51.png" height=250>
<img src="assets/img/image52.png" height=250>
]

???

+ People demonstrate a clear bias to perceive a correspondence between behavior and attitudes
+ We expect that one's values, attitudes, beliefs, and opinions will determine behavior
  + She acts friendly therefore she IS friendly
+ This is part of reason for causal misattribution
+ Causal misattribution is when a belief is so strong that we OFTEN MISPERCEIVE THE ACTUAL CAUSE OF A BEHAVIOR
+ BEHAVIOR corresponds to their PERSONALITY...She acts friendly, therefore she IS friendly
+ Use celebrities picture to walk through the friendly example
+ One is friendly the other is not, ignore situational constraints on behavior
+ Perhaps Timothee was in a hurry so that's why he didn't smile back, etc.
+ Assume act corresponds to what is inside

---
#  Using mental shortcuts to make attributions 

+ **.blue[Fundamental attribution error (FAE):]** Tendency to overestimate dispositions (personality) as causes of behavior and underestimate situational influences
  + At heart, people are personality theorists

<br>
.center[<img src="assets/img/image53.png">]

???

+ Fundamental attribution error (FAE) is the tendency to overestimate dispositions (personality) as causes of behavior and underestimate situational influences
  + At heart, people are personality theorists
+ You read about this in Chapter 1
+ Editorials about the Genovese murder committed the FAE
+ They said it was something about the personality of Kitty's neighbors that kept them from helping, but Darley and Latane showed it was really something about the situation

---
#  Reasons for the FAE

+ Perceptual salience of actor


+ Two-step process of attribution
  1. Assume it's something about the person
  1. Correct for the situation

.center[<img src="assets/img/image54.jpeg">]

???

+ BEFORE: Part of the reason for the FAE is because of our visual POV
+ Visual POV lends itself to bias
+ POV leads me to believe it's something about you...I'm not watching the surrounding
+ Perceptual salience of the actor is important
  + Studies of taped conversation show we make attributions based on how we see the scene
+ If you want to be seen as in charge of a situation, sit so that others can see you
+ 2-step process: 1) person...2) correct for situation
+ Implications for times that we are tired or busy
+ Effortful correction can break down...so we're left with assuming it's something about the person

---
#  Gilbert, Pelham, & Krull (1988) 

+ Participants observed a person behave anxiously in an anxiety-provoking situation or a non-anxiety provoking situation


+ Control subjects just watched the tapes


+ Cognitively busy subjects watched the same tapes while simultaneously attempting to memorize the topics that the anxious woman was discussing

???

+ In one study of the FAE...
+ Participants were shown a silent videotape of an anxious-looking woman who was ostensibly discussing with an off-camera stranger either a series of anxiety-provoking topics (e.g., sexual fantasies, embarrassing moments) or a series of mundane topics (e.g., gardening)
+ Participants were instructed to diagnose the woman's dispositional anxiety
+ Cognitively busy subjects watched the same tapes while simultaneously attempting to memorize the topics that the anxious woman was discussing
+ The idea was that such participants would rehearse the topics...and that doing so would impose a cognitive load that would keep them from correcting their initial characterizations of the woman. 
  + She's behaving anxiously, so she's anxious, etc.

---
#  Gilbert, Pelham, & Krull (1988) 

```{r gph1, echo=FALSE, fig.align='center', fig.height=7, fig.width=10}
palette <- c("#E69F00", "#56B4E9")
cond1 <- c("Relaxing", "Relaxing", "Anxious", "Anxious")
cond2 <- c("Control", "Cognitive Load", "Control", "Cognitive Load")
disp_anx <- c(10.31, 9.28, 7.79, 8.88)
gph1 <- data.frame(cond1, cond2, disp_anx)

gph1 %>% 
  ggplot(aes(factor(cond1, levels = c("Relaxing", "Anxious")), 
             disp_anx, fill = factor(cond2, levels = c("Control", "Cognitive Load")))) +
  scale_fill_manual(values = palette) +
  geom_col(position = "dodge") +
  expand_limits(y = c(0, 11.5)) +
  labs(
    title = "",
    x = "",
    y = "Ratings of Dispositional Anxiety",
    fill = ""
  ) +
  theme_xaringan() +
  theme(
    title = element_text(size = 24),
    axis.text = element_text(size = 20),
    legend.title = element_text(size = 20),
    legend.text = element_text(size = 16)
  )
```


???

+ Control participants did just what any reasonable person would do: 
  + They concluded that the woman who looked nervous while discussing topic that evoke anxiety was not nearly so anxious a person as was the woman who looks equally nervous while discussing gardening
+ Participant under cognitive load attributed the same amount of anxiety to the woman in both the anxious-topic and the mundane-topic conditions...as if they had been unable to consider the attributional implications of the very information they were rehearsing
+ This study suggests that when people know about the actor's situation and are instructed to diagnose the actor's dispositions, they do indeed characterize before they correct...and that situational correction is more easily impaired than is dispositional characterization

---
#  Gilbert's model 

<br>
<br>
<br>
.center[<img src="assets/img/image67.png" width=100%>]

???

+ Here is Gilbert's model of attribution
+ The first thing to note is that we automatically categorize behavior and apply a dispositional characterization
  + She looks anxious; she's an anxious person. 
+ Only if we have sufficient time and motivation and ability do we potentially correct for this initial dispositional attribution by taking into account situational constraints or other information that would lead us to make a less dispositional and more situational attribution
+ Basically, we jump to dispositional conclusions and only later correct them

---
##  Actor/observer differences in attribution 

+ We have more information about ourselves than observers do


+ People are more apt to see others' behavior as dispositionally caused than their own


+ We commit the FAE more when explaining other people's behavior than our own

<br>
.center[<img src="assets/img/image55.png">]

???

+ We have more information about ourselves than observers do
+ People are more apt to see others' behavior as dispositionally caused than their own
+ We commit the FAE **more** when explaining other people's behavior than our own
+ This matters even for subtle things like positioning a camera angle in police interrogation...if focused on suspect, we're more likely to find them GUILTY
  + But, if it's focused on both interrogator and suspect, we have MORE DOUBT ABOUT THEIR GUILT
+ Actor/Observer differences: follows visual P.O.V. 
+ When I trip, I focus on the slippery floor (situation)
+ You focus on me being clumsy (person)
+ When I bump into someone and cause them to drop their books, I'm focusing on the crack in the sidewalk that made me trip and they are focusing on me knocking their books out of their hands

---
background-image: url('assets/img/image68.png')
background-size: contain

???

+ Storms used a design similar to Fiske & Taylor to investigate the ROLE OF PERCEPTUAL SALIENCE in actor/observer differences
+ 2 PEOPLE sat down to talk (Fiske & Taylor)
+ 2 people OBSERVED the conversation (bubbles & blossom)...placed so they could see one actor or the other better
+ 2 CAMERAS also recorded one actor's face or the other actor's face
+ After the conversation, the 4 participants were asked to what extent was the ACTOR'S B DUE TO PERSONAL CHARACTERISTICS VS. CHARACTERISTICS OF THE SITUATION (e.g., topic of conversation, B of partner)
+ ACTORS made attributions about THEMSELVES
+ OBSERVERS made attributions about THE PERSON THEY WATCHED
+ And this is what we would EXPECT...When they only engage in or watch conversation and then make attributions, OBSERVERS should explain it more in terms of the ACTOR'S DISPOSITION while ACTORS should explain it more in terms of the SITUATION (FAE)

---
#  Storms (1973) 

```{r gph2, echo=FALSE, fig.align='center', fig.height=7, fig.width=10}

cond1 <- c("No Tape", "No Tape", "Tape Same POV", "Tape Same POV", "Tape Different POV", "Tape Different POV")
cond2 <- c("Actors", "Observers", "Actors", "Observers", "Actors", "Observers")
disp_attrb <- c(2.2, 4.8, 0.2, 4.9, 6.9, 1.7)
gph2 <- data.frame(cond1, cond2, disp_attrb)

gph2 %>% 
  ggplot(aes(factor(cond1, levels = c("No Tape", "Tape Same POV", "Tape Different POV")), 
             disp_attrb, fill = factor(cond2, levels = c("Actors", "Observers")))) +
  scale_fill_manual(values = palette) +
  geom_col(position = "dodge") +
  expand_limits(y = c(0, 7)) +
  labs(
    title = "",
    x = "",
    y = "Dispositional Attributions",
    fill = ""
  ) +
  theme_xaringan() +
  theme(
    title = element_text(size = 24),
    axis.text = element_text(size = 20),
    legend.title = element_text(size = 20),
    legend.text = element_text(size = 16)
  )
```

???

+ And this is what we find
+ No Tape: When they just thought about the conversation, Participants showed the same actor/observer bias as before
  + (FAE) OBSERVERS explained it more in terms of the ACTOR'S DISPOSITION while ACTORS explained it more in terms of the SITUATION (FAE)
+ Tape Same POV: Some Ps were shown videotapes of the conversation before their ratings
  + Some saw ON TAPE what they had EXPERIENCED LIVE (actor saw partner's face; observer saw original actor's face)
  + Showed same FAE effect
+ Tape Different POV: Other Ps were shown videotape with opposite POV
+ ACTORS A AND B SAW THEIR OWN FACE...Object of PERCEPTUAL SALIENCE was REVERSED
+ After looking at their own face, actors made MORE DISPOSITIONAL attributions about themselves
+ After seeing their CONVERSATION PARTNER'S SITUATION, they made MORE SITUATIONAL attributions ABOUT THEIR PARTNER
+ Reverses the A/O effect b/c OBJECT OF PERCEPTUAL SALIENCE DIFFERS
+ ACTOR/OBSERVER OCCURS B/C OF PERCEPTUAL SALIENCE
+ (Look at real world example from the media)

---
#  False consensus effect 

+ We tend to think people share our opinions, attitudes, beliefs and would behave as we do in situations


+ This may lead to the perception of a false consensus, a consensus that in fact does not exist

???

+ We tend to think people share our opinions, attitudes, beliefs and would behave as we do in situations
+ This may lead to the perception of a false consensus, a consensus that in fact does not exist

---
#  False consensus 

+ Tendency to see one's own choices and opinions as more common than they are


+ **.purple[31%]** of class said they would pay the fine
  + Those who said they would pay estimated that
      + **64%** would pay
      + **36%** would contest


+ **.purple[69%]** of class said they would contest the charge
  + Those who said they would contest estimated that
      + **58%** would contest
      + **42%** would pay

???

+ This is class demo data. You need to compute these yourself based on responses to the demo handed out at the beginning or end of the second moral judgment lecture.
+ Those who said they would pay thought a majority would also pay; those who would contest also thought a majority would contest
+ We tend to think others think the way we do
+ Both felt others would do as they did
+ Ask for your music preference or attitudes and see this bias
+ You construe situation in a particular way and assume others will too. If we paid the fine, then other people surely would too. And vice versa.

---
#  Reasons for false consensus 

+ Accessibility
  + Our own opinions more at the forefront of our minds
  + Our construals of the event are highly accessible, alternative ways of construing the event are not


+ Evidence
  + False consensus less likely to occur when there is less room for interpretation of the event
  + Less ambiguous, more likely that most people construe it in the same way

???

+ There was some ambiguity to scenario (uncertainties about effort involved in contesting ticket) so we made some assumptions. 
+ The more AMBIGUITIES, the more likely you are to get false consensus effects
+ Assume others assume same things. 
+ Ex. How many like 60's rock and roll? Do you think of the Beetles or acid rock? Do I like it? Do others?
+ If more specific, effect diminishes, suggesting a social cognition aspect to the effect
+ Might try to adjust, but perhaps don't adjust enough. Social Cognition approach

---
#  Pluralistic ignorance 

+ Pluralistic ignorance is a psychological state characterized by the belief that one's private attitudes and judgments are different from those of others, even though one's public behavior is the same

.center[<img src="assets/img/image61.png" width=55%>]

???

+ Pluralistic ignorance is a psychological state characterized by the belief that one's private attitudes and judgments are different from those of others, even though one's public behavior is the same
+ It usually develops most commonly under circumstances in which there is widespread misrepresentation of private views
+ In these cases, people's tendency to rely on the public behavior of others to identify the norm leads them astray
+ The social norm that is communicated misrepresents the prevailing sentiments of the group

---
#  Pluralistic ignorance 

.center[<img src="assets/img/image62.png" width=100%>]

???

+ One such case is a classroom dynamic investigated by Miller and McFarland (1987, 1991). 
+ The situation is as follows: A professor who has just presented difficult material will typically ask students if they have any questions. This request for students to acknowledge their confusion often fails to elicit a response, even though confusion is widespread. 
+ The students' inaction is driven by pluralistic ignorance: Individual students are inhibited from raising their hands out of fear of asking a stupid question, but they interpret their classmates' identical behavior as an indication that everyone else understands the material. 
+ In this situation, pluralistic ignorance will not be resolved by students deciding that they actually do understand the material. They have ample and irrefutable evidence that they do not understand it. Instead, pluralistic ignorance will likely persist, leaving students feeling deviant and alienated from each other.

---
#  Pluralistic ignorance 

+ Alcohol use by college undergraduates has become a major concern of university administrators and public health officials across the country

<br>
<br>
<br>
.right[<img src="assets/img/image63.png" width=55%>]

???

+ Alcohol use by college undergraduates has become a major concern of university administrators and public health officials across the country. 
+ A powerful predictor of alcohol use among college students is peer influence. 
+ The alcohol situation on many campuses is exacerbated by the central role of alcohol in many social activities and gathering around campus. 
+ There are often strong norms promoting alcohol use on many campuses around the country. 

---
#  Pluralistic ignorance 

+ Prentice and Miller (1993) investigated the role of pluralistic ignorance in students' alcohol consumption

<br>
<br>
<br>
.right[<img src="assets/img/image64.png" width=60%>]

???

+ In one study Prentice and Miller asked college students their own comfort with the drinking habits of students at the school and how comfortable average students felt with the drinking habits of students at the school

---
#  Pluralistic ignorance 

+ Prentice and Miller (1993) found clear evidence of pluralistic ignorance

<br>
<br>
```{r tbl1, echo = FALSE}
tbl1 <- tibble::tribble(
~`Measure`, ~`Self`, ~`Average Student`,
"Women","4.68","7.07",
"Men","6.03","7.00",
"Total","5.33","7.04"
)

tbl1 %>%
  knitr::kable(format = "html",
               escape = FALSE,
               align = c("lcccccccc")) %>%
  kableExtra::kable_styling(font_size = 24)
```

???

+ Both men and women reported being more uncomfortable with the drinking habits of students on campus than they thought the average student was. 
+ Again, students assume that everyone on campus loves the drinking habits and climate, but in fact most students are less enamored with it. 
+ Said a slightly different way: Students believed that they were more uncomfortable with campus alcohol practices than was the average student.
