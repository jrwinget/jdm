---
title: "Moral Judgment I"
subtitle: "Utilitarianism vs. Deontology"
author: "PSYC/PHIL 279, Winget"
output:
  xaringan::moon_reader:
    css: xaringan-themer.css
    lib_dir: libs
    nature:
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: false
---

```{r xaringan-themer, include=FALSE, warning=FALSE}
library(tidyverse)
library(xaringanthemer)

style_mono_dark(
  base_color = "#4C837A",
  text_color = "#E1DDBF",
  background_color = "#04263C",
  title_slide_text_color = "#FFFFFF",
  text_slide_number_color = "#04263C",
  colors = c(
  red = "#FF0000",
  blue = "#00FFFF",
  purple = "#FF00FF"
  ),
  extra_css = list(
    ".large-text" = list("font-size" = "150%"),
    ".small-text" = list("font-size" = "75%")
  )
)
```

background-image: url('assets/img/image02.png')
background-size: contain

???

+ Today, we'll start our discussion on moral judgment
+ We know morality has to do with right and wrong
+ But when we think about it influencing our judgments, you can think of morality as having two basic questions
+ One is a descriptive scientific question: 
  + How does moral thinking work?...Why are we the kinds of moral creatures that we are?...Why are we immoral to the extent that we're immoral? 
+ And then the further normative question: 
  + How can we make better moral decisions?...Is there any fact of the matter about what's right or wrong at all?...If so, what is it?...Is there a theory that organizes, that explains what's right or what's wrong? 
+ We'll explore both of these questions through the lens of both philosophy and the scientific literature
+ 
+ Before we dive in, it's worth pointing out how complicated moral judgments can be
+ Consider this comic of Calvin and Hobbes, where Calvin is stuck with the moral dilemma to cheat on a test or not
+ He talks about the rightness and wrongness of the choices, the satisfaction that can be derived, how often other people might do this sort of thing, the larger consequences of cheating, and so on
+ He does all of this to the point where he ran out of time to even take the test...an ethics test at that
+ Sometimes, it might seem like we're not able to study this topic scientifically because, on the surface, it seems morality is such a subjective concept
+ But this simply isn't true
+ While the content of what is moral or immoral might change from person to person or culture to culture...we can systematically study the underlying processes that lead to moral judgments and decision making

---
#  Moral judgment and choice 

+ Moral thinking is important for decision making as a whole, because most real decisions involve moral issues, at least because they affect other people


+ The most basic moral judgments are statements about what *decisions* someone in a certain situation should make

<br>
<br>
.center[
<img src="assets/img/image03.png" height=200> &nbsp;
<img src="assets/img/image04.png" height=200>
]

???

+ Now, moral thinking is important for decision making, as a whole, because most real decisions involve moral issues...at least because they affect other people
+ The choice of one's work, for example, is often considered to be a purely personal decision
+ But...we can do various amounts of good or harm to other by choosing different paths through our work life
+ e.g., On the one had, we have the saintly aid worker who risks death in order to fight an epidemic in a poor region...but, on the other hand, we have the Mafia Don who bleeds the rich and poor alike
  + And these are only a couple examples of the extremes of a continuum on which all of us are placed
  + Most of us probably fall somewhere in the middle...trying to do good but making some mistakes along the way
+ As another example, consider personal relationships
  + Personal relationships only become so “personal” when they involve promises that are kept and broken, expectations of loyalty, and so forth
  + When they lack these qualities, the relationships seem less personal...underscoring the importance of how the moral issues affect our close relationships
+ Of course, certain issues involve much more obvious moral questions...especially those that arouse political passion...like abortion, the death penalty, aid for the poor, etc.
+ At its core, the most basic moral judgments are statements about what *decisions* someone in a certain situation should make


---
#  What are moral judgments? 

+ Morality involves telling each other what we should do or otherwise trying to influence each other's decision making
	+ Teaching principles
	+ Setting an example
	+ Gossiping
	+ Rewarding or punishing behaviors

<br>
.right[<img src="assets/img/image05.png" width=400>]

???

+ Morality involves telling each other what we should do, or otherwise trying to influence each other's decision making
+ We can express morality, as a way of influencing others, in several ways: 
  + Teaching principles directly by explaining them or by expressing moral emotions...(saying we shouldn't steal, getting angry when this happens)
  + Setting an example of how to behave...(modeling good behavior)
  + Gossiping (when we gossip, we express moral principles by assuming them when we criticize others)...(people gossip about affairs or cheating in relationships to criticize the cheater)
  + Rewarding behavior that follows the principles and punishing behaviors that violate them...(child rewarded for good behavior..or society punishing bad behavior by fines/imprisonment)

---
#  Moral statements 

+ Imperatives 
  + "You should not steal' resembles, in usage, the statement "Do not steal"


+ Impersonal
  + They are meant to apply to anyone who is in a certain circumstance
  + Want to regulate conduct concerning issues that concern us all

???

+ Now, I mentioned that the most basic moral judgments are statements about what *decisions* someone in a certain situation should make
+ These moral statements are essentially imperatives...they're commands that tell us what to do or not to do
  + The statement “You should not steal” resembles, in usage, the command “Do not steal”
  + They're commands in that in order to be moral we **have** to follow them
  + If we had a statement of that said, "You should be kind to animals" it wouldn't make no sense to say, “You *should* be kind to animals...but don't be”
  + According to this statement, in order to be moral, we have to be kind to animals
+ Moral statements are also impersonal in that they are meant to apply to anyone who is in a certain circumstance
  + This is because we want them to regulate conduct concerning issues that concern us all
  + When we try to influence people morally, we try to influence some standard they hold for how they should behave in certain circumstances
  + We try to influence the principles they follow and try to influence not just one person, but many people

---
#  Types of moral judgment 

+ Morality versus convention


+ Social norms

<br>
.center[
<img src="assets/img/image06.png" height=250> &nbsp;
<img src="assets/img/image07.png" height=250>
]

???

+ Moral judgments can be understood by comparing them to closely related judgments...with which they are often confused
+ The analysis of types of judgment grew out of the work of Lawrence Kohlberg (1970), who, in turn, was influenced by Jean Piaget
+ One type of judgment are social conventions...these are judgments about what is expected or approved or condoned by law
+ Conventions can be changed...but moral rules cannot be changed
  + e.g., laws change, but the moral duty to help someone when injured doesn't
+ Morality and convention can conflict...as in the case of Heinz who needs a drug to save his wife's life, but the pharmacist is charging way too much
  + Morally speaking, stealing the drug is considered the right thing to do even though it breaks convention
+ Moral judgments can also come in the form of social norms
+ Examples of social norms are fashion or dress (some of which require considerable effort to follow)...norms of politeness...and codes of ethics (such as those that forbid cheating on exams, etc.)
+ We follow these norms because of what others think...unlike moral rules which we think should be followed regardless of what others think

---
##  Utilitarianism as a normative model 

+ The basic idea of modern utilitarianism is that we treat each moral decision as a choice among competing actions


+ Each action has certain consequences, with different probabilities, for certain people


+ We endorse the action that leads to the greatest good for the greatest number of people

<br>
.right[<img src="assets/img/image08.jpg" width=200>]

???

+ Now, there are models of morality...and utilitarianism is one of the more popular/famous ones
+ Jeremy Bentham is widely considered the founder of modern utilitarianism...later it was championed by John Stuart Mill
+ The basic idea of utilitarianism is that we treat each moral decision as a choice among competing actions
+ Each action has certain consequences...with different probabilities...for certain people
+ We endorse the action that leads to the greatest good for the greatest number of people
+ In other words, to decide which action is morally best, we simply add up the expected utilities of the consequences for all of the people
+ The best acts are those with the highest expected utility, across all of the people

---
#  Utilitarianism: Problems 

+ Utilitarianism also has trouble accounting for values such as justice and individual rights


+ Also, it is often difficult, if not impossible to measure and compare the values of certain benefits and costs

<br>
.center[<img src="assets/img/image09.jpg" width=500>]

???

+ Utilitarianism also has trouble accounting for values such as justice and individual rights
+ For example, assume a hospital has four people whose lives depend upon receiving organ transplants...a heart, lungs, a kidney, and a liver
  + If a healthy person wanders into the hospital, his organs could be harvested to save four lives at the expense of one life
  + This would arguably produce the greatest good for the greatest number
  + But few would consider it an acceptable course of action...let alone the most ethical one
+ Also, the utilitarian calculation requires that we assign values to the benefits and harms resulting from our actions and compare them with the benefits and harms that might result from other actions
+ But it's often difficult, if not impossible, to measure and compare the values of certain benefits and costs
+ How do we go about assigning a value to life or to art?
+ And how do we go about comparing the value of money with, for example, the value of life, the value of time, or the value of human dignity?
+ Also, can we ever be really certain about all of the consequences of our actions?
+ Our ability to measure and to predict the benefits and harms resulting from a course of action or a moral rule is dubious, to say the least
+ So, although utilitarianism is arguably the most reason-based approach to determining right and wrong, it has obvious limitations

---
#  Utilitarianism: Conclusions 

+ Utilitarianism can play a role in moral decisions


+ It invites us to consider the immediate and less immediate consequences of our actions


+ Utilitarianism asks us to look beyond self-interest to consider impartially the interests of all persons affected by our actions

???

+ If our moral decisions are to take into account considerations of justice, then apparently utilitarianism cannot be the sole principle guiding our decisions
+ It can, however, play a role in these decisions
+ The principle of utilitarianism invites us to consider the immediate and the less immediate consequences of our actions
+ Given its insistence on summing the benefits and harms of all people, utilitarianism asks us to look beyond self-interest to consider impartially the interests of all persons affected by our actions 
+ As John Stuart Mill once wrote:
  + The happiness which forms the utilitarian standard of what is right in conduct, is not...(one's) own happiness, but that of all concerned
  + As between his own happiness and that of others, utilitarianism requires him to be as strictly impartial as a disinterested and benevolent spectator

---
#  Deontological rules 

+ Deontology refers to rules of duty or obligation, or rules that concern what to do and what not to do, rather than the value of consequences

<br>
.center[<iframe width="560" height="315" src="https://www.youtube.com/embed/wWZi-8Wji7M" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>]

???

+ The general alternative to utilitarianism is deontology...a term that refers to rules of duty or obligation...or rules that concern what to do and what not to do, rather than the value of consequences
+ Many deontological rules prohibit actions that cause some harm...even if the action prevents a greater harm
+ For example, such a rule might say that it is wrong to bomb civilians in order to terrorize an enemy into surrender...even if doing so would save many more lives in the long run
+ Some pacifists oppose war, even if the purpose of the war is to prevent someone else from waging a more destructive war

---
#  Deontological rules 

+ Many modern deontologists trace their roots to Immanuel Kant's (1785) categorical imperative


+ Moral principles are required in all cases, not dependent on circumstances


+ Lying is always wrong, even if doing so would make another person feel good (e.g., telling a white lie about someone's appearance)

<br>
.right[<img src="assets/img/image10.jpg" width=200>]

???

+ Many modern deontologists trace their roots to Immanuel Kant's (1785) categorical imperative
+ Kant argued that moral principles had to be categorical (that is, required in all cases) rather than hypothetical (that is, dependent on other desires)
+ e.g., Lying is always wrong, even if doing so would make another person feel good (e.g., telling a white lie about someone's appearance)

---
##  Pitting the two against each other 

+ Many moral dilemmas pit utilitarian and deontological moral intuitions against one another


+ A classic example is the trolley problem

<br>
.center[<iframe width="560" height="315" src="https://www.youtube.com/embed/JWb_svTrcOg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>]

???

+ Many moral dilemmas, as we will discuss in more detail next class, pit utilitarian and deontological moral intuitions against one another
+ A classic example is the trolley problem
+ In the trolley problem, a runaway trolley is headed down a set of tracks toward a group of 5 workers
  + The brakes are out, so there's no way of stopping the trolley...and the workers are so far away that they don't hear the trolley (or any cries for them to move)
  + If the trolley hits them, they will be killed
  + But, you can pull a level that will divert the trolley to a different set of tracks to avoid the 5 workers
  + However, on this other set of tracks, there is a single worker that will be killed
  + The question is, do you pull the lever...saving the 5 but killing the 1
  + Utilitarianism = kill the one to save the 5
  + Deontology = you can't sacrifice one person to save the group
+ There are a ton of variations on this...and we'll spend more time next class unpacking some of the research behind decisions to this thought experiment
+ But for now, here's a clip demonstrating this idea...

---
#  Kohlberg's stages of moral development 

+ The traditional descriptive view of moral judgments is a rationalist view


+ Children develop the ability to carry out moral reasoning over stages

???

+ The traditional descriptive view of moral judgments is a rationalist view
  + This view suggests that people first consciously and deliberately think through a moral problem
+ Children develop the ability to carry out moral reasoning over stages
  + Their moral reasoning is limited by their cognitive abilities at each age
+ It follows, then, that we might develop our moral judgments in a series of stages...with some people having more developed moral reasoning skills than others

---
#  Kohlberg's stages of moral development 

+ Pre-conventional level (Stages 1 and 2)
	+ Moral judgments based on seeking rewards and avoiding punishment


+ Conventional level (Stages 3 and 4)
	+ Moral judgments driven by social conventions, helping to keep society stable by upholding its expectations


+ Post-conventional level (Stages 5 and 6)
	+ Moral judgments based on a deeper, more universal understanding of morality

???

+ In Kohlberg's model, there are 6 stages of moral development that can be grouped into 3 different levels
+ Pre-conventional level is associated with Stages 1 and 2
	+ Here, moral judgments based on seeking rewards and avoiding punishment
+ Conventional level is associated with Stages 3 and 4
	+ Here, moral judgments driven by social conventions, helping to keep society stable by upholding its expectations
+ Post-conventional level is associated with Stages 5 and 6
	+ Here, moral judgments based on a deeper, more universal understanding of morality

---
#  The Heinz dilemma

A woman was near death from a special kind of cancer. There was one drug that the doctors thought might save her. It was a form of radium that a druggist in the same town had recently discovered. The drug was expensive to make, but the druggist was charging ten times what the drug cost him to produce. He paid $200 for the radium and charged $2,000 for a small dose of the drug. The sick woman's husband, Heinz, went to everyone he knew to borrow the money, but he could only get together about $1,000 which is half of what it cost. He told the druggist that his wife was dying and asked him to sell it cheaper or let him pay later. But the druggist said: "No, I discovered the drug and I'm going to make money from it." So Heinz got desperate and broke into the man's store to steal the drug for his wife.

<br>
.center[**.purple[Should Heinz have broken into the store to steal the drug for his wife?]**

**.purple[Why or why not?]**]

???

+ Kohlberg tested this idea in what's referred to as the Heinz dilemma
+ Read slide and pose questions to class

---

.pull-left[
+ 1) Should Heinz steal the drug? <br><br><br><br>
+ 2) Is it actually right or wrong for him to steal the drug? <br><br><br>
+ 3) Does Heinz have a duty or obligation to steal the drug? <br><br><br>
+ 4) If Heinz doesn't love his wife, should he steal the drug for her? Does it make a difference in what Heinz should do whether or not he loves his wife? <br><br>
+ 5) Suppose the person dying is not his wife but a stranger. Should Heinz steal the drug for the stranger?
]

.pull-right[
+ 6) Suppose it's a pet animal he loves. Should Heinz steal to save the pet animal? <br><br>
+ 7) Is it important for people to do everything they can to save another's life? <br><br>
+ 8) It is against the law for Heinz to steal. Does that make it morally wrong? <br><br>
+ 9) In general, should people try to do everything they can to obey the law? <br><br><br><br>
+ 10) In thinking back over the dilemma, what would you say is the most responsible thing for Heinz to do?
]

???

+ In previous studies, after making their decision to the initial dilemma, adult participants have been probed with the following questions to assess these different stages of development
+ After answering each question, participants had to also explain their rational
  + For example...after answering question 1...and stating whether Heinz **should** steal the drug....participation also had to explain why or why not
  + For item 9...participants had to answer the question...explain why or why not...and they also had to explain how their answer to this question applied to what Heinz should do
+ As you can see...all of these questions are about not only right/wrong, but also about punishment...social conventions...and human rights
+ By explaining their logic...the why or why not...this probes either moral reasoning a bit....which allows researchers to assess which stage they fell into

---
#  Stage one <br> (punishment & obedience):  

+ Heinz **.purple[should not]** steal the drug
	+ He will be put in prison
	+ Which means he's a mean bad person


+ OR...Heinz **.purple[should]** steal the drug
	+ It is only worth $200
	+ Heinz had even offered to pay & wouldn't steal anything else

???

+ Participants who were stuck in stage 1, tended to focus on factors like punishment and obedience
+ Those who thought Heinz **should not** steal the drug were likely to say things like...
	+ He will be put in prison
	+ Which means he's a mean bad person
+ Whereas those who thought Heinz **should** steal the drug were likely to say things like...
	+ It is only worth $200
	+ Heinz had even offered to pay & wouldn't steal anything else

---
#  Stage two <br> (reward & self-interest):  

+ Heinz **.purple[should not]** steal the drug
	+ Prison is awful a jail cell is worse than his wife's death


+ OR...Heinz **.purple[should]** steal the drug
	+ He will be much happier if he saves his wife

???

+ Participants who were stuck in stage 2, tended to focus on factors like reward and self-interest
+ Those who thought Heinz **should not** steal the drug were likely to say things like...
	+ Prison is awful a jail cell is worse than his wife's death
+ Whereas those who thought Heinz **should** steal the drug were likely to say things like...
	+ He will be much happier if he saves his wife

---
#  Stage three <br> (conformity – good person):  

+ Heinz **.purple[should not]** steal the drug
	+ Stealing is bad & he's not a criminal
	+ He tried without breaking the law, you can't blame him


+ OR...Heinz **.purple[should]** steal the drug
	+ His wife expects it
	+ He wants to be a good husband

???
+ Participants who were stuck in stage 3, tended to focus on factors like conformity and whether or not Heinz was a good person
+ Those who thought Heinz **should not** steal the drug were likely to say things like...
	+ Stealing is bad & he's not a criminal
	+ He tried without breaking the law, you can't blame him
+ Whereas those who thought Heinz **should** steal the drug were likely to say things like...
	+ His wife expects it
	+ He wants to be a good husband

---
#  Stage four <br> (law & order): 

+ Heinz **.purple[should not]** steal the drug
	+ the law prohibits stealing it's  illegal


+ OR...Heinz **.purple[should]** steal the drug
	+ He should take the punishment for the crime & pay the druggist
	+ Actions have their consequences

???

+ Participants who were stuck in stage 4, tended to focus on factors like law and order
+ Those who thought Heinz **should not** steal the drug were likely to say things like...
	+ the law prohibits stealing it's  illegal
+ Whereas those who thought Heinz **should** steal the drug were likely to say things like...
	+ He should take the punishment for the crime & pay the druggist
	+ Actions have their consequences

---
#  Stage five <br> (human rights):  

+ Heinz **.purple[should not]** steal the drug
	+ The scientist has a right to fair compensation
	+ His wife's illness it does not make his actions right


+ OR...Heinz **.purple[should]** steal the drug
	+ Everyone has a right to choose life, regardless of the law

???

+ Participants who were stuck in stage 5, tended to focus on human rights factors
+ Those who thought Heinz **should not** steal the drug were likely to say things like...
	+ The scientist has a right to fair compensation
	+ His wife's illness it does not make his actions right
+ Whereas those who thought Heinz **should** steal the drug were likely to say things like...
	+ Everyone has a right to choose life, regardless of the law

---
#  Stage six <br> (universal human ethics):  

+ Heinz **.purple[should not]** steal the drug
	+ Others may need the drug just as badly & their lives are equally significant


+ OR...Heinz **.purple[should]** steal the drug
	+ Saving a human life is a more fundamental value than the property rights of another person

???

+ Finally, participants who made it to the final stage of moral development tended to focus on universal human ethics
+ Those who thought Heinz **should not** steal the drug were likely to say things like...
	+ Others may need the drug just as badly & their lives are equally significant
+ Whereas those who thought Heinz **should** steal the drug were likely to say things like...
	+ Saving a human life is a more fundamental value than the property rights of another person

---
#  Gender and moral judgment 

+ Gilligan (1982) pointed out that Kohlberg's longitudinal research, though extensive, included only male study participants


+ A more recent version of her theory suggests that any given moral dilemma can be approached from two orientations:
  + A care orientation (roughly corresponding to Stage 3)
  + A justice orientation (Stage 4)

???

+ This research was great in that it was longitudinal...Kohlberg collected data from a sample of participants over time, and was able to see how people's moral judgments developed
+ It was clear that moral judgments changed over time because he largely studied the same group of participants over time
+ However, it wasn't without some major limitations
+ Gillian was the first to point out that Kohlberg's research only included male study participants
+ Gilligan argued that what is valued as “more advanced” moral reasoning differs by gender...such that men focus more on rights and the law, and women focus more on interpersonal goals
+ A more recent version of her theory suggests that any given moral dilemma can be approached from two orientations:
  + A care orientation (roughly corresponding to Stage 3)
  + A justice orientation (Stage 4)

---
#  Gender and moral judgment 

+ Gilligan and Attanucci (1988): Both genders reason from both orientations, although women tend to focus more on care and men more on justice


+ Jaffee and Hyde (2000) conducted a meta-analysis of 180 studies conducted on this topic, enabling them to determine whether the overall effect of gender emerges across the data from all of these studies taken together

???

+ Later research by Gilligan and colleagues suggested that both genders actually reason from both orientations
+ Although women do tend to focus more on care and men tend to focus more on justice...they each use both to based their moral judgments on
+ About a decade later, after much more research had been conducted on this topic, other researchers conducted a meta-analysis on the effects of gender on moral judgment
+ A meta-analysis is a special kind of study where researchers analyze all the studies conducted on a topic instead of recruiting new participants
  + They basically give researchers a better idea of what a body of literature says rather than just a single study
+ So Jaffee and Hyde were able to determine whether the overall effect of gender emerges across the data from all of these studies taken together

---
#  Jaffee & Hyde's (2000) findings

+ Somewhat small effect size of about $d = .3$ for the effect of gender on care orientation, such that women adopted the care orientation more than men overall 


+ *.blue[Direction]* of the gender difference held true across all age groups and all socioeconomic groups, but the *.blue[strength]* of that difference varied by age and socioeconomic status 

???

+ They found that there was a small effect size for the effect of gender on the care orientation
  + Women did tend to use it more than men, but overall, this effect was weak
+ However, they did find that the direction of this effect held across all age groups and socioeconomic groups
+ What differed was the strength of the difference...sometimes the effect was larger for certain groups, sometimes it was weaker...but women did tend to adopt the care orientation more than men

---
#  Jaffee & Hyde's (2000) findings

+ There was also a small effect size for the effect of gender on justice orientation $(d = .2)$, such that men adopted the justice orientation more than women (Jaffee & Hyde, 2000)


+ Age, though not socioeconomic status, affected the strength of gender differences in adopting the justice orientation


+ Overall, their findings supported Gilligan's (1982) claims across the board, although to a weaker degree than expected

???

+ They also found there was a small effect size for the effect of gender on the justice orientation
  + Men did tend to use it more than females, but overall, these effect was weak...even weaker than the effect for the care orientation
+ For this effect, however, only age affected the strength of gender differences in adopting the justice orientation...SES did not have any influence
+ Overall, however, their findings supported Gilligan's (1982) claims across the board...just to a weaker degree than expected
+ Gilligan's arguments regarding whether the care orientation or the justice orientation ought to be treated as equivalently ranked developmental stages of moral reasoning remain worthy of serious consideration
