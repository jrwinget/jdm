---
title: "The Good Life"
subtitle: "Making Decisions That Make Us Happy"
author: "PSYC 279, Winget"
output:
  xaringan::moon_reader:
    css: xaringan-themer.css
    lib_dir: libs
    nature:
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: false
---

```{r xaringan-themer, include=FALSE, warning=FALSE}
library(tidyverse)
library(xaringanthemer)

style_mono_dark(
  base_color = "#4C837A",
  text_color = "#E1DDBF",
  background_color = "#04263C",
  title_slide_text_color = "#FFFFFF",
  text_slide_number_color = "#04263C",
  colors = c(
  red = "#FF0000",
  blue = "#00FFFF",
  purple = "#FF00FF"
  ),
  extra_css = list(
    ".large-text" = list("font-size" = "150%"),
    ".small-text" = list("font-size" = "75%")
  )
)
```

#  Should we strive for happiness? 

+ Being happy feels good!


+ Happy people are energetic, creative, and productive in the workplace


+ They are more cooperative, and motivated to help others


+ Happy people have more friends, stronger immune systems, and cope more effectively with stress


+ Happiness carries a wide variety of benefits

<br>
.center[<img src="assets/img/image16.jpeg" width=95%>]

???

+ Coming off the heels of our discussions on emotion, it's worth talking about having emotions as goals in themselves
+ In America especially, we have this idea that everyone should be happy...and if you aren't there's something wrong with you
+ It's even in our Declaration of Independence...we're endowed with certain unalienable Rights, that among these are Life, Liberty and the pursuit of Happiness
+ But, is this really something we should be striving for?
+ 
+ When trying to envision a happy day, or a happy life, what images come to mind? 
+ For some people, it might be sharing a meal with good friends and family members while laughing together, telling stories, and feeling loved
+ For others, happiness may come from accomplishing an important goal and basking in the glory of a job well done
+ And for others still, happiness may be the byproduct of doing good deeds, helping others, and believing that the world is a better place because of it
+ Although people may vary a good deal in what they think will make them happy, an overwhelming majority of U.S. residents place "finding happiness" very high on their list of major life goals (Diener, Suh, Smith, & Shao, 1995)
+ So, should we strive for happiness?
+ Well it is associated with a number of positive things
  + Being happy feels good!
  + Happy people are energetic, creative, and productive in the workplace
  + They are more cooperative, and motivated to help others
  + Happy people have more friends, stronger immune systems, and cope more effectively with stress
  + Happiness carries a wide variety of benefits
+ But as with pretty much everything we discuss in this course, there's more to it than what meets the eye

---
#  Defining happiness 

+ Most researchers define happiness as consisting of three components <br> (Diener et al., 1995): 
  1. __ instances of positive affect
  1. __ instances of negative affect, and 
  1. A __ of life satisfaction

.center[<img src="assets/img/image17.png" width=60%>]

???

+ Most researchers define happiness as consisting of 3 components: 
  1. Frequent instances of positive affect
  1. Infrequent instances of negative affect, and 
  1. A high level of life satisfaction (Diener et al., 1995)
+ Positive are simply experiences of good feelings (e.g., excited, joyful, pleased)...and negative affect are experiences of bad feelings (e.g., irritable, sad, tense)
+ Life satisfaction, by contrast, is a more global, cognitive evaluation of how content a person is with the state of his or her life
+ Those with a high level of life satisfaction would agree with statements such as, "The conditions of my life are excellent" (Diener, Emmons, Larsen, & Griffin, 1985)

---
#  What doesn't lead to happiness  

+ Money doesn't make us happy


+ Wealth differences between nations predicts __


+ However, income and SWB within nations are largely __


+ Recent economic growth has not translated into a rise in SWB

???

+ Four replicable findings have emerged regarding the relation between income and subjective well-being (SWB): 
  1. There are large correlations between the wealth of nations and the mean reports of SWB in them
  1. There are mostly small correlations between income and SWB within nations...although these correlations appear to be larger in poor nations, and the risk of unhappiness is much higher for poor people
  1. Economic growth in the last decade or so in most economically developed societies has been accompanied by little rise in SWB...and increases in individual income lead to variable outcomes, and...
  1. People who prize material goals more than other values tend to be substantially less happy...unless they are rich
+ Thus, more money may enhance SWB when it means avoiding poverty and living in a developed nation...but income appears to increase SWB little over the long-term when more of it is gained by well-off individuals whose material desires rise with their incomes

---
#  What doesn't lead to happiness  

+ __

.center[<img src="assets/img/image18.png" width=75%>]

???

+ Kross E, Verduyn P, Demiralp E, Park J, Lee DS, et al. (2013) Facebook Use Predicts Declines in Subjective Well-Being in Young Adults. PLOS ONE 8(8): e69841.https://doi.org/10.1371/journal.pone.0069841
+ 
+ Over 500 million people interact daily with Facebook
+ An experiment by Kross and colleagues used experience-sampling, the most reliable method for measuring in-vivo behavior and psychological experience
+ The researchers text-messaged people 5x per day for two-weeks to examine how Facebook use influences 2 components of subjective well-being: 
  1. how people feel moment-to-moment, and...
  1. how satisfied they are with their lives
+ Their results indicate that Facebook use predicts negative shifts on both of these variables over time
+ The more people used Facebook at one time point, the worse they felt the next time they were text-messaged; 
+ The more they used Facebook over two-weeks, the more their life satisfaction levels declined over time
+ Interacting with other people “directly” (DM) did not predict these negative outcomes
+ They were also not moderated by the size of people's Facebook networks, their perceived supportiveness, motivation for using Facebook, gender, loneliness, self-esteem, or depression
+ On the surface, Facebook provides an invaluable resource for fulfilling the basic human need for social connection
+ But rather than enhancing well-being, these findings suggest that Facebook may undermine it

---
##  Why we get it wrong 

+ We get used to stuff
  + Hedonic adaptation! (aka, hedonic treadmill)


+ We psychologically adapt to both positive and negative circumstances, experiences, and so forth


+ That new 80" TV will never bring us the same level of happiness as when we first bought it

.center[<img src="assets/img/image19.png" width=55%>]

???

+ So, if money doesn't make us happy, social media doesn't make us happy, why do we think they do?
+ Well, one reason is that we get used to stuff
+ This idea is known as hedonic adaptation (aka hedonic treadmill)
+ It's basically the tendency for humans to quickly return to a relatively stable level of happiness despite major positive or negative events or life changes
+ According to this theory, as a person makes more money, expectations and desires rise in tandem...which results in no permanent gain in happiness
+ In other words, that new 80” TV or that brand new luxury car will never bring us the same level of happiness as when we first bought it

---
##  Why doesn't money make us happy, specifically? 

+ __


+ Adaptation (__)

.center[<img src="assets/img/image20.png" width=90%>]

???

+ You might wonder, well why doesn't money make us happy?...money can be used for many things, only some of which are buying fancy things
+ Well, we often make upward social comparisons to people who have more income than we do...which reduces our happiness with what we have
  + There is always more to be made than we currently have...new tech is constantly coming out...our situation can always be improved
+ In response, we adapt to our newly acquired level of income and look for ways to continually increase this income
+ In other words, we get used to what we have so it no longer brings us happiness
+ This is the psychological immune system that we talked about in the affective forecasting lecture a while back at work
  + Much like our physical immune system is always trying to bring us back to our baseline, our psych immune system tries to do the same thing

---
#  Why we get it wrong 

.pull-left[<br>
<img src="assets/img/image21.png" width=100%>]

.pull-right[<br><br>
+ Our habits lead us __
  + Habits make it __ to change or regulate behavior (Neal, Wood, & Quinn, 2006)


+ This is especially so for strong habits
]

???

+ Another reason striving for happiness doesn't always work out is that our habits can lead us astray
+ Our ingrained habits are often at odds with the things that make us happy...like exercising and eating well
+ Neal, Wood, and Quinn (2006) review evidence showing how our habits, especially our strong habits, stop us from changing or regulating our behavior in positive ways that will increase our happiness
+ Habits are a big obstacle to our efforts to change our everyday behaviors surrounding eating and exercising

---
#  What does lead to happiness? 

<br>
.pull-left[<br>
<img src="assets/img/image22.png" width=100%>]

.pull-right[<br>
+ __


+ We report being happier when __ (Myers, 2000)


+ Our __ are a strong predictor of our happiness
]

???

+ SO, what actually **does** lead to happiness?
+ One important factor is our friends, family, and social relationships in general
+ We are social animals with a strong need to belong
+ Need to belong is a genuine need for people (not just a want)...and it's basically a need to have close relationships with others
+ People who can't fulfill this need feel depressed and lonely, engage in more self-defeating behaviors, take more risks, and have higher mortality rates
+ So when this need isn't met, people suffer more than merely being unhappy...it is essential in order to be healthy
+ We also report being happier when around other people
+ So taken together, our social relationships are a strong predictor of our happiness
+ Explain graph...people who are married fulfill this need to belong (report being happier)

---
#  What does lead to happiness? 

+ __

<br>
.center[<img src="assets/img/image23.png" width=75%>]

???

+ Healthy behaviors...like exercising and having a healthy diet...have also been shown to increase our happiness
+ A review of the evidence by Callahan (2004) shows that exercise is beneficial for mental health
+ It reduces anxiety, depression, and negative mood, and improves self-esteem and cognitive functioning
+ Exercise is also associated with improvements in the quality of life of those living with Schizophrenia

---
#  What does lead to happiness? 

+ __

<br>
.center[<img src="assets/img/image24.png" width=70%>]

???

+ Time affluence also leads to increased happiness
+ Time affluence is the feeling that one has sufficient time to pursue activities that are personally meaningful, to reflect, to engage in leisure
+ In contrast, time poverty is the feeling that one is constantly stressed, rushed, overworked, and behind
+ Now, I'll let Laurie Santos, one of the prominent researchers of time affluence, discuss this idea in more detail

---
# Laurie Santos on time affluence

.center[
<video width="600" height="400" controls>
    <source src="assets/santos-video.mp4" type="video/mp4">
</video>
]

???

+ 

---
#  What does lead to happiness? 

+ __ purchases make us happier than material purchases (Van Boven & Gilovich, 2003)


+ In a national sample of adults, fully 57% of respondents reported they had derived greater happiness from their __


+ Whereas only 34% reported greater happiness from their __ purchases

???

+ “Go out and buy yourself something nice” 
+ That's often the advice we are given when we're feeling down or to celebrate an accomplishment or whatever
+ However well intentioned this advice is, it is probably bad advice
+ Research shows that buying experiences, rather than material goods, makes us happy
+ Experiential purchases can be defined as purchases made with the primary intention of acquiring a life experience
  + an event or series of events that one lives through
+ In one study, these definitions were presented to a nationwide sample of over a thousand Americans, who were asked to think of a material and an experiential purchase they had made with the intention of increasing their own happiness
+ Asked which of the two purchases made them happier, fully 57% of respondents reported that they had derived greater happiness from their experiential purchase, while only 34% reported greater happiness from their material purchase
+ Similar results emerged using a between-subjects design in which participants were randomly assigned to reflect on either a material or experiential purchase they had made
+ Individuals experienced elevated mood when contemplating a past experiential purchase (relative to those contemplating a past material purchase), suggesting that experiential purchases produce more lasting hedonic benefits

---
#  What does lead to happiness? 

+ Using our money to __ (Dunn, Aknin & Norton, 2008)


+ Personal spending did not predict happiness, but spending money on __ predicted greater happiness


+ Experimental evidence revealed a similar pattern


+ Those assigned to spend money on __ were happier than those assigned to spend money on themselves

???

+ An experiment revealed a similar pattern of results (Dunn, Aknin, & Norton, 2008)
+ Using our money to benefit others rather than ourselves also tends to make us happier
+ Researchers approached individuals on the University of British Columbia (UBC) campus, handed them a $5 or $20 bill...and then randomly assigned them to spend the money on themselves or on others by the end of the day
+ When participants were contacted that evening, individuals who had been assigned to spend their windfall on others were happier than those who had been assigned to spend the money on themselves

---
# Elizabeth Dunn

.center[
<video width="600" height="400" controls>
    <source src="assets/dunn-video.mp4" type="video/mp4">
</video>
]

???

+ So now, let's listen to a little more on this idea by the lead author of the study, Elizabeth Dunn

---
#  What does lead to happiness? 

+ Buying __

<br>
<br>
.center[
<img src="assets/img/image25.png" height=225>
<img src="assets/img/image26.png" height=225>
]

???

+ One reason why small frequent pleasures beat infrequent large ones is that we are less likely to adapt to the former
+ Having a beer after work with friends, for example, is never exactly the same as it was before
+ This week the bar had a new India Pale Ale from Oregon on tap, and Sam brought along his new friend Kate who told a funny story about dachshunds
+ If we buy an expensive dining room table or a fancy big screen TV, on the other hand, it's pretty much the same table or TV today as it was last week
+ Because frequent small pleasures are different each time they occur, they forestall adaptation
