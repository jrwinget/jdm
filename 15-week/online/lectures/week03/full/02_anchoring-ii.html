<!DOCTYPE html>
<html lang="" xml:lang="">
  <head>
    <title>Anchoring &amp; Primacy Effects in Judgment</title>
    <meta charset="utf-8" />
    <meta name="author" content="PSYC/PHIL 279, Winget" />
    <link rel="stylesheet" href="xaringan-themer.css" type="text/css" />
  </head>
  <body>
    <textarea id="source">
class: center, middle, inverse, title-slide

# Anchoring &amp; Primacy Effects in Judgment
## Part II
### PSYC/PHIL 279, Winget

---




#  Boundary conditions 

+ The anchors so far have been plausible, in that they could be the correct answer


+ What happens if we provide people with implausible anchors? 
  + Will they still have an effect?

.center[&lt;img src="assets/img/image40.jpg" width=80%&gt;]

???

+ So far, we have discussed anchoring studies in which the anchor is plausible
 + That is, the value could be a correct answer
+ But there are other types of anchors we could use as well
  + There are anchors out there where the answer is implausible
  + These probably aren't too informative
+ So, what happens if we provide people with clearly implausible anchors?
  + Do we see the same effect if the context is a little different?

---
#  Boundary conditions 

+ When did Einstein first visit the United States?
  + Plausible: 1939 (high), 1905 (low)
  + Implausible: 1992 (high), 1215 (low)


+ When was Aristotle born?
  + Plausible: 222 BC (high), 490 BC (low)
  + Implausible: 1832 AD (high), 25,000 BC (low)

&lt;br&gt;
.pull-left[.center[&lt;img src="assets/img/image22.png" height=250&gt;]]
.pull-right[.center[&lt;img src="assets/img/image12.png" height=250&gt;]]

???

+ For example, if participants were asked to estimate when Einstein first visited the US, a plausible anchor might be 1939
  + This is a bit high, but it seems reasonable
  + Same for 1905...this one might be a lower anchor, but it's still reasonable
  + The correct answer is 1921
+ But if were were to provide the anchors 1995 or 1215, these are clearly so extreme they don't seem like plausible anchors
  + Same idea here for when Aristotle was born...
+ Even implausible anchors like these have been shown to influence people's judgments

---
#  Strack &amp; Mussweiler (1997) 

.center[
![](02_anchoring-ii_files/figure-html/gph2-1.png)&lt;!-- --&gt;
]

???

+ Researchers presented participants with both plausible and implausible anchors for a variety of judgments
+ And they found the typical anchoring effect overall
+ But what was really interesting was that *both* plausible and more importantly implausible anchors produced the effects
+ The values are standardized (z-scores)
  + If you've taken stats, these should be familiar
  + If you haven't, just think of them as being a way to compare 2 different metrics with one another
  + Different judgment problems are reported in different metrics (age, date, miles, etc.)
  + We can't accurately compare years to miles (it's a bit like comparing apples and oranges)
  + So we convert them to Z scores, which allows us to directly compare them
+ High anchors led to higher estimates (above 0) than low anchors (below 0)
+ This is crazy! Shouldn’t happen

---
#  Subliminally presented anchors 

+ Wilson and colleagues (1996) showed that merely seeing an anchor can cause anchoring effects


+ How about anchors presented outside of conscious awareness? 
  + AKA: Subliminally presented anchors


+ Mussweiler and Englich (2005) provide a good test of this idea

.center[&lt;img src="assets/img/image41.jpg" width=70%&gt;]

???

+ Others have shown even more fascinating anchoring effects
+ Wilson et al. (1996) wondered that since the anchoring-and-adjustment effect is so robust, if it might even occur for anchors that are not consciously processed
+ In the Wilson et al. research, anchors were presented but participants were told it was part of a different task and were explicitly asked not to make any deliberate comparison to the anchoring question
  + Results showed that when participants simply saw this anchor in their environment (and were making any direct comparison to the anchoring question) their judgments were influenced by the anchor compared to when no anchors were in their environment
+ This study is fascinating, but it still leaves the possibility that people were consciously aware of the anchor, even if they weren't using it to compare to the anchoring question
+ So to push the boundaries of the anchoring effect, others directly tested whether *subliminally* presented anchors might influence judgments
  + These are anchors that we are not even consciously aware of...the idea is that system 1 picks up on the information and is processing it out of our conscious awareness
  + Mussweiler and Englich (2005) provide a good test of this idea

---
#  Subliminally presented anchors 

&lt;br&gt;
.center[&lt;img src="assets/img/image31.png" width=100%&gt;]

&lt;br&gt;
After the question appeared on screen, the nonsense letters appeared for 60 seconds. During those 60 seconds, the anchor appeared every six seconds for 15 milliseconds each time. Immediately after this the nonsense letters appeared again.

???

+ In this study, participants worked on a computer and were asked to fixate on the center of the screen while answering questions
+ They were presented with the question on the screen for 3 seconds (Temp in Germany)
+ After the questions appeared on the screen, the nonsense letters appeared for 60 seconds
+ During those sixty seconds, a single anchor value (either 5 or 20) was presented every 6 seconds for 15 milliseconds at a time
  + 15 milliseconds is too brief for the information to be consciously registered
+ Participants then gave their estimate in response to the initially presented question

---
#  Subliminally presented anchors 

+ Results of Mussweiler and Englich (2005)

&lt;br&gt;
&lt;img src="02_anchoring-ii_files/figure-html/gph3-1.png" style="display: block; margin: auto;" /&gt;

???

+ Participants were probed after the study to see if they were aware of the subliminal anchor (e.g., asked if they noticed anything unusual)...all good
+ Results showed people made significantly higher judgments of temp in the high subliminal anchor condition (20; mean answer = 14.9) than in the low subliminal anchor condition (5; mean answer = 12.8)
+ Follow up work showed this result was also replicated for another judgments/questions as well (e.g., estimating car prices)

---
#  Cross-modal anchoring effects 

+ What if anchors prime a general sense of magnitude (large or small)?


+ If so, then anchors would effect estimates across domains


+ Physical quantities may serve as anchors

&lt;br&gt;
&lt;br&gt;
.pull-left[&lt;img src="assets/img/image13.png" height=275&gt;]
.pull-right[&lt;img src="assets/img/image9.png" height=275&gt;]

???

+ More recent research has looked at whether an anchor can prime a more general sense of large vs. small that could affect judgment across domains
  + This is called cross-model anchoring
  + Basically, it's the idea that an large anchor in one domain can prime a general sense of largeness in other domains

---
#  Cross-modal anchoring effect 

“How long is the Mississippi River?" 

.pull-left[.center[
&lt;img src="assets/img/image27.png" width=90%&gt;
&lt;img src="assets/img/image10.png" height=200&gt;
]]

.pull-right[
![](02_anchoring-ii_files/figure-html/gph4-1.png)&lt;!-- --&gt;
&lt;br&gt;
.center[*Oppenheimer et al. (2008)*]
]

???

+ In one experiment, participants were presented with a set of three horizontal lines and were asked to replicate the lines as best as they could without using a ruler (see slide for lines)
+ The three lines were a straight line, a wavy line, and an inverted-u
+ Participants in the short-anchor conditions replicated the lines at the bottom, and those in the long-anchor condition replicated the lines at the top
+ After this, participants were presented with an ostensibly unrelated judgment task in which they were asked to estimate various quantities
+ The key item asked them to estimate the length of the Mississippi River
+ People who were given long lines to draw gave significantly larger estimates than those given short lines to draw 

---
##  Perspective taking as egocentric anchoring 

+ Children’s thought is often egocentric
  + They take their perspective as an accurate reflection of the world


+ As adults, we know better...we come to view the world less egocentrically


+ However, we do not completely outgrow our childhood tendencies

&lt;br&gt;
.center[&lt;img src="assets/img/image11.png" width=45%&gt;]

???

+ Another way anchoring effects can emerge is through perspective taking
+ Research has consistently demonstrated children often fail to take the perspective of others
  + Their thinking/reasoning tends to be egocentric
+ We, as adults, often recognize that our perspective may be biased or wrong in any number of ways
  + At least if they're reasonable adults
  + As we grow up, we view the world less egocentrically
+ However, as research on perspective taking shows, we do not completely outgrow our childhood tendencies to egocentrism
+ NEXT SLIDE

---
##  Perspective taking as egocentric anchoring 

+ Even adults have difficulty setting aside their own perspective when considering the perceptions of others


+ This egocentric anchoring can be seen when we try to take the perspective of others

.right[&lt;img src="assets/img/image42.jpg"&gt;]

???

+ Many social judgments, even among adults, are still egocentrically biased
+ People tend to believe, for example, that their internal states and intentions are more transparent to others than they actually are (Gilovich, Savitsky, &amp; Medvec, 1998)
+ They overestimate the extent to which others attend to those states (Gilovich, Medvec, &amp; Savitsky, 2000) and exaggerate the extent to which others will share their thoughts and feelings (Keysar, 1994; Nickerson, 1999; Ross &amp; Ward, 1996; Van Boven, Dunning, &amp; Loewenstein, 2000)
+ It is clear that even adults can have a difficult time setting aside their own perspective when considering the perceptions of others

---
##  Perspective taking as egocentric anchoring &amp; adjustment 

+ When we take the perspective of others, we initially anchor on our perspective


+ And only subsequently do we adjust (with some effort) until we reach a plausible perspective

&lt;br&gt;
.center[&lt;img src="assets/img/image28.png"&gt;]

???

+ People adopt others’ perspectives by initially anchoring on their own perspective and only subsequently, serially, and effortfully accounting for differences between themselves and others until a plausible estimate is reached, using what Tversky &amp; Kahneman (1974) described as the “anchoring and adjustment heuristic.” 
+ This adjustment is comprised of a series of discrete mini-adjustments coupled with hypothesis tests: People “jump” some amount from their original egocentric anchor and evaluate whether this new perspective plausibly captures the other’s perception (Epley &amp; Gilovich, 2001)
+ If so, adjustment stops
+ If not, another jump is made to a perspective more discrepant from their own, and so on, until a plausible assessment is reached...one that accommodates the difference between one’s own and another’s perspective

---
#  Egocentrism over email 

+ Nonverbal cues, which are critical in communicating tone and meaning, are missing from email


+ Therefore, miscommunication is easy over email and text


+ People routinely overestimate how well they communicate over email and text

&lt;br&gt;
.center[&lt;img src="assets/img/image14.png" width=60%&gt;]

???

+ A very common place we see egocentrism emerge in today's work is through email
+ In an email, nonverbal cues, which are critical in communicating tone and meaning, are often missing
  + No facial cues, no gestures, no vocal inflections, etc.
+ Therefore, miscommunication is really easy over email and text
+ And, research has shown that people routinely overestimate how well they communicate over email and text

---
background-image: url(assets/img/image15.png)
background-size: cover
background-position: center, middle

???

+ In one study, pairs of participants took part in an experiment
+ They were told the study concerned detection of sarcasm
+ They were given 20 statements about a number of topics (eg., food, weather, Greek life, etc.)
+ Participants were correctly told that half of the statements were serious and half were sarcastic
+ Participants then chose 10 statements to communicate to their partner that they thought would be easiest for their partner to detect as serious or sarcastic
+ They were then randomly assigned to either transmit the messages over email or over voice mail
+ After writing or recording, participants then indicated how many statements they thought their partner would get correct (anticipated accuracy)
+ Actual accuracy was simply the number of statements their partner got correct
+ Participants were clearly overconfident in their ability to communicate sarcasm over email!

---
#  Emojis to the rescue? 

.center[&lt;img src="assets/img/image25.jpg"&gt;]

.right[*Miller, Kluver, Thebault-Spieker, Terveen, &amp; Hecht (2017)*]

???

+ So one answer to this problem, believe it or not, might be emjois
  + They're text-based and designed to communicate emotions, so maybe they could convey some of these nonverbal cues humans so often use to communicate
+ Unfortunately, however research indicates that they too suffer from issues of misconstrual and variability in interpretation, even when presented with text
+ The problem is that people do not always agree on the meaning of the emoji and often fail to understand the intent behind emojis
+ Nevertheless, inserting emojis into text messages does seem to help convey meaning better than having no emojis
+ So, they are an improvement, but they don't solve the entire problem

+ Miller, H., Kluver, D., Thebault-Spieker, J., Terveen, L., &amp; Hecht, B. (2017, May). Understanding emoji ambiguity in context: The role of text in emoji-related miscommunication. In Eleventh International AAAI Conference on Web and Social Media.

---
#  Applications of anchoring effects 

+ Medical diagnosis
  + First, figure out the initial probability of the diagnosis
  + Second, assess the strength of the diagnostic test administered

&lt;br&gt;
.center[&lt;img src="assets/img/image19.png" width=85%&gt;]

???

+ So far, we have discussed anchoring effects largely in a laboratory setting
+ This leaves open the question of whether anchoring effects actually influence important judgments in people’ lives
+ In medical diagnosis, clinicians should ideally start from the initial probability of the diagnosis being considered
+ That is, they should ask how rare or how common is that diagnosis in general
+ Then clinicians should run an available diagnostic test to gain more information about a particular patient
+ Unless the test is 100% diagnostic the clinician should then factor in the strength of the evidence given by the test
+ That is, what’s the outcome of the test + the accuracy of the test

---
#  Anchoring and medical diagnosis 

+ Anchoring and adjustment may enter into the medical diagnosis process at several points


+ Use anchor as initial estimate of probability


+ Clinicians may, during an initial assessment, be drawn to a salient feature of the patient (e.g., a startling skin rash), which may act as an anchor

&lt;br&gt;
.center[&lt;img src="assets/img/image17.png" width=55%&gt;]

???

+ Although this model of diagnostic reasoning is pretty straightforward, clinician error can occur at a couple of different points
+ People may often rely on an anchor when they do not know the true probability of a diagnosis (step 1 on previous slide)
  + Although doing this may work much of the time, simply due to the way anchors work, it cannot be foolproof 
+ Second the degree to which test results influence diagnostic judgments may depend on whether the results are obtained early or late in the diagnostic process
+ Also, anchoring-and-adjustment may also come into play earlier in the process, when the clinical is making their first examination of the patient and generating possible diagnoses
  + For example, clinicians might be drawn to the most salient features during this examination (e.g., a rash)
  + This attention-drawing feature may actually act as an anchor and end up disproportionately affecting the clinician's eventual diagnosis

---
##  Referral letters in clinical diagnosis 

+ Same case


+ Depression referral


+ Anchoring in intermediate, not experienced, clinicians


+ Anxiety referral


.right[
&lt;img src="assets/img/image43.jpg" width=50%&gt;&lt;br&gt;
*Spaanjaars et al., 2015*
]

???

+ Spaanjaars and colleagues went back further in the diagnostic process, asking whether referral letters that suggest a possible diagnosis have a strong influence on clinician’s final diagnoses
+ In the study, clinicians read a clear-cut case history of depression after reading a referral letter that either suggested depression or anxiety as a diagnosis
+ Clinicians with intermediate levels of expertise (2-10 years) showed a clear anchoring effect to the suggested diagnosis
+ This likely resulted because they viewed either diagnosis as likely diagnoses for the case, even though the case history suggested clearly that it was the right diagnosis
+ Clinicians with a great deal of expertise (11-50 years) did not show anchoring

---
#  Anchoring in legal settings 

+ In civil tort lawsuits, the plaintiff usually requests a specific dollar amount in compensatory and sometimes punitive damages


+ Lawyers often ask for a range of values

&lt;br&gt;
.center[&lt;img src="assets/img/image16.png"&gt;]

???

+ Medical decision making isn't the only area susceptible to anchoring effects
  + They occur in legals settings a surprising amount
+ For example, civil tort law is a branch of the law that deals with breaches of civil duties rather than a contractual duty or societal duty 
+ The idea is that people are liable for the consequences of their actions if they cause harm to another person/entity
+ So in a civil tort lawsuit, the plaintiff usually makes a request for monetary damages for compensation
+ In doing so, lawyers often ask for a range of values that would be acceptable

---
#  Anchoring in legal settings 

+ Hastie et al. (1999) conducted a study in which they manipulated this range of values


+ Plaintiffs asserted either that “an award in the range from $15 to $50 (or $50-$100) million would be appropriate”


+ The median awards were $15 million versus $50 million in the two conditions

&lt;br&gt;
.center[&lt;img src="assets/img/image44.jpg" width=60%&gt;]

???

+ So, Hastie and colleagues decided to conduct a study in which they manipulated the range of values lawyers ask for
+ There were 2 conditions in this study
  + Participants either heard plaintiffs ask for an award in the range from $15 to $50 million would be appropriate
  + OR they heard plaintiffs ask for an award in the range from $50 to $100 million would be appropriate
+ Those in the 15-50 condition judged awards of $15 million to be appropriate
  + while the participants in the 50-100 condition judged $50 million to be appropriate
+ These results are especially interesting because it was observed after the judge clearly instructed the jurors that the attorney’s recommendations are NOT evidence
+ In other words, the values were arbitrary and yet people still anchored on them when coming up with their awards
+ These results suggest something interesting for attorneys: The more you ask for, the more you get!

---
##  Anchoring effects in real estate pricing 

+ Northcraft and Neale (1987) study on anchoring and real estate pricing


+ Business students and real estate agents visited a home and viewed a portfolio of information about the house


+ They manipulated the listing price to be either 4% above or below appraised value (moderate anchor) or 12% above or below value (extreme anchor)

&lt;br&gt;
.center[&lt;img src="assets/img/image45.png" width=35%&gt;]

???

+ Anchors also pop up in real estate situations all the time
+ In one study, Northcraft and Neale (1987) asked business students and experienced real estate agents to visit a local home for sale
+ They were allowed 20 minutes to look over the home and surrounding area
+ They received a portfolio of information, including a listing sheet with home information (features of the home, other homes in the neighborhood, etc.)
+ The critical manipulation was the price on the listing sheet for the home
+ The actual appraised value of the home was $74,900
+ In four between-subjects conditions, they listed the price as either 4% above or below the actual value (moderate anchors) or 12% above or below the actual value (extreme anchors)
+ They were then asked to estimate 1) the house’s appraisal value, 2) an appropriate listing price, 3) what price would be reasonable for a seller to pay for the house, and 4) what lowest price they would accept if they were selling the home

---
#  Anchoring and real estate pricing 

+ Both business students and real estate agents were influenced by the anchors


+ Highest suggested prices for the high extreme anchor, and lower for those who saw the other anchors

&lt;br&gt;
.center[&lt;img src="assets/img/image18.png" width=70%&gt;]

???

+ Both business students and real estate agents were influenced by the anchors
  + People who should likely be the least influence because of their expertise
+ Their price suggestions were highest for those who saw the highest extreme anchor and much much lower for those who saw the other anchors

---
#  Starting values in negotiation 

+ Whenever individuals put their firm up for acquisition, seek agreement on salary, or sell a used car, someone has to set the starting price

&lt;br&gt;
.center[&lt;img src="assets/img/image24.png" width=85%&gt;]

???

+ Another interesting area anchoring effects emerge is in negotiations
+ We'll spend an entire lecture talking about negotiations toward the end of the semester, but in the context of anchoring, starting values can actually serve as an anchor in the negotiation
  + Whenever individuals put their firm up for acquisition, seek agreement on salary, or sell a used car, someone has to set the starting price
+ And, when the starting price is high, it often results in the negotiation ending high as well
+ Buyer–seller negotiations conducted using laboratory or in-class exercises have repeatedly demonstrated that final outcomes are positively anchored by first offers

---
#  Starting values in negotiation 

+ Why does starting high often result in ending high?
  + Insufficient adjustment
  + Selective accessibility

&lt;br&gt;
.center[&lt;img src="assets/img/image30.png" width=85%&gt;]

???

+ There are actually a few reasons for this
+ First, the more extreme the starting value, the greater the adjustment needed, but the more likely this adjustment will be insufficient
  + Thus, extreme starting values will produce more extreme final estimates
+ Second, individuals selectively generate information consistent with the anchor, which partially mediates the influence of anchors on subsequent judgments
+ These two processes of insufficient adjustment and selective accessibility combine to explain the influence of starting prices on final prices in negotiations
+ Take the example of a buyer facing a seller
  + The buyer knows she should adjust from the seller's high opening offer, but does so insufficiently
  + Similarly, her opponent's high opening offer selectively directs negotiators' attention toward an item's positive attributes
  + In contrast, a lower opening offer would direct attention to its flaws
  + In estimating a used car's true value, a high first offer is likely to lead the buyer to focus on the car's pristine interior rather than its ailing clutch, thereby leading to a higher valuation (Mussweiler, Strack, &amp; Pfeiffer, 2000) and making a high final price more palatable

---
#  Countering effects of anchoring 

+ Simply informing people about the effect of an anchor is not sufficient to attenuate the anchor’s effects on judgment (Chapman &amp; Johnson 2002)


+ George et al. (2000) created a decision support system intended to help people estimate the value of homes on the real estate market while countering any effects of anchoring from the listing price

.center[&lt;img src="assets/img/image46.png" width=50%&gt;]

???

+ Because the desirability of an anchoring effect can be good or bad across different situations, it's worthwhile to try to learn how to counter the effect when needed
+ Unfortunately, though, the anchoring effect has been very difficult to eliminate
+ For example, simply information people about the effect isn't enough to less an anchor's effects on judgment
+ One attempt to help people reduce their own anchoring effects was to provide them with decision support systems
  + People in their study were presented with pieces of information about a property (photos, room size, amenities, etc.) and were asked to make estimates of the home's value
  + Anchors were high or low, and decision support system either warned when estimates were too close to the anchor or did not warn

---
#  Countering effects of anchoring 

+ However, George et al. (2000) found that people using the decision support system showed the anchoring effect just as robustly as in previous work


+ Some evidence suggests that simply experiencing positive affect may reduce the effect of anchoring by reducing how much people rely on superficial or heuristic-based reasoning overall (Estrada, Isen, &amp; Young 1997)

&lt;br&gt;
.center[&lt;img src="assets/img/image47.jpg" width=70%&gt;]

???

+ Despite the optimism, people using the decision support system showed the anchoring effect just as much as previous work had
+ People who received the warnings changed their estimates, but still failed to adjust enough from the anchor
+ Now, there is some work that suggests simply experiencing positive affect (e.g., happiness) may reduce the effect of anchoring
  + What's going on is that positive affect reduces the amount people rely on heuristic-based reasoning overall
+ In one study, researchers randomly gave doctors a little bag of candies or not (idea being those who got candies were put into more of a positive mood)
  + They all then read a case study of a patient and were asked to think aloud
  + Anchoring occurred when the doctor mentioned an incorrect diagnosis early on and failed to adjust
  + Doctors with positive affect (who had received candy) show significantly less anchoring in their reasoning than those in the control condition
+ So a more fruitful approach to countering anchoring effects might be to focus on general interventions that reduce reliance on heuristics overall, rather than trying to address specific features of the anchoring effect itself
    </textarea>
<style data-target="print-only">@media screen {.remark-slide-container{display:block;}.remark-slide-scaler{box-shadow:none;}}</style>
<script src="https://remarkjs.com/downloads/remark-latest.min.js"></script>
<script>var slideshow = remark.create({
"highlightStyle": "github",
"highlightLines": true,
"countIncrementalSlides": false
});
if (window.HTMLWidgets) slideshow.on('afterShowSlide', function (slide) {
  window.dispatchEvent(new Event('resize'));
});
(function(d) {
  var s = d.createElement("style"), r = d.querySelector(".remark-slide-scaler");
  if (!r) return;
  s.type = "text/css"; s.innerHTML = "@page {size: " + r.style.width + " " + r.style.height +"; }";
  d.head.appendChild(s);
})(document);

(function(d) {
  var el = d.getElementsByClassName("remark-slides-area");
  if (!el) return;
  var slide, slides = slideshow.getSlides(), els = el[0].children;
  for (var i = 1; i < slides.length; i++) {
    slide = slides[i];
    if (slide.properties.continued === "true" || slide.properties.count === "false") {
      els[i - 1].className += ' has-continuation';
    }
  }
  var s = d.createElement("style");
  s.type = "text/css"; s.innerHTML = "@media print { .has-continuation { display: none; } }";
  d.head.appendChild(s);
})(document);
// delete the temporary CSS (for displaying all slides initially) when the user
// starts to view slides
(function() {
  var deleted = false;
  slideshow.on('beforeShowSlide', function(slide) {
    if (deleted) return;
    var sheets = document.styleSheets, node;
    for (var i = 0; i < sheets.length; i++) {
      node = sheets[i].ownerNode;
      if (node.dataset["target"] !== "print-only") continue;
      node.parentNode.removeChild(node);
    }
    deleted = true;
  });
})();
(function() {
  "use strict"
  // Replace <script> tags in slides area to make them executable
  var scripts = document.querySelectorAll(
    '.remark-slides-area .remark-slide-container script'
  );
  if (!scripts.length) return;
  for (var i = 0; i < scripts.length; i++) {
    var s = document.createElement('script');
    var code = document.createTextNode(scripts[i].textContent);
    s.appendChild(code);
    var scriptAttrs = scripts[i].attributes;
    for (var j = 0; j < scriptAttrs.length; j++) {
      s.setAttribute(scriptAttrs[j].name, scriptAttrs[j].value);
    }
    scripts[i].parentElement.replaceChild(s, scripts[i]);
  }
})();
(function() {
  var links = document.getElementsByTagName('a');
  for (var i = 0; i < links.length; i++) {
    if (/^(https?:)?\/\//.test(links[i].getAttribute('href'))) {
      links[i].target = '_blank';
    }
  }
})();
// adds .remark-code-has-line-highlighted class to <pre> parent elements
// of code chunks containing highlighted lines with class .remark-code-line-highlighted
(function(d) {
  const hlines = d.querySelectorAll('.remark-code-line-highlighted');
  const preParents = [];
  const findPreParent = function(line, p = 0) {
    if (p > 1) return null; // traverse up no further than grandparent
    const el = line.parentElement;
    return el.tagName === "PRE" ? el : findPreParent(el, ++p);
  };

  for (let line of hlines) {
    let pre = findPreParent(line);
    if (pre && !preParents.includes(pre)) preParents.push(pre);
  }
  preParents.forEach(p => p.classList.add("remark-code-has-line-highlighted"));
})(document);</script>

<script>
slideshow._releaseMath = function(el) {
  var i, text, code, codes = el.getElementsByTagName('code');
  for (i = 0; i < codes.length;) {
    code = codes[i];
    if (code.parentNode.tagName !== 'PRE' && code.childElementCount === 0) {
      text = code.textContent;
      if (/^\\\((.|\s)+\\\)$/.test(text) || /^\\\[(.|\s)+\\\]$/.test(text) ||
          /^\$\$(.|\s)+\$\$$/.test(text) ||
          /^\\begin\{([^}]+)\}(.|\s)+\\end\{[^}]+\}$/.test(text)) {
        code.outerHTML = code.innerHTML;  // remove <code></code>
        continue;
      }
    }
    i++;
  }
};
slideshow._releaseMath(document);
</script>
<!-- dynamically load mathjax for compatibility with self-contained -->
<script>
(function () {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src  = 'https://mathjax.rstudio.com/latest/MathJax.js?config=TeX-MML-AM_CHTML';
  if (location.protocol !== 'file:' && /^https?:/.test(script.src))
    script.src  = script.src.replace(/^https?:/, '');
  document.getElementsByTagName('head')[0].appendChild(script);
})();
</script>
  </body>
</html>
