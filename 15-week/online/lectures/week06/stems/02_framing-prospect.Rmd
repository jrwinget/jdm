---
title: "Framing Effects & Prospect Theory"
author: "PSYC/PHIL 279, Winget"
output:
  xaringan::moon_reader:
    css: xaringan-themer.css
    lib_dir: libs
    nature:
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: false
---

```{r xaringan-themer, include=FALSE, warning=FALSE}
library(tidyverse)
library(xaringanthemer)

style_mono_dark(
  base_color = "#4C837A",
  text_color = "#E1DDBF",
  background_color = "#04263C",
  title_slide_text_color = "#FFFFFF",
  text_slide_number_color = "#04263C",
  colors = c(
  red = "#FF0000",
  blue = "#00FFFF",
  purple = "#FF00FF"
  ),
  extra_css = list(
    ".large-text" = list("font-size" = "150%"),
    ".small-text" = list("font-size" = "75%")
  )
)
```

#  Framing effects  

+ Framing influences preferences between choices even when the choices are equivalent in expected value


+ Framing choices in terms of gain leads people to be risk-averse


+ Framing choices in terms of loss leads people to be risk-seeking (Tversky & Kahneman, 1981)

<br>
.center[<img src="assets/img/image02.png" width=55%>]

???

+ Last class, we focused a lot on expected utility theory and discussed how it is a normative model of how people make decisions
+ Today, we’ll spend our time talking about more descriptive models of how people actually do make their choices
+ 
+ Framing effects are one of the many ways that people’s decision strategies deviate from normative ones
+ Framing effects occur when people show a systematic preference for one choice in a choice task, but when they think about the offered choices deliberately, they realize the choices are actually identical
  + e.g., some people prefer the taste of blue m&ms to others, but if they stopped to think about it, they might admit there really is no difference in flavor...just a difference in color
+ Frames can take on many forms, but one of the most common distinctions is the difference between gain framing and loss framing
  + when framing choices in terms of gain, people tend to be more risk-averse
      + people overwhelmingly choose the option that has more certainty involved...they don’t like to take risks
+ when framing choices in terms of loss, people tend to be more risk-seeking
      + people overwhelmingly choose the option that has more uncertainty involved...perhaps to try to minimize any loss

---
#  Gain framing 

+ *Choose one:*
  + A) A sure gain of $240
  + B) A 25% chance to gain $1000, and a 75% chance to gain nothing


+ People tend to prefer A even though EV(A) is lower than EV(B)

<br>
<br>
<br>
<br>
<br>
.right[<img src="assets/img/image13.svg" height=200>]

???

+ For example, here, both gambles are framed in terms of gain...the amount of money you would win in
+ Gamble A indicates a definite outcome with no uncertainty...and Gamble B involves taking a major risk
+ The expected value of 100% certainty of gaining $240 (1 x 240 = 240) is actually lower than the expected value of a 25% chance of winning $1000 (0.25 x 1000 = 250)
+ But, people tend to prefer Gamble A
  + They are more risk averse: preferring to take the “sure thing” actually leads to a worse outcome in this case

---
#  Loss framing 

+ *Choose one:*
  + C) A sure loss of $750
  + D) A 75% chance to lose $1000, and a 25% chance to lose nothing
  
  
+ People tend to prefer D, the risk

<br>
<br>
<br>
<br>
<br>
.right[<img src="assets/img/image14.png" height=200>]

???

+ Now in this example, both gambles are frames in terms of loss...money you would lose in the gamble vs. money you would win
+ Gamble C indicates a definite outcome with no uncertainty...and Gamble D involves taking a major risk
+ The expected value of 100% certainty of losing $750 (1 x 750 = 750) is actually the same as the expected value of a 75% chance of losing $1000 (.75 x 1000 = 750)
+ But, people tend to prefer Gamble D
  + They are more risk-seeking now: they choose the option with more uncertainty, in an effort perhaps to avoid losing any money

---
#  Disease outbreak 

+ An outbreak is expected to kill 600
+ Two alternative programs:

<br>
.center[***.red[Which program do you favor?]***]

.pull-left[
+ Program A
  + 200 will be saved
  
  
+ Program B
  + 1/3 probability 600 will be saved
  + 2/3 probability no one will be saved
]

???

+ The most famous demonstration of framing effects was achieved by asking people to read a vignette describing a potential disease outbreak
  + Asked to imagine the US is preparing for the outbreak of an unusual disease, which is expected to kill 600 people
  + 2 alternative programs to combat the disease have been proposed
  + Assume the exact scientific estimate of the consequences as follows:
      + In Program A...
      + In Program B...
  + Which of these 2 do you favor?...Have class write down answer

---
#  Disease outbreak 

+ An outbreak is expected to kill 600
+ Two alternative programs:

<br>
.center[***.red[Which program do you favor?]***]

.pull-right[
+ Program A
  + 400 will die


+ Program B
  + 1/3 probability no one will die
  + 2/3 probability 600 will die
]

???

+ Before we dig into the results, let’s look at this second problem
  + Again, imagine the US is preparing for the outbreak of an unusual disease, which is expected to kill 600 people
  + 2 more alternative programs to combat the disease have been proposed
  + Assume the exact scientific estimate of the consequences as follows:
      + In Program A...
      + In Program B...
  + Which of these 2 do you favor?...Have class write down answer

---
#  Disease outbreak 

+ An outbreak is expected to kill 600
+ Two alternative programs:

<br>
<br>

.pull-left[
+ Program A
  + 200 will be saved


+ Program B
  + 1/3 probability 600 will be saved
  + 2/3 probability no one will be saved


.center[**.red[A: 72%]**]
]

.pull-right[
+ Program A
  + 400 will die


+ Program B
  + 1/3 probability no one will die
  + 2/3 probability 600 will die

<br>
<br>
.center[**.red[B: 78%]**]
]

???

+ If you’re like the participants in this study, you likely chose Program A in the first example and Program B in the second
  + Turns out, the expected value of the programs are exactly the same
  + The only difference is how the programs are framed
  + In the example on the left, both gambles are framed in terms of gain...the amount of lives saved
      + Program A indicates a definite outcome with no uncertainty...and Program B involves taking a major risk
      + The expected value of 100% certainty of saving 200 (1 x 200 = 200) is actually the same as the expected value of a 1/3 chance of saving 600 (0.33 x 600 = 200)
      + But, people tend to prefer Gamble A
      + They are more risk averse: preferring to take the “sure thing” 
  + In the example on the right, both gambles are frames in terms of loss...lives lost 
      + Program A indicates a definite outcome with no uncertainty...and Program B involves taking a major risk
      + The expected value of 100% certainty of losing 400 lives (1 x 400 = 400) is actually the same as the expected value of a 2/3 chance of losing 600 lives (.66 x 600 = 400)
      + But, people tend to prefer Program B
      + They are more risk-seeking now: they choose the option with more uncertainty, in an effort perhaps to avoid losing any lives

---
#  Irrational decisions 

<br>
<br>
.center[<iframe width="560" height="315" src="https://www.youtube.com/embed/V2EMuoM5IX4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>]

???

+ Sara Garafalo’s TedEd video (4:39) on decision-making that deviates from rational models
+ Good summary of loss aversion

---
##  Framing effects and health messaging 

+ A highly influential idea by Rothman and Salovey (1997) was to carefully design health-promoting messages to capitalize on known framing effects


+ Rothman, Martino, Bedell, Detweiler, and Salovey (1999) asked whether oral health behaviors could be influenced in accord with the model

<br>
.center[<img src="assets/img/image15.png" width=45%>]

???

+ One application of framing effects has been seen in the health domain
+ Researchers carefully designed health promoting messages based on the finding that gain framing causes risk aversion and loss framing cases risk seeking
  + because behaviors aimed at preventing health problems reduce risk, their model predicts that gain framing will be more effective than loss framing for these types of behaviors
  + also, because behaviors aimed at detecting health problems might feel risky to people bc they might find out something bad, the model predicts loss framing should be more effective for these types of behaviors
+ Rothman and colleagues looked at influencing oral health behaviors through framing
  + Asked students to read a pamphlet on either a disclosing mouth rinse (used to detect gum disease) or a plaque-reducing mouth rinse (used to lower the chance of gum disease by removing plaque)
  + People read with gain-framed or loss-framed messages about the rinses
      + 4 conditions
      + gain frame = highlighted benefits of locating plaque on teeth to prevent build-up (detection) or benefits of safely removing plaque with a rinse (prevention)
      + loss frame = focused on the risk of being unable to locate areas of plaque build-up (detection) or the risk of missing out on a safe way to remove plaque (prevention)

---
##  Framing effects and health messaging 

+ Rothman et al. (1999) findings:
  + People reported stronger intent to purchase and to use a *plaque removal* rinse if they read a gain framed pamphlet than if they read a loss-framed pamphlet
  + People more strongly intended to purchase and to use a *disclosing* rinse if they read a loss-framed pamphlet than if they read a gain-framed pamphlet

<br>
.right[<img src="assets/img/image16.png" width=25%>]

???

+ people reported stronger intent to purchase and use the plaque removal rinse if they had read a gain-framed pamphlet than a loss-framed one
  + with gain frames, we're more risk averse, and plaque removal rinse minimizes risks
+ whereas they more strongly intended to purchase and use the disclosing rinse if they had read a loss-framed pamphlet than a gain-framed one
  + with loss frames, we're more risk seeking, and disclosing rinses could lead to finding out something risky (like gum disease)
+ Being able to predict people’s choices given framing may be useful in designing the most persuasive possible messages

---
#  Choosing vs. rejecting 

+ Shafir (1993) reported a framing effect that occurs when we think of the choice task as one in which we are choosing an option versus rejecting an option


+ People violate the rational principle of procedure invariance

<br>
<br>
.center[<img src="assets/img/image17.jpg" width=60%>]

???

+ The framing of the choice task itself may even influence decisions
+ For example, Shafir (1993) reported a framing effect that occurs when we think of the choice task as either choosing an option *or* rejecting an option
+ Depending on which task people think they are choosing or rejecting, they give different weight to the different characteristics of each choice
  + what’s interesting is no matter whether people think they are choosing or rejecting, each is still the same task, it’s just framed differently
+ This violates the rational principle of procedure invariance
  + procedure invariance occurs when people’s preferences between choices do not change depending on whether they are asked to make the choice in one way or in another way that is essentially the same way as the first
  + Showing people 2 options and asking them to either choose one or reject one are equivalent tasks, but people’s judgments show they are making different choices depending on the task

---
##  To which parent would you award/deny sole custody of the child? 

.pull-left[
+ Parent A
  + Average income
  + Average health
  + Average working hours
  + Reasonable rapport with child
  + Relatively stable social life

<br>
<br>
.center[.red[
Award: 36%	
Deny: 45%]]
]

.pull-right[
+ Parent B
  + Above-average income
  + Minor health problems
  + Lots of work-related travel
  + Very close relationship with child
  + Extremely active social life

<br>
.center[.red[
Award: 64%  
Deny: 55%]]
]

<br>
<br>
<br>
.right[*Shafir, 1993*]

???

+ Let’s put this into context
+ Suppose you are a jury member on a child custody case in which one parent must be ruled the primary caregiver
  + judge might ask you to decide which parent you choose for this role...or the judge might instead ask which parent you reject for this role
  + A framing effect occurs when one of the parents has average attributes (Parent A) and the other has extreme attributes (both positive and negative; Parent B)
  + when people were asked to award custody, they awarded it to Parent B (64 vs 36)
  + But when people were asked to deny custody, they tended to deny it to Parent B (55 vs 45)
  + Thus, the framing of the question affected people’s judgments
      + If it hadn’t, the 2 percentages for Parent B should add up to 100%
+ So, depending on which task people think they are carrying out (e.g., awarding or denying), they give different weight to the different characteristics of each choice

---
##  Prospect theory .small-text[(Kahneman & Tversky, 1979; Tversky & Kahneman, 1992) ]

+ People’s decisions are reference dependent


+ Individuals view consequences (potential gains and losses) in terms of changes from a reference level, which is usually the *status quo*
  + Silver vs. bronze medalists

.center[<img src="assets/img/image04.png" width=55%>]

???

+ Other studies show people’s decisions depend on what reference level they are using as a standard of comparison
  + e.g., happiness of silver vs. bronze olympic medalists...we talked about this earlier in the context of counterfactual thinking
+ The reference level tends to be the status quo, but may also be some aspiration level
+ Along similar lines, a person’s judgment of their utility depends on the person’s current state of wealth
+ This tendency to be reference dependent caused problems for understanding decision-making through expected utility theory
+ So, Kahneman & Tversky proposed an influential descriptive model of risky choice called prospect theory
  + the prospects in this theory refer to the different "gambles" people choose when carrying out risky decision-making
  + e.g., which program to adopt in the disease outbreak

---
##  Prospect theory .small-text[(Kahneman & Tversky, 1979; Tversky & Kahneman, 1992) ]

<br>
<br>
.center[<iframe width="560" height="315" src="https://www.youtube.com/embed/sM91d5I36Po" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>]

???

+ Video illustrates reference dependence at the beginning (may need to pause to highlight the medalist part) and then provides a brief look at prospect theory that we will discuss next in detail

---
##  Prospect theory .small-text[(Kahneman & Tversky, 1979; Tversky & Kahneman, 1992) ]

+ Prospect theory accounts for the following major characteristics of people’s choices under uncertainty:
  + Framing effects
  + Loss aversion
  + Under certain conditions, people are reliably risk-seeking
  + People have non-linear preferences in risky choice
  + Certainty effect
  + Allais Paradox


+ People’s willingness to choose a risky option is influenced by the source of uncertainty

???

+ Prospect theory accounts for:
  + The framing effects we've been discussing
  + loss aversion 
      + people want to avoid losses more strongly than they want to gain gains
  + under certain conditions, people are reliably risk-seeking
      + when framed as losses, people are reliably prefer a gamble to sure thing
  + people have non-linear preferences in risky choice
      + in expected utility theory, outcomes are linear in the sense they are simply a product of the outcome and its probability
      + however, people’s actual preferences are nonlinear...we don’t always place value on things at their exact monetary value
  + certainty effect
      + we value a payoff that's certain more than a gamble...even if they have equal expected values
  + allais paradox
      + another example of how people’s preferences are nonlinear
      + we’ll come back to this later in the lecture
+ Prospect theory also suggests for people’s willingness to choose a risky option is influenced by the source of uncertainty
  + e.g., people tend to bet on uncertain events in a domain they feel they have some competence or expertise..even if they believe the other betting options to have the same likelihood of success

---
#  Prospect theory 

.pull-left[
+ There are two stages in prospect theory
  1. Editing alternatives
  1. Evaluation
      + Valuation
      + Decision weighting
      + Integration
]

.pull-right[<img src="assets/img/image06.png" width=100%>]

???

+ There are two stages in prospect theory
  + Editing the alternatives, which involves constructing a cognitive representation of the acts, contingencies, and outcomes relevant to the decision. This stage is where the reference level and framing effects have their impact
  + And, Evaluation, in which the decision maker assesses the value of each prospect and chooses accordingly
+ The evaluation stage can be broken into there steps for each prospect
  + Valuation, in which the value function is applied to each consequence associated with each outcome
  + Decision weighting in which each valued consequence is weighted for impact by a function based on its probability of occurrence
  + And finally, integration in which the weighted values across all the outcomes associated with each prospect  are combined by adding them up
+ Here's a diagram from the textbook of what those stages look like

---
##  Utility function for GAIN: Concave  

<br>
.center[<img src="assets/img/image07.png" width=100%>]

???

+ Prospect theory suggests people first tend to simplify each prospect into either a gain or a loss in order to make the choice less complex
+ People define gains and losses with respect to a reference point
  + It is the 0, 0 coordinate on this graph
+ When considering gains, the utility function tends to look a bit like this
  + The way people value gains diminishes as wealth (or the gains) increases
  + Shape of function is concave
+ Utility depends on what reference point we use:
  + If I have $0 and then find $100, the utility of that $100 is very high (illustrated by the y-axis)
  + However, if I have $800 and find $100, I’m still happy, but not nearly as much as I am when I’m broke
  + This is what is meant by diminishing (marginal) returns

---
##  Utility function for LOSS: Convex  

<br>
.center[<img src="assets/img/image08.png" width=100%>]

???

+ However, people treat losses differently
+ Because people are loss averse, because they want to avoid losses more strongly than they want to gain gains, the function takes on a different shape
+ So, when considering losses, the utility function tends to look like this
  + It’s now convex
+ Utility depends on what reference point we use:
  + If I have $0 and then lose $100, the utility of that $100 is very high (illustrated by the y-axis; though it’s negative now)
  + However, if I have $800 and lose $100, I’m still unhappy, but not nearly as much as I am when I’m broke

---
background-image: url('assets/img/image09.png')
background-size: cover

???

+ The subjective value function expresses how much people subjectively value each outcome of the prospect
+ Values in prospect theory are expressed as gains and losses relative to the reference point
  + 0, 0 coordinate on this graph
+ The subjective value v(x) we place on an outcome depends on whether we are considering gains or losses
  + we become risk-averse when thinking about gains...risk-seeking when thinking about losses
+ As gains increase, a person’s subjective value increases, but this tends to taper off over time
+ As losses increase, a person’s subjective value decreases at a steeper rate than it does for gains
  + If you look very closely, you can see a very drastic decrease near 0 on the loss side
  + This indicates people are very sensitive losses, even very little ones
+ This function is called the subjective value and is represented by the variable v, and describes how much people subjectively value each possible outcome

---
background-image: url('assets/img/image12.png')
background-size: contain

???

+ In contrast to the v function, the w function expresses how much the probability (P) of each outcome affects the overall value of the prospect
+ Having this function is necessary for a good descriptive model of risky choice because people don’t treat probability at face value
  + People’s choices are non-linear...as we’ll see when we discuss the Allais Paradox
+ Instead, we’re influenced by probabilities in distorted but predictable ways
+ w function basically describes how people are actually influenced by probabilities
  + can be thought of as the decision weight: the amount of influence each outcome has on people’s evaluations
  + describes how we tend to overweight low probabilities and underweight everything else (moderate to high probabilities)
  + turns out to be true for both gains and losses

---
#  Allais Paradox 

<br>
<br>
.center[<iframe width="512" height="332" src="https://player.pbs.org/viralplayer/3006557934/" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" seamless allowfullscreen></iframe>]

???

+ 

---
#  Allais Paradox 

+ Situation X: Choose between
  + G1: (1, $1000)
  + G2 :(0.10, $5000; 0.89, $1000; 0.01, $0)


+ Situation Y: Choose between
  + G3: (0.11, $1000; 0.89, $0)
  + G4: (0.10, $5000; 0.90, $0)

???

+ The Allais paradox is an example of how people’s preferences are non-linear
+ Situation X
  + Suppose you are asked to choose between 2 gambles:
  + G1: 100% chance you will win $1000
  + G2: 10% chance you will win $5000, an 89% chance you will win $1000, and a 1% chance you will win nothing
  + Which would you choose? (have class write down answer)
+ Situation Y
  + Now, consider another choice between 2 gambles:
  + G3: 11% chance you will win $1000 and an 89% chance you will win nothing
  + G4: 10% chance you will win $5000 and a 90% chance you will win nothing
  + Which would you choose? (have class write down answer)

---
#  Most people choose: 

+ Situation X: Choose between
  + .red[G1 (1, $1000)]
  + G2 (0.10, $5000; 0.89, $1000; 0.01, $0)


+ Situation Y: Choose between
  + G3 (0.11, $1000; 0.89, $0)
  + .red[G4 (0.10, $5000; 0.90, $0)]


+ Non-normative response according to EU theory

???

+ In situation X, people tend to prefer G1
+ In situation Y, people tend to prefer G4
+ Yet, according to expected utility theory, people who are rationally choosing should choose G1 and G3 or G2 and G4

---
#  Allais Paradox: Proof, Part I 

+ Situation X: Choose between
  + G1 (1, $1000)
  + G2 (0.10, $5000; 0.89, $1000; 0.01, $0)


+ Let:
  + U($5000) = A
  + U($1000) = B
  + U ($0) = C
  + EU(G1) = B
  + EU(G2) = 0.10A + 0.89B + 0.01C


+ Data:
  + EU(G1) > EU(G2)
  + B > 0.10A + 0.89B + 0.01C
  + B - .blue[0.89B] > 0.10A + 0.89B + 0.01C - .blue[0.89B]
  + 0.11B > 0.10A + 0.01C

???

+ Let’s say the utility of winning $5000 is some value A
  + The utility of winning $1000 is value B
  + The utility of winning $0 is value C
+ Now we can calculate the expected utilities of G1 and G2
  + EU(G1) = B
  + EU(G2) = 0.10A + 0.89B + 0.01C
+ Now, let’s go back to the data
  + People preferred G1 over G2, which can be represented as EU(G1) > EU(G2)
  + Plugging in the expected utilities we just calculated, we can express the inequality as B > 0.10A + 0.89B + 0.01C
  + Now, we can simplify a bit, subtracting 0.89B from each side of the equation
  + Which leaves us with 0.11B > 0.10A + 0.01C
+ We’ll come back to this in a moment

---
#  Allais Paradox: Proof, Part II

+ Situation Y: Choose between 
  + G3 (0.11, $1000; 0.89, $0) 
  + G4 (0.10, $5000; 0.90, $0) 


+ Let:
  + U($5000) = A
  + U($1000) = B
  + U ($0) = C
  + EU(G3) = 0.11B + 0.89C
  + EU(G4) = 0.10A + 0.90C


+ Data:
  + EU(G4) > EU(G3)
  + 0.10A + 0.90C > 0.11B + 0.89C
  + 0.10A + 0.90C - .blue[0.89C] > 0.11B + 0.89C - .blue[0.89C] 
  + 0.10A + 0.01C > 0.11B

???

+ Now, let’s consider the expected utilities of G3 and G4, plugging in the probabilities and utilities A, B, and C into the gambles
  + EU(G3) = 0.11B + 0.89C
  + EU(G4) = 0.10A + 0.90C
+ As we saw in the data, people preferred G4 to G3, which can be expressed as
  + 0.10A + 0.90C > 0.11B + 0.89C
+ To simplify this, we can subtract 0.89C from each side of the equation
+ Which comes out to
  + 0.10A + 0.01C > 0.11B

---
#  Summary of proof 

+ SO, when people prefer EU(G1) > EU(G2)
+ They mean...
  + .blue[0.11B > 0.10A + 0.01C]


+ And when people prefer EU(G4) > EU(G3)
+ They mean...
  + .blue[0.10A + 0.01C > 0.11B]

<br>
<br>
<br>
<br>
.center[***.red[This REVERSAL of preference is the Allais Paradox]***]

???

+ Bringing together the two parts of the proof to sum it all up...
+ When people say they prefer G1 over G2, they are saying
  + 0.11B > 0.10A + 0.01C (final equation from Part I)
+ When people say they prefer G4 over G3, they are saying
  + 0.10A + 0.01C > 0.11B (final equation from Part II)
+ Note now these 2 equations are exactly the same except for the direction of the inequality sign
+ Regardless of what utility each person holds for the values of A, B, and C, people should either consistently prefer one side of these equations or the other 
  + e.g., 0.10A + 0.01C over 0.11B in both situations
  + Their preferences should be consistent if they are making decisions according to expected utility theory...but they aren't
+ This so-called “paradox” is really only a paradox if we expected people’s choices to fall in line with expected utility theory
+ From the perspective of prospect theory, this inconsistency is simply described as what people do

---
##  The decision weight (w) function: Overweighting of very low probabilities

+ Helps to illustrate the Allais Paradox


+ Situation X: Choose between
  + G1 (1, $1000)
  + G2 (0.10, $5000; 0.89, $1000; 0.01, $0)
  + .blue[0.01 of winning nothing “seems” bigger than it ought to]


+ Situation Y: Choose between
  + G3 (0.11, $1000; 0.89, $0)
  + G4 (0.10, $5000; 0.90, $0)
  + .blue[Difference between 0.11 and 0.10 “seems” smaller]

???

+ So in the context of prospect theory, the difference between these preferences has to do with the decision weight (w) function
+ Remember, as probabilities get smaller, we tend to overweight them more
+ So, in situation X, we overestimate the low probability of winning nothing
  + 0.01 seems bigger than it should because we are comparing to the 0% chance that we will win nothing in G1
+ But in situation Y, we underestimate the difference between .11 and .10
  + Even though the difference between these values is still .01, the reference point is different..we're comparing them to each other
  + So when we look at the difference between .11 and .10, it seems smaller

---
background-image: url('assets/img/image11.png')
background-size: cover

???

+ Point out the increase in slope from 0 to .01
+ Point out the increase in slope from .10 to .11
+ Note how the slope is steeper for 0-.01 than it is from .10-.11
