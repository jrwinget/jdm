<!DOCTYPE html>
<html lang="" xml:lang="">
  <head>
    <title>Choice &amp; Mental Accounting</title>
    <meta charset="utf-8" />
    <meta name="author" content="PSYC/PHIL 279, Winget" />
    <link href="libs/remark-css-0.0.1/default.css" rel="stylesheet" />
    <link href="libs/remark-css-0.0.1/default-fonts.css" rel="stylesheet" />
    <link href="libs/remark-css-0.0.1/metropolis-fonts.css" rel="stylesheet" />
    <link rel="stylesheet" href="my-style.css" type="text/css" />
  </head>
  <body>
    <textarea id="source">
class: center, middle, inverse, title-slide

# Choice &amp; Mental Accounting
### PSYC/PHIL 279, Winget

---

#  Choice 

+ Complex organisms do not need to strive for the best possible situations in order to survive
  + They only need only find situations that are ‘good enough’ (Simon, 1956)
+ Theories of bounded rationality account for uncertainty, lack of complete information about the choices available, and the complexity of the decision that is being made (Simon, 1972)
+ Choice overload effect
  + Most likely to happen when people aren’t very familiar with the things they are choosing between
  + Haven’t developed a strong preference for one of the options

.right[&lt;img src="assets/img/image1.png" width=325&gt;]

???

+ Life is made of a series of choices about whether and how to spend our resources of time, money, effort, and opportunity
+ Today, we’ll talk about what factors influence how we choose to expend these resources
+ And whether or not those choices align with our values and goals
+ 
+ Choice refers to a selection made from 2 or more options 
  + E.g., whether to purchase from one store or another
+ We've already discussed how the classic theories of rational choice assume people have complete knowledge about their choices and select the most optimal choice whenever faced with a decision
+ What we really do instead is satisfice
  + We don't need to maximize to survive (and we rarely do)...so we look for outcomes that are “good enough”
  + It makes more sense to have theories of bounded rationality
      + Despite this, businesses seem to think having more options is better...more options = more freedom, more likely to get what you want
      + But, is this really the case?
      + The research suggests when given too many choices, people may actually have a more difficult time making a decision
      + However, if people have a strong preference for one of the choices or are familiar with the things they are choosing between, this choice overload effect may be mitigated or even eliminated

---
#  Satisficing vs. maximizing 

+ Fewer options
  + Free rein to satisfice
  + Make a decision that will be good enough without requiring strenuous mental gymnastics
+ Many options
  + Pressure to maximize
  + Attempt the best-quality decision

&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
.right[&lt;img src="assets/img/image7.png" width=300&gt;]

???

+ Maximizing and satisficing can be thought of as 2 different end goals of a choice
+ If one has few options to choose from, a person might feel like they've been given free rein to satisfice
  + Their decision doesn't require one to do a lot of strenuous mental gymnastics or asses every possible choice
+ But if a person is given a lot of options, this might encourage maximizing
  + With so much information available to consider, a person might feel pressured to make a best-quality decision

---
#  Satisficing vs. maximizing 

+ Schwartz et al. (2002) found that maximizing was:
  + Positively correlated with feeling regretful, depressed, and being perfectionistic
  + Negatively correlated with feeling happy, feeling satisfied with life, being optimistic, and having high self-esteem
+ They speculated that possessing the drive to maximize one’s decisions makes it difficult to feel fully content with whatever the outcome may be
+ One is more pleased and content with one’s choices *after the fact* if one was not trying to maximize them at the time that the choices were made

&lt;br&gt;
.center[&lt;img src="assets/img/image8.png" width=250&gt;]

???

+ There might be individual differences that explain how much a person maximizes vs. satisfices
  + Schwartz et al. (2002) developed Maximization Scale to measure individual’s desire to maximize
  + Found maximizing was **positively** correlated with feeling regretful, depressed, and being perfectionistic
  + Maximizing was also **negatively** correlated with feeling happy, feeling satisfied with life, being optimistic, and having high self-esteem
  + In other words, the tendency to maximize doesn't really predict satisfaction
+ Might be because maximizing makes it difficult to feel fully content with whatever the outcome may be
+ It could be that people are actually more pleased and content with their choices after the fact if they were not trying to maximize when making choices
  + So in this respect, satisficing might be the better approach

---
#  Purchasing decisions 

+ Seeing a product activates neural circuits associated with thinking about gains
+ Seeing the price of a product activates neural circuits associated with thinking about losses
+ Knutson et al. (2007) tested whether examination of activation in the medial prefrontal cortex (associated with integrating gain and loss information) could be used to predict purchasing decisions
+ As consumers, an awareness of research findings on price and product primacy could be useful in helping us figure out ways to spend our resources in a way that best meets our goals

.center[
&lt;img src="assets/img/image9.jpg" width=200 height=100&gt;
&lt;img src="assets/img/image10.svg" height=100&gt;
]
.center[
&lt;img src="assets/img/image11.jpg" width=200 height=100&gt;
&lt;img src="assets/img/image12.png" height=100&gt;
]

???

+ When it comes to purchasing decisions, researchers look at people’s evaluations of price, but also how they feel about the product
  + Reflects a dual-process model
+ Since people are not always aware of the factors influencing their choices, researchers have turned to neuroimaging techniques to help identify underlying decision processes
+ This research shows that seeing a product activates neural circuits associated with thinking about gains...whereas seeing the price of the product instead activates distinct neural circuits associated with thinking about loses 
+ Researchers hypothesized they could predict whether or not a person will purchase a product by examining the pattern of neural activation during the decision making process
  + To test, they had participants view products and decide to purchase them during an fMRI scanning
      + They found certain neural pathways associated with being more likely to purchase a product
      + Also found distinct neural pathways associated with being less likely to purchase a product
+ Findings like these have important implications for understanding how people choose to spend their resources
  + For consumers, an awareness of research on price and product primacy could be useful in figuring out ways to spend money that best meets their goals

---
#  Iyengar: Choosing what to choose 

&lt;br&gt;
.center[
&lt;a href="https://www.ted.com/talks/sheena_iyengar_choosing_what_to_choose/transcript"&gt;&lt;img src="assets/img/image2.png" width=600/&gt;
&lt;/a&gt;
]

???

+ Sheena Iyengar explains the choice overload effect in the context of her “jam study” (16:05)
+ Click on picture 
+ Stop around 6:49 (9:09 remaining)..." The main reason for this is because, we might enjoy gazing at those giant walls of mayonnaises, mustards, vinegars, jams, but we can't actually do the math of comparing and contrasting and actually picking from that stunning display."
+ Iyengar mentions how the choice overload effect can harm our future financial well-being

---
#  Mental accounting 

+ Individuals and households
+ Not governed by formal rules
+ Still certain tendencies that people seem to follow implicitly, without necessarily knowing that many others do the same (Thaler, 1999)
  + e.g., people tend to mentally categorize types of expenditures and types of income, and treat these categories differently

&lt;br&gt;
.center[&lt;img src="assets/img/image13.jpg" width=450&gt;]

???

+ Another explanation for this comes from where we think our resources/finances are coming from
+ Keeping track of and handling our resources is a process called mental accounting
+ Individuals, groups, and households all do this...and it does not appear to be conducted by any sort of formal rules
+ Yet, there are still certain tendencies that people seem to implicitly follow
  + First, people tend to mentally categorize types of expenditures and income...and then they treat each category differently
  + E.g., a family that is reluctant to spend its hard-earned savings on a week-long holiday trip may feel better about the expense if they think they are paying for it out of a small bonus from work rather than their savings

---
#  Mental accounting 

+ The behavioral life-cycle hypothesis (Shefrin &amp; Thaler, 1988)
  + People mentally divide their wealth into three accounts:
      1. Current income
      1. Current assets
      1. Future wealth
  + People will encounter temptations to spend (vs. save) their money
  + Whether or not people save is influenced by how the wealth is described

&lt;br&gt;
.center[
&lt;img src="assets/img/image14.jpg" width=225&gt;
&lt;img src="assets/img/image15.jpg" width=225&gt;
&lt;img src="assets/img/image16.jpg" width=225&gt;
]

???

+ The behavioral life-cycle hypothesis attempts to specify how this all works
+ It factors in 3 characteristics of mental accounting that were not generally addressed by traditional economic theories:
  1. It reflects how people treat different categories of money differently in mental accounting (divide wealth into 3 categories)
      + Current income: paychecks fall into this category
      + Current assets: things like nonretirement savings...like rainy day funds
      + Future wealth: retirement savings and future income
  1. It takes into account the fact people will encounter temptations to spend their money instead of save it
  1. It accounts for the fact that whether or not people save is influenced by how the wealth is described or framed
      + E.g., if one received a salary increase (current income) at work, it will be categorized differently than if it is framed as a bonus (current assets), even if the amounts are the same
      + This matters: Research shows people are more likely to spend current income than current assets

---

+ As the president of an airline company, you have invested $10 million of the company's money into a research project. The purpose was to build a plane that would not be detected by conventional radar. When the project is 90% completed, another firm unveils a new plane that cannot be detected by radar. Also, it is apparent that their plane is much faster and far more economical than the plane your company is building.

+ The question is: *should you invest the last $1 million of the research funds to finish the radar-blank plane?*

???

+ Consider this situation
  + How many of you say yes?...How many say no?
+ Example of sunk cost
  + Likely to spend example

--

&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
.right[
.red[Yes: 85%]
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
*Arkes &amp; Blumer (1985)*
]

---

+ As the president of an airline company, you are considering investing the company’s money into a research project. The purpose is to build a plane that cannot be detected by conventional radar. Another firm, however, has just unveiled a new plane that cannot be detected by radar. Also, it is apparent that their plane is much faster and far more economical than the plane your company is considering building.

+ The question is: *should you invest $1 million in research funds toward building your company’s radar-blank plane?*

???

+ Consider this situation
  + How many of you say yes?...How many say no?
+ Both of these are examples of sunk cost
  + First was the likely to spend example
  + This is the not likely to spend example
+ Only difference between the 2 is whether or not you've already invested money in the project when you find out about the new plane

--

&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
.right[
.red[Yes: 17%]
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
*Arkes &amp; Blumer (1985)*
]

---
#  Sunk costs  

+ Resources that have already been spent
+ Sunk costs should not factor into decisions about the future, but do (Arkes &amp; Blumer, 1985)

&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
.center[&lt;img src="assets/img/image17.jpg" width=500&gt;]

???

+ Sunk costs are resources that have already been spent
+ And people tend to treat money they've already spent on something as though that is a good reason to continue to put down even more money for it...even if it’s clear the extra spending will not make them happier overall
+ Suppose a couple is looking to build a home
  + They agree on a budget that will use all of their savings, purchase a piece of land, and hire builders
  + As the builders are working, they discover there is a huge slab of underground rock covering most of the land, and it must be removed to build the house--a very expensive, unforeseen cost
  + Now, the couple has a choice: they could cut their losses and look for a new house or piece of land...or they could take out a major loan, go into debt, proceed with the original plan, and hope no other problems arise
+ Traditional economic theories say this should be straightforward
  + A sum of money has already been spent on a service/good = the sunk cost
  + The current decision is not about past costs, it’s about future costs...whether they should go into unplanned debt to build a house or stay out of debt
  + So according to traditional economic theories, the sunk costs should be irrelevant to the current decision
  + Yet, people often find it very difficult to walk away from what they have already spent toward achieving a goal...and they may often even choose to spend much more money than originally planned toward achieving that original goal

---
#  Time vs. money 

+ People are used to keeping track of money (i.e., mentally accounting for it) and thinking of it as a valuable commodity, but they aren’t necessarily used to doing so for time (Soman, 2001)
+ Lee et al. (2015) found that preferences were significantly more stable for judgments based on time than they were for judgments based on money

&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
.right[&lt;img src="assets/img/image18.jpeg" width=300&gt;]

???

+ There’s some evidence suggesting people treat different types of resources (e.g., time and money) differently
  + Money can be held and saved...but time passes by and is gone
  + People tend to keep track of money and thinking of it as a valuable commodity...but they aren’t necessarily used to doing this for time
+ Based on this idea, Lee and colleagues (2015) hypothesized money and time may activate each of the two types of information processing
  + They argued money activated System 2 processing (deliberate, analytical)
      + We think deliberately about economic exchanges
  + And time activated System 1 processing (heuristic, potentially related to affect)
  + Further hypothesized when people state preference among 2 or more choices, their preference remains more consistent if this decision was based on affective processing
      + Reasonably likely to feel the same about a preference at different points in time
      + If we think analytically about the preference, we might weight factors differently at different points in time, which leads to less overall stability
  + Results supported both of these ideas...preferences were more stable for judgments based on time than for judgments based on money

---
#  Nudges 

+ Our decisions regarding whether and how to spend our resources of time, money, effort, and opportunity can be swayed by others’ preferences:
  + Mandates and bans
  + Economic incentives (and disincentives)
  + Nudges

&lt;br&gt;
&lt;br&gt;
.center[
&lt;img src="assets/img/image19.jpeg" height=150 width=200&gt;
&lt;img src="assets/img/image20.JPG" height=150 width=200&gt;
&lt;img src="assets/img/image21.jpg" height=150 width=200&gt;
]

???

+ Our decisions about how to spend our resources can be swayed by others’ preferences in several ways
+ Others’ preferences can be expressed at a societal or group level through mandates and bans, economic incentives and disincentives, and nudges
  + Mandates and bans
      + Explicitly state one is required to make certain choices 
      + e.g., **banning plastic straws in a city or state to reduce plastic pollution**
  + Economic incentives (and disincentives)
      + Encourage people to make certain choices by linking monetary gains/losses to decisions
      + **Plastic bag tax**
  + Nudges
      + Encourage people to make certain choices but while retaining autonomy
      + e.g., **make paper bags and reusable bags more accessible/noticeable**

---
#  Nudges 

+ Sunstein (2014) defined these as “liberty-preserving approaches that steer people in particular directions, but that also allow them to go their own way”
+ The means by which societal institutions can carry out libertarian paternalism (Thaler &amp; Sunstein, 2008)

.center[&lt;img src="assets/img/image22.jpg" width=600&gt;]

???

+ Sunstein (2014) called nudges “liberty-preserving approaches that steer people in particular directions, but also allow them to go their own way”
+ They’re the means by which some researchers suggested societal institutions could achieve libertarian paternalism
  + Libertarian paternalism is the idea that institutions can influence people’s behavior while still fundamentally upholding people’s rights to liberty and free choice
+ The idea is that nudges are intended to encourage people to do things that would be good for them (e.g., having fewer sugary drinks) and/or for society (e.g., donating one’s organs upon death)
+ Thaler and Sunstein (2008) argued the influence of nudges can never really be escaped, even if the nudges are not intentionally placed
  + E.g., if fatty foods (e.g., french fries) happen to be placed at the eye level of kids in a school cafeteria, most students will choose to eat them than if they were placed higher up
  + If an apple or banana had happened to be at eye level, kids would be more likely to choose those items
  + Even if the foods are randomly placed, whatever food is most salient in the environment will still affect kids’ choices
  + One way or another, our choices are influenced by the structure of the environments we are in
  + This is the idea of libertarian paternalism: rather than leave this to chance, we might as well implement nudges that guide people toward decisions we know will be more beneficial rather than ones we know will be harmful

---
#  Types of nudge (Sunstein, 2014) 

+ Increase the ease or availability with which a choice is made apparent
+ Educational
+ The default rule
  + Sets the default to the desired response
  + Capitalizes on the status quo bias

???

+ There are many different types of nudges that can be implemented
  + One type of nudge is one that increases the ease or availability with which a choice is made apparent
      + E.g. to encourage people to buy locally farmed produce, a grocery store might place it at the front entrance where it’s most likely to be seen
      + To encourage voting, a city might open up additional voting locations and hire extra staff
  + Another type of nudge is educational in nature
      + These nudges seek to provide people with factual information about different choices (e.g., mostly like consequences of choices, findings from research, etc.)
      + E.g., warning labels on cigarettes or artificial sweeteners
  + A third type of nudge, the default rule, sets the default to the desired response
      + This nudge capitalizes on the status quo bias: people’s tendency to choose to do nothing
      + E.g., a country that wants to encourage organ donors might, by default, have people sign up to be an organ donor when they register for an ID or driver’s license
      + Thus, people are organ donors by default but they are free to opt out at any time they wish before death

---
#  Nudges 

+ Who knows best: the individual, or the societal institution which nudges?
  + This is a source of debate
+ It is important for people to be aware of the choice architecture of their environments, and for governments to be transparent about them

&lt;br&gt;
&lt;br&gt;
.center[&lt;img src="assets/img/image23.jpeg" width=400&gt;]

???

+ The use of nudges by societal institutions (e.g., the government) brings up an important question...who knows what's best: institutions like the government, or the individual?
+ Some have argued individuals themselves are generally going to be better at figuring out which choices would be best for them because they have more mental access to the reasons they’re making their choices
+ Others have made the case that people are not always better at judging their own best interests than others
  + E.g., sometimes individuals are not fully informed about the different consequences of each choice
  + Scientists who have spent a lifetime studying the effects of tobacco are likely to know more about it than people who have not done this sort of work...and many nudges are educational
  + Some research suggests people do not always have conscious access to the reasons for their decisions
+ It's a debate we won't solve today...but it’s important for people to be aware of the choice architecture of their environments and for governments to be transparent about them

---
#  Analyzing reasons vs. feelings 

+ When making difficult decisions, people often spend a good deal of time thinking about the advantages and disadvantages of the alternatives

&lt;br&gt;
&lt;br&gt;
.center[
&lt;img src="assets/img/image4.png" width=400 height=250&gt;
&lt;img src="assets/img/image3.png" width=200 height=250&gt;
]

???

+ So nudges have their limitations, but so does self-reflection for why we make the choices we do
  + When making difficult decisions, we spend a good deal of time thinking about the advantages and disadvantages of the alternatives
+ We may even pull out a sheet of paper and write the pluses and minuses of each choice
+ Reflection of this kind is often thought to be beneficial to decision making...we're organizing what may be a confusing jumble of thoughts and feelings about each alternative option into something more coherent
+ In fact, Benjamin Franklin proposed creating a pro and con sheet as an aid to decision making
  + Why do I like this car versus that car
  + Why do I like this college versus that college
+ Advocated analyzing the *reasons* why we prefer one thing over another

---
#  Analyzing reasons vs. feelings 

+ However, such introspection about reasons can reduce satisfaction with our choices
+ The reason is that while we know how we feel about something, we rarely know why we feel a particular way

&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
.center[&lt;img src="assets/img/image24.jpg" width=400&gt;]

???

+ But, such self-reflection about reasons can reduce our satisfaction with our choices
+ The reason is that even though we know *how* we feel about something, we rarely know *why* we feel a particular way
+ So when we reflect on reasons, we focus on explanations that are salient and seem plausible
+ The problem is that what seems like a plausible cause and what actually determines people’s reactions are not always the same thing

---
#  Reasons-generated attitude change 

+ Difficult to put into words exactly *why* we feel the way we do
+ The reasons that come to mind sometimes differ from our “gut” feelings
+ People assume that their reasons reflect their feelings, even if they do not

&lt;br&gt;
&lt;br&gt;
.center[&lt;img src="assets/img/image25.jpg" width=500&gt;]

???

+ It's often difficult to put into words exactly *why* we feel the way we do...and the reasons that usually come to mind sometimes differ from our “gut” feelings
+ The problem stems from the fact that people assume that their reasons for their feelings actually reflect their feelings...even if they don't
+ Gut feelings or intuition can sometimes be informative
+ But we SHOULDN’T ALWAYS BE IMPULSIVE 
  + If someone makes you angry, punch them
  + Marry the first person that you have feelings for
+ Similarly, we shouldn't always use our gut feelings to explain why we feel the way we do

---
#  Analyzing reasons leads to suboptimal choices 

+ Wilson and Schooler (1991) had students take part in a “Jam Taste Test”
+ One group was asked to analyze **why** they feel the way they do about the jams
+ Another group was told to taste the jams and provide their ratings
+ All groups ratings of the jams were compared to expert ratings

.center[&lt;img src="assets/img/image5.png" width=400&gt;]

???

+ To test this idea, Wilson and Schooler had students take part in a "Jam Taste Test"
+ Half of the participants were asked to rate the jams and analyze why they felt the way they did about the jams
+ And the other half was asked to taste the jams and simply provide their ratings
+ All of the participants' ratings were compared to expert ratings of the same jams
+ Expert ratings came from Consumer Reports...and was used to as a measure of whether participant choices were good
+ The closer the ratings of individual jams were to expert ratings, the better choices the participants made

---
class: inverse
background-image: url('assets/img/image26.png')
background-size: contain

???

+ Results revealed that the ratings of people in the control condition (just rated the jams) more closely resembled those of experts than those in the reasons condition
+ Being asked to analyze reasons caused people to change their evaluations of the jams
  + They focused on plausible, but wrong, reasons for why they liked a jam
+ When researchers looked at the reasons people generated, they did not correlate with the reasons why certain jams were good or bad generated by experts

---
#  Analyzing reasons leads to unsatisfying choices 

+ Wilson et al. (1993) looked at whether analyzing reasons would reduce post-choice satisfaction
+ People evaluated posters with one group analyzing reasons and the other group served as control
+ Took home poster and asked three weeks later how satisfied with poster

???

+ In another study, Wilson and colleagues tested whether analyzing reasons would reduce post-choice satisfaction
+ Participants in this study were asked to evaluate a series of posters
+ One group was asked to introspect about the reasons for their preference for each poster
+ Another group was simply asked to evaluate the posters
+ Each group chose one poster to take home with them
+ Three weeks later they were asked how satisfied they were with their choice
+ Participants in the reasons condition were significantly less satisfied with their choices

---
#  Wilson et al. (1993)

&lt;br&gt;
.center[
&lt;video width="560" height="315" controls&gt;
    &lt;source src="assets/self-knowledge.mp4" type="video/mp4"&gt;
&lt;/video&gt;
]

???

+ Here's Tim Wilson explaining some more details of the study

---
#  Advice for everyday life 

+ Do not act impulsively
+ Develop ***informed*** gut feelings
+ Once you have them, do not spend too much time analyzing them, and do not worry if you can’t explain your feelings

&lt;br&gt;
&lt;br&gt;
.center[&lt;img src="assets/img/image6.png" width=400&gt;]

???

+ Given these findings, there are some suggestions for curbing these effects in your everyday lives
+ First, when you have a gut reaction or impulse, try to take a moment before acting on it bc it might not be the best approach
+ By doing this, you can DEVELOP A RELIABLE SENSE OF REASONS, AND INFORMED GUT FEELINGS, TO GUIDE FUTURE DECISIONS
  + But this does take some effort and time to do
+ Once you have these informed/refined gut feelings, do not spend too much time analyzing them
  + Remember, analyzing reasons can lead to unsatisfying outcomes
  + Along these lines, don't worry if you can't explain them either
+ This approach allows you to sort of train your System 1 to be more reliable in the future while still retaining your satisfaction for your decision
    </textarea>
<style data-target="print-only">@media screen {.remark-slide-container{display:block;}.remark-slide-scaler{box-shadow:none;}}</style>
<script src="https://remarkjs.com/downloads/remark-latest.min.js"></script>
<script>var slideshow = remark.create({
"highlightStyle": "github",
"highlightLines": true,
"countIncrementalSlides": false
});
if (window.HTMLWidgets) slideshow.on('afterShowSlide', function (slide) {
  window.dispatchEvent(new Event('resize'));
});
(function(d) {
  var s = d.createElement("style"), r = d.querySelector(".remark-slide-scaler");
  if (!r) return;
  s.type = "text/css"; s.innerHTML = "@page {size: " + r.style.width + " " + r.style.height +"; }";
  d.head.appendChild(s);
})(document);

(function(d) {
  var el = d.getElementsByClassName("remark-slides-area");
  if (!el) return;
  var slide, slides = slideshow.getSlides(), els = el[0].children;
  for (var i = 1; i < slides.length; i++) {
    slide = slides[i];
    if (slide.properties.continued === "true" || slide.properties.count === "false") {
      els[i - 1].className += ' has-continuation';
    }
  }
  var s = d.createElement("style");
  s.type = "text/css"; s.innerHTML = "@media print { .has-continuation { display: none; } }";
  d.head.appendChild(s);
})(document);
// delete the temporary CSS (for displaying all slides initially) when the user
// starts to view slides
(function() {
  var deleted = false;
  slideshow.on('beforeShowSlide', function(slide) {
    if (deleted) return;
    var sheets = document.styleSheets, node;
    for (var i = 0; i < sheets.length; i++) {
      node = sheets[i].ownerNode;
      if (node.dataset["target"] !== "print-only") continue;
      node.parentNode.removeChild(node);
    }
    deleted = true;
  });
})();
(function() {
  "use strict"
  // Replace <script> tags in slides area to make them executable
  var scripts = document.querySelectorAll(
    '.remark-slides-area .remark-slide-container script'
  );
  if (!scripts.length) return;
  for (var i = 0; i < scripts.length; i++) {
    var s = document.createElement('script');
    var code = document.createTextNode(scripts[i].textContent);
    s.appendChild(code);
    var scriptAttrs = scripts[i].attributes;
    for (var j = 0; j < scriptAttrs.length; j++) {
      s.setAttribute(scriptAttrs[j].name, scriptAttrs[j].value);
    }
    scripts[i].parentElement.replaceChild(s, scripts[i]);
  }
})();
(function() {
  var links = document.getElementsByTagName('a');
  for (var i = 0; i < links.length; i++) {
    if (/^(https?:)?\/\//.test(links[i].getAttribute('href'))) {
      links[i].target = '_blank';
    }
  }
})();
// adds .remark-code-has-line-highlighted class to <pre> parent elements
// of code chunks containing highlighted lines with class .remark-code-line-highlighted
(function(d) {
  const hlines = d.querySelectorAll('.remark-code-line-highlighted');
  const preParents = [];
  const findPreParent = function(line, p = 0) {
    if (p > 1) return null; // traverse up no further than grandparent
    const el = line.parentElement;
    return el.tagName === "PRE" ? el : findPreParent(el, ++p);
  };

  for (let line of hlines) {
    let pre = findPreParent(line);
    if (pre && !preParents.includes(pre)) preParents.push(pre);
  }
  preParents.forEach(p => p.classList.add("remark-code-has-line-highlighted"));
})(document);</script>

<script>
slideshow._releaseMath = function(el) {
  var i, text, code, codes = el.getElementsByTagName('code');
  for (i = 0; i < codes.length;) {
    code = codes[i];
    if (code.parentNode.tagName !== 'PRE' && code.childElementCount === 0) {
      text = code.textContent;
      if (/^\\\((.|\s)+\\\)$/.test(text) || /^\\\[(.|\s)+\\\]$/.test(text) ||
          /^\$\$(.|\s)+\$\$$/.test(text) ||
          /^\\begin\{([^}]+)\}(.|\s)+\\end\{[^}]+\}$/.test(text)) {
        code.outerHTML = code.innerHTML;  // remove <code></code>
        continue;
      }
    }
    i++;
  }
};
slideshow._releaseMath(document);
</script>
<!-- dynamically load mathjax for compatibility with self-contained -->
<script>
(function () {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src  = 'https://mathjax.rstudio.com/latest/MathJax.js?config=TeX-MML-AM_CHTML';
  if (location.protocol !== 'file:' && /^https?:/.test(script.src))
    script.src  = script.src.replace(/^https?:/, '');
  document.getElementsByTagName('head')[0].appendChild(script);
})();
</script>
  </body>
</html>
