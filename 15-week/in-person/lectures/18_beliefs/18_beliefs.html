<!DOCTYPE html>
<html lang="" xml:lang="">
  <head>
    <title>Beliefs</title>
    <meta charset="utf-8" />
    <meta name="author" content="PSYC/PHIL 279, Winget" />
    <link href="libs/remark-css-0.0.1/default.css" rel="stylesheet" />
    <link href="libs/remark-css-0.0.1/default-fonts.css" rel="stylesheet" />
    <link href="libs/remark-css-0.0.1/metropolis-fonts.css" rel="stylesheet" />
    <link rel="stylesheet" href="my-style.css" type="text/css" />
  </head>
  <body>
    <textarea id="source">
class: center, middle, inverse, title-slide

# Beliefs
## Everyday, Superstitious, Magical, &amp; Paranormal
### PSYC/PHIL 279, Winget

---

#  What are beliefs 

+ Beliefs are propositional attitudes
	+ Propositions are generally taken to be whatever it is that sentences express (e.g., *Armadillos are tasty appetizers*)
	+ Beliefs are, then, the mental state of having some attitude, stance, take, or opinion about the truth-value of a proposition
	
&lt;br&gt;
.center[&lt;img src="assets/img/image1.png" width=400&gt;]

???

+ Mention exam and decision diary (no new content for this one)
+ Today, we'll talk about how our beliefs guide us through our daily lives
+ We have all sorts of beliefs...some more general like everyday beliefs, and some more specific like magical or paranormal beliefs
+ But they all influence our decision-making process to a significant degree
+ 
+ So to begin, we should discuss what beliefs actually are...
+ Beliefs are propositional attitudes
+ And, propositions are basically T/F conclusions about whatever a statement expresses
+ e.g., I believe that armadillos are tasty appetizers (or I don’t)
+ So, beliefs are essentially when we have some mental attitude, opinion, stance, etc. about the truth of a propositional statement
+ As another example, to believe that armadillos have four legs requires that a person has the information armadillos have four legs stored in their mental system and that they believe the information correctly characterizes some real state of affairs
+ This means there are 2 components to belief:
  + The representation component of believing refers merely to the existence of meaningful information within a mental system
  + The assessment component refers to the relation between that information and other information that already exists within the system

---
#  How mental systems believe (Gilbert, 1991) 

+ If we were to design a device to believe, what is the most logical way for us to go about doing so?
	+ The most obvious answer is that one must comprehend an idea before one can assess it (i.e., determine if it is true or false)
	+ Rene Descartes was the first modern thinker to formalize this rather simple notion

???

+ If we were to take a step back and try to create some sort of device that has beliefs, we'd have to find a logical way to go about doing so
+ Often, when we think about how the human mind believes, it's natural to jump to the question of how mental systems **should** believe
  + The obvious suggestion would be that we have to comprehend an idea before we can assess it...
  + We have to understand something before we can determine if it's true or not, right?
+ In this way, comprehension is thought to precede assessment...and it's important to note that comprehension and assessment are separate processes
+ Rene Descartes was the first modern thinker to formalize this idea by partitioning the mind into relatively active (controlled) and passive (automatic) domains
+ Comprehension, he claimed, was passive...Ideas impressed themselves upon the mind as physical objects might upon soft wax
+ And just as wax did nothing to receive the object's impression, so the mind did nothing to comprehend the world around it

---
#  The Cartesian Procedure 

+ According to the Cartesian Procedure, although having ideas was effortless and automatic, accepting or rejecting them was effortful and controlled

&lt;br&gt;
.center[
.pull-left[&lt;img src="assets/img/image2.png" height=300&gt;]
.pull-right[&lt;img src="assets/img/image3.png" height=300&gt;]
]

???

+ This brings us to the Cartesian Procedure...although having ideas was effortless and automatic, accepting or rejecting them was effortful and controlled
+ e.g., We may represent the proposition that cats dislike raisin cookies before or even without assessing the veracity (or truth-value) of that proposition
+ This procedure is the dominant view among philosophers, and people more generally
+ And, it seems so intuitively appealing and true that this is how computer programming languages often work
+ The operations of comprehension and assessment are as separate as everybody knows they should be

---
#  That is, almost everybody 

+ Baruch Spinoza argued that to comprehend a proposition, a person had implicitly to accept that proposition
	+ Only later, if the person realized that this proposition conflicted with some other, they might change their mind
	+ We automatically comprehend and accept ideas, and only later through controlled processing unaccept them

&lt;br&gt;
&lt;br&gt;
.right[&lt;img src="assets/img/image4.png" width=400&gt;]


???

+ That is, *almost* everybody thought comprehension and assessment are separate processes...everyone except for Spinoza
+ Spinoza tossed away Descartes’ distinction between comprehension and assessment, arguing instead that they were the same operation
+ His idea was that people believe in the ideas they comprehend, as quickly and automatically as they believe in the objects they see
+ Only after accepting and deliberately reflecting on these ideas can we change our minds and unaccept them

---
#  A comparison of the two procedures 

&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
.center[&lt;img src="assets/img/image5.png" width=700&gt;]


???

+ So let's break these ideas down a bit
+ Think of the representation stage occurring in System 1 and the assessment stage occurring in System 2
  + The key to distinguishing between the Cartesian and Spinozan hypotheses is to consider what happens when System 2 processing fails
+ When this happens in the Cartesian model, because the assessment stage has not occurred, a person should not be able to determine whether an idea is true or false
  + The idea is represented in the mind, but not tagged as true or false
+ When this happens in the Spinozan model, a person should believe an idea to be true...comprehension and acceptance are one and the same

---
#  When systems break down 

+ The Spinozan hypothesis asserts that rejecting an idea requires the extra step of unaccepting
	+ If this is so, then people should initially accept both true and false ideas upon comprehension, but when the processing of the idea is interrupted, should not be able to go on and unaccept (or tag) false ideas
	+ Asymmetry between true and false ideas as a result of interruption
+ The Cartesian model, on the other hand, considers acceptance and rejection to be alternative consequences of a single assessment process
  + No asymmetry between true and false ideas as a result of interruption


???

+ The Spinozan hypothesis asserts that rejecting an idea requires the extra step of unaccepting...or "tagging" a mental representation as false
	+ If this is so, then people should initially accept both true and false ideas upon comprehension but, when the processing of the idea is interrupted, should not be able to go on and unaccept (or tag) false ideas
	+ So, interruption should cause Spinozan systems to mistake false ideas for true ones, but not vice versa
+ The Cartesian model, on the other hand, considers acceptance and rejection to be alternative consequences of a single assessment process
  + And therefore predicts no asymmetry between true and false ideas as a result of interruption

---
#  When systems break down 

&lt;img src="18_beliefs_files/figure-html/gph1-1.png" style="display: block; margin: auto;" /&gt;
.right[*Gilbert et al. 1990*]

???

+ Gilbert and colleagues decided to test these competing hypotheses
+ In the context of a language-learning experiment, subjects were presented with novel propositions on a computer screen...(A Tyrwin is a doctor)
+ During trials, subjects were later informed that the preceding proposition was either true or false
+ In some of these trials, subjects' processing of the proposition was interrupted by having them quickly perform an unrelated task...(pushing a button in response to a tone)
+ Finally, subjects were presented with the original propositions (in question form) and were asked to determine whether they were true or false
+ **Note: The critical results are the false-false and false-true conditions**
  + These are the conditions the 2 models made specific predictions about
  + Here participants are less likely to identify propositions as false (false-false) and more likely to identify false propositions as true (false-true) when interrupted...all consistent with the Spinozan model

---
#  “Bob Talbert Not Linked to Mafia” 

+ In a series of studies, Wegner and colleagues (1981) presented subjects with information about a person and form an impression of them
	+ Subjects who read negations (“Bob Talbert did not eat his cat”) developed more negative impressions than those who read neutral assertions (“Bob Talbert celebrated his birthday”)
	+ We seem to believe (to some extent) negated information

???

+ Now, there is something unusual about linguistic negations
+ Take the title of this slide that Bob Talbert is not linked to the mafia
+ In order to comprehend the negation, a listener must first comprehend the core assertion of the proposition...namely that Bob Talbert **is** linked to the mafia
  + Then, they have to reject it
+ Wegner and colleagues (1981) presented participants with information about a person and asked them to form an impression
  + Those who saw negations about the person (“Bob Talbert did not eat his cat”) had more negative impressions than those who read neutral assertions (“Bob Talbert celebrated his birthday”)
  + So, it seems that we tend to believe...at least to some extent...negated information

---
#  Magical beliefs (Rozin) 

+ Laws of sympathetic magic
	+ Law of contagion
	+ Law of similarity
	
&lt;br&gt;
.center[&lt;img src="assets/img/image6.png" width=600&gt;]

???

+ Now that we’ve covered how mental systems believe...that is, how beliefs are formed...let’s discuss some of our more magical beliefs
+ Paul Rozin, in a series of groundbreaking studies in the 80s and 90s documented a consistent pattern of beliefs, thoughts, and practices observed in the average person
+ He called these beliefs the laws of sympathetic magic...and he distinguished among 2 types

---
#  Law of contagion 

+ “Once in contact, always in contact”
+ Contact between objects (or people) may result in a transfer of an essence
+ In forward contagion, an essence from a source reaches the recipient by direct contact, or through a medium (an article of clothing)
+ In backward contagion, the recipient often actively seeks the contagious item (the recipient operates on the item)

.center[&lt;img src="assets/img/image7.png" width=450&gt;]

???

+ The law of contagion holds that things that have once been in contact with each other may influence or change each other for a period of time that extends well past the termination of contact
+ The transfer of properties...which can be physical or psychological attributes or intentions...seems to be accomplished through transfer of an “essence”
+ In forward contagion, an essence from a source reaches the recipient by direct contact, or through a medium (an article of clothing)
+ In backward contagion, the recipient often actively seeks the contagious item (the recipient operates on the item)

---
#  Law of contagion 

.pull-left[&lt;img src="assets/img/image9.png"&gt;]
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
.pull-right[&lt;img src="assets/img/image8.png"&gt;]

???

+ In forward contagion, think of a person who refuses to wear a piece of clothing previously worn by their hated enemy, even though the piece of clothing was washed and cleaned
  + an essence from a source reaches the recipient through a medium (the clothing)
  + People in studies are very reluctant to wear a sweater formerly owned by Adolf Hitler, even if thoroughly washed
  + It’s as if they believe it is still contaminated by his evil essence
+ In backward contagion, consider a person who acquires a lock of hair to cast a love spell or to be used in the construction of a voodoo doll
  + the recipient often actively seeks the contagious item

---
#  Law of contagion 

+ Initially, in the 80s people showed extreme overreactions to non-risky contact with persons with AIDS
+ There were disputes in California, for example, over whether past ownership of a home by a person with AIDS must be revealed to a prospective buyer


&lt;br&gt;
.center[&lt;img src="assets/img/image10.png" width=500&gt;]

???

+ Rozin and colleagues examined the idea of contagion in the context of peoples’ fears about transmission of AIDS, which tended to be quite extreme
+ Initially, in the 80s people showed extreme overreactions to non-risky contact with persons with AIDS
+ There were disputes in California, for example, over whether past ownership of a home by a person with AIDS must be revealed to a prospective buyer
+ Example of forward contagion

---
#  Law of contagion 

+ Participants were reluctant to wear a sweater previously worn by someone with AIDS, even after washed


+ They were even reluctant to wear a sweater merely owned by a person with AIDS


+ Similar result for a metal fork used (or merely owned)


+ “Once in contact, always in contact”

???

+ The results of Rozin and colleagues studies revealed how extreme people's fears about the transmission of AIDS were
+ For AIDS to be transmitted, a person needs to come into direct contact with body fluids from a person with HIV...and the HIV in these fluids needs to get into the bloodstream of an HIV negative person
+ But, in Rozin's studies...Participants were reluctant to wear a sweater previously worn by someone with AIDS, even after washed
  + They were even reluctant to wear a sweater merely owned by a person with AIDS
  + Similar result for a metal fork used (or merely owned)...HIV cannot be spread through saliva
+ A core feature of the law of contagion is that once joined through direct or indirect physical contact, a transfer of essence occurs that is often permanent
  + “Once in contact, always in contact”

---
#  Law of contagion 

+ Minimal contact is usually sufficient to transmit near maximal amounts of essence
	+ In the AIDS study, duration of contact had little effect on reluctance to wear the sweater
	+ Thus, a core characteristic of contagion is dose insensitivity

&lt;br&gt;
&lt;br&gt;
.center[&lt;img src="assets/img/image11.png" width=500&gt;]

???

+ This study also illustrates another characteristic of contagion...namely dose insensitivity
+ This is the idea that minimal contact is usually sufficient to transmit near maximal amounts of essence
  + In the AIDS study, duration of contact had little effect on reluctance to wear the sweater
+ As another example, it would’t matter if a cockroach quickly walked over a fork or spent the evening sleeping on it...people are unwilling to use the fork to eat their dinner (again, even if thoroughly washed)

---
#  Law of contagion 

+ In contagion, all of the properties of the source pervade the entire source and are contained in its essence (magical contagion is holographic)


+ Hitler’s fingernails are as contaminating as his brains


+ In the AIDS study, 43% of participants reported that there was no place at all on the body of person with AIDS that they would feel comfortable touching

???

+ In contagion, all of the properties of the source contaminant pervade the entire source...and these properties are contained in the source's essence
+ Magical contagion is holographic in the sense of the whole is equivalent to the parts
  + That is, a single part of the source/contaminant is equal to the entire source
+ Contact with *any part* of the source transfers essence...something as small as fingernail clippings or a lock of hair is enough to trigger contagion
  + e.g., Hitler's fingernails are just as contaminating as his brains
  + Nearly half of the participants in the AIDS study reported they wouldn't fell comfortable touching any part of a person with AIDS...not even to shake their hand

---
#  Law of similarity 

+ According to this law, image equals object
+ People are reluctant to eat a well-crafted mound of chocolate fudge shaped to appear like “doggie-doo”

&lt;br&gt;
.pull-left[&lt;img src="assets/img/image13.png"&gt;]
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
.pull-right[&lt;img src="assets/img/image12.png"&gt;]

???

+ Another part of our magical beliefs involve the law of similarity...which basically says that an image of a contaminant amounts to the actual contaminant 
+ Said another way, things that show a superficial resemblance...which in most cases amounts to looking alike...also have other, deeper resemblances
  + Here, appearance equals reality
  + Even if the source isn't actually contaminated...if it looks similar to something that is, that's enough to cause the effect
+ e.g., In one study, people were significantly less accurate when throwing darts at the face of a revered figure (JFK) or their loved ones, than at a blank paper or picture of a disliked person (Hitler)
+ The idea is that at some level people thought that damaging the photo would do harm to the person pictured
+ People were reluctant to consume sugar taken from a bottle labeled “Sodium Cyanide, Poison,” even though they themselves had arbitrarily labeled the bottle
+ People don't want to eat chocolate fudge if it's shaped like dog poop
+ The appearance of these things amounts to a real contaminant for people

---
#  (Pseudo-Profound) Bullshit 

+ The philosopher Frankfurt (2005) defines bullshit as something that is designed to impress but that was constructed absent direct concern for the truth


+ “Hidden meaning transforms unparalleled abstract beauty”

???

+ Speaking of poop...people also form beliefs around bullshit...which Frankfurt (2005) defines as something that is designed to impress but that was constructed without direct concern for the truth
+ This distinguishes bullshit from lying, which involves a deliberate manipulation and subversion of truth
+ e.g., take the statement...“Hidden meaning transforms unparalleled abstract beauty”
+ Although this statement may seem to convey some sort of potentially profound meaning, it is merely a collection of buzzwords put together randomly in a sentence that has syntactic structure...it doesn't *really mean* anything
+ This is an example of what is called pseudo-profound bullshit
+ It attempts to impress rather than inform...it supposedly conveys something profound

---
#  Bullshit receptivity 

+ People have a bias toward accepting something as true (Gilbert, 1991)
+ People may confuse vagueness for profundity
+ Less likely to if the source is suspect or a known bullshitter

???

+ So, what might cause someone to erroneously rate pseudo-profound bullshit as profound?
+ Well, people have a general bias toward accepting something as true (Gilbert, 1991)
	+ So, people may first accept bullshit as true (or meaningful) and either retain a sense that it is meaningful or invoke reasoning to test its truth/meaningfulness
+ But, people may confuse vagueness for profundity
	+ The person is simply unaware that they should consider the statement further
+ But people may be less likely to believe the statement if the source is suspect or a known bullshitter

---
#  What predicts bullshit receptivity? 

+ Analytic thinking should facilitate detection of bullshit (CRT performance)


+ Possessing epistemically suspect (magical beliefs) beliefs should inhibit detection

???

+ System 2 processing (or analytic thinking) should help people detect bullshit
+ Recall the Cognitive Reflection Test we discussed in the systems of reasoning lecture at the beginning of the semester
  + "If a bat and ball cost $1.10, and the bat costs $1.00 more than the ball. How much does the ball cost?"
+ Studies have shown that people who score higher on this test are more likely to detect bullshit
+ Other studies have shown that those who hold magical beliefs...beliefs that are epistemically suspect...are less likely to detect bullshit
  + For example, the belief in angels (and the corresponding belief that they can move through walls) conflicts with the common folk-mechanical belief that things cannot pass through solid objects
  + Often accompanied by an unwillingness to critically reflect on such beliefs...here again, we see System 2 come into play

---
#  Rejecting science 

+ *Smoking increases the risk of lung cancer*


+ *Humans evolved by natural selection*


+ *The earth is undergoing climate change because of human activity*


+ *Modern vaccines do not cause autism*


+ *The earth is round*

???

+ The flip side of believing bullshit is not believing something that is actually true
+ There seems to be recent trends in the rejection of scientifically supported conclusions based on empirical research
+ Consider these statements...(read)
+ Scientists overwhelmingly agree that these statements are true, yet many lay people have regarded them with skepticism (and sometimes outright rejection)

---
#  Rejecting science

&lt;br&gt;
&lt;br&gt;
.center[
&lt;iframe width="560" height="315" src="https://www.youtube.com/embed/qEDM-O7RZSQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen&gt;&lt;/iframe&gt;
]

???

+ For example, there are now things like the Flat Earth Society
+ After clip:
  + This clip comes from the Netflix flat earther documentary, and I recommend watching it if only to witness them debunk themselves with an experiment

---
#  Rejecting science 

+ The consequences of misinformed rejections of science can be profound, strongly influencing public policy and causing widespread rejection of potentially lifesaving items (e.g. vaccines to eradicate deadly diseases)
  + e.g., There can be devastating potential consequences for public health

&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
.right[*Goertzel, 2010*]

???

+ The consequences of misinformed rejections of science can be profound
+ They can strongly influence public policy and cause widespread rejection of potentially lifesaving items (e.g. vaccines to eradicate deadly diseases)
+ And this can lead to devastating potential consequences for public health
+ For example...

---
#  Case study 

+ Water agencies on the west coast of the US asked engineers in 1997 to combat water shortages by purifying sewage water and pumping it back into the drinking water supply
	+ Safe and efficient plan from a scientific perspective


+ Rejected by the public
  + As in the classic magical contagion effect studies, people showed an automatic, visceral negative response to a once-contaminated item

???

+ In 1997, west coast water agencies were trying to combat water shortages by purifying sewage water that could be fed back into the drinking water supply
+ Engineers determined this was a safe and efficient plan from a scientific perspective
+ But this idea was outright rejected by the broader public
+ Just like the magical contagion effects we discussed earlier, people had a automatic, negative reaction to this purified water bc it was once contaminated with human waste

---
#  Case study 

+ Next step: Determine what actions would help ease people’s aversion (Haddad et al. 2009)


+ People were more willing to accept the purified water if:
	+ It was first explained to them that all drinking water has been contaminated at some point in the past
	+ Some of the water was first allowed to mix with groundwater before being pumped back into the drinking water supply


+ In OC, California, this additional step eventually led to broad acceptance of the water purification scheme

???

+ So the water agencies' next step was to figure out what they could do to ease the public's aversion to this plan
+ Haddad and colleagues found that people were more willing to accept the purified water if:
  + It was first explained to them that all drinking water has been contaminated at some point in the past
	+ Some of the water was first allowed to mix with groundwater before being pumped back into the drinking water supply
+ In Orange County, California, this additional explanation step eventually led to the public's acceptance of using the purified water

---
#  Climate change 

+ Lewandowsky et al. (2013) asked whether people who reject climate change tend to believe in:
  1. Unregulated free markets
	1. Conspiracy theories in general


+ Those who believe in having a relatively unregulated free market may tend be highly suspicious of scientific findings that have implications for government-imposed regulations


+ A tendency to endorse conspiracy theories could predict rejection of climate change

???

+ Another important area were misinformed rejections of science are causing devestating effects is climate change
+ In one study, Lewandowsky and colleagues (2013) asked whether people who reject climate change also tended to believe in:
  1. Unregulated free markets
	1. Conspiracy theories in general
+ Historical accounts suggest that those who believe in having a relatively unregulated free market may tend be highly suspicious of scientific findings that have implications for government-imposed regulations
+ But they also argued that a tendency to endorse conspiracy theories could predict rejection of climate change

---
#  Lewandowsky et al. found that: 

+ Believing in a free-market ideology was an extremely strong predictor of rejecting climate science and rejecting the smoking–lung cancer relationship, but was not a predictor of rejection of the HIV–AIDS causal relationship


+ The tendency to believe conspiracy theories predicted rejection of **all** the science findings they tested

???

+ The researchers found that believing in a free-market ideology was an extremely strong predictor of rejecting climate science
+ The also found that free-market ideology was a strong predictor of rejecting the smoking–lung cancer relationship...but it was not a predictor of rejection of the HIV–AIDS causal relationship
  + *Note: the fact that free-market ideology was a strong predictor of rejecting climate science and smoking-lung cancer relationship may have regulatory implications for industry*
+ The researchers also found a tendency to believe conspiracy theories predicted rejection of all the science findings they tested
  + *Note: This rejection may be based on a general mistrust of authority figures (e.g. scientists), rather than the research itself*

---
#  Conspiracy theories 

+ Attempts to explain significant occurrences in terms of plotting among powerful people or organizations who attempt to hide their role
.right[*Douglas &amp; Sutton, 2008; Sunstein &amp; Vermeule, 2009*]


+ For example, that the tsunami off the coast of Japan on March 11, 2011 resulted directly from a deliberate attack on Japan by the U.S. military

???

+ Research also shows that people who tend to use conspiracy theories also tend to explain significant outcomes in terms of powerful people/organizations and that these powerful elites try to hide their role in the plans
+ For example, some conspiracy theorists say the tsunami that occurred off the coast of Japan on March 11, 2011 was the direct result from a deliberate attack on Japan by the U.S. military

---
#  Conspiracy theories 

+ Conspiracy theories can be false, partially true, or completely true
+ Researchers often leave out the question of truth versus falsity
  + Focus on understanding the patterns of reasoning
+ Some conspiracy theories are harmless
+ Others are incredibly harmful, and may have helped to fuel wars and genocides (Goertzel, 2010)

&lt;br&gt;
.center[
.pull-left[&lt;img src="assets/img/image14.jpg" height=250&gt;]
.pull-right[&lt;img src="assets/img/image15.jpg" height=250&gt;]
]

???

+ Conspiracy theories can be false, partially true, or completely true
+ Researchers often leave out the question of truth versus falsity...and focus solely on understanding the patterns of reasoning that create and sustain conspiracy theories
+ Some conspiracy theories are harmless
  + Santa Claus is real
+ Others are incredibly harmful, and may have helped to fuel wars and genocides (Goertzel, 2010)
  + Global warming is a hoax

---
#  Belief perseverance 

+ Once people accept a conspiracy theory, they become resistant to newly presented evidence to the contrary
+ Contradictory evidence can be explained away as being part of the ongoing plot/cover-up
+ Wood et al.’s (2002) findings suggest that believers of conspiracy theories are most strongly influenced by their central belief in a secret cover-up among powerful persons, and are less concerned about the compatibility between the details

&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
.right[*Lieberman &amp; Arndt, 2000*]

???

+ Once people accept a conspiracy theory, they can be incredibly resistant to accepting any newly presented evidence to the contrary
+ Any contradictory evidence can be explained away as being itself a part of the ongoing plot/cover-up
+ Wood et al.’s (2002) findings suggest that believers of conspiracy theories are most strongly influenced by their central belief in a secret cover-up among powerful persons, and are less concerned about the compatibility between the details
    </textarea>
<style data-target="print-only">@media screen {.remark-slide-container{display:block;}.remark-slide-scaler{box-shadow:none;}}</style>
<script src="https://remarkjs.com/downloads/remark-latest.min.js"></script>
<script>var slideshow = remark.create({
"highlightStyle": "github",
"highlightLines": true,
"countIncrementalSlides": false
});
if (window.HTMLWidgets) slideshow.on('afterShowSlide', function (slide) {
  window.dispatchEvent(new Event('resize'));
});
(function(d) {
  var s = d.createElement("style"), r = d.querySelector(".remark-slide-scaler");
  if (!r) return;
  s.type = "text/css"; s.innerHTML = "@page {size: " + r.style.width + " " + r.style.height +"; }";
  d.head.appendChild(s);
})(document);

(function(d) {
  var el = d.getElementsByClassName("remark-slides-area");
  if (!el) return;
  var slide, slides = slideshow.getSlides(), els = el[0].children;
  for (var i = 1; i < slides.length; i++) {
    slide = slides[i];
    if (slide.properties.continued === "true" || slide.properties.count === "false") {
      els[i - 1].className += ' has-continuation';
    }
  }
  var s = d.createElement("style");
  s.type = "text/css"; s.innerHTML = "@media print { .has-continuation { display: none; } }";
  d.head.appendChild(s);
})(document);
// delete the temporary CSS (for displaying all slides initially) when the user
// starts to view slides
(function() {
  var deleted = false;
  slideshow.on('beforeShowSlide', function(slide) {
    if (deleted) return;
    var sheets = document.styleSheets, node;
    for (var i = 0; i < sheets.length; i++) {
      node = sheets[i].ownerNode;
      if (node.dataset["target"] !== "print-only") continue;
      node.parentNode.removeChild(node);
    }
    deleted = true;
  });
})();
(function() {
  "use strict"
  // Replace <script> tags in slides area to make them executable
  var scripts = document.querySelectorAll(
    '.remark-slides-area .remark-slide-container script'
  );
  if (!scripts.length) return;
  for (var i = 0; i < scripts.length; i++) {
    var s = document.createElement('script');
    var code = document.createTextNode(scripts[i].textContent);
    s.appendChild(code);
    var scriptAttrs = scripts[i].attributes;
    for (var j = 0; j < scriptAttrs.length; j++) {
      s.setAttribute(scriptAttrs[j].name, scriptAttrs[j].value);
    }
    scripts[i].parentElement.replaceChild(s, scripts[i]);
  }
})();
(function() {
  var links = document.getElementsByTagName('a');
  for (var i = 0; i < links.length; i++) {
    if (/^(https?:)?\/\//.test(links[i].getAttribute('href'))) {
      links[i].target = '_blank';
    }
  }
})();
// adds .remark-code-has-line-highlighted class to <pre> parent elements
// of code chunks containing highlighted lines with class .remark-code-line-highlighted
(function(d) {
  const hlines = d.querySelectorAll('.remark-code-line-highlighted');
  const preParents = [];
  const findPreParent = function(line, p = 0) {
    if (p > 1) return null; // traverse up no further than grandparent
    const el = line.parentElement;
    return el.tagName === "PRE" ? el : findPreParent(el, ++p);
  };

  for (let line of hlines) {
    let pre = findPreParent(line);
    if (pre && !preParents.includes(pre)) preParents.push(pre);
  }
  preParents.forEach(p => p.classList.add("remark-code-has-line-highlighted"));
})(document);</script>

<script>
slideshow._releaseMath = function(el) {
  var i, text, code, codes = el.getElementsByTagName('code');
  for (i = 0; i < codes.length;) {
    code = codes[i];
    if (code.parentNode.tagName !== 'PRE' && code.childElementCount === 0) {
      text = code.textContent;
      if (/^\\\((.|\s)+\\\)$/.test(text) || /^\\\[(.|\s)+\\\]$/.test(text) ||
          /^\$\$(.|\s)+\$\$$/.test(text) ||
          /^\\begin\{([^}]+)\}(.|\s)+\\end\{[^}]+\}$/.test(text)) {
        code.outerHTML = code.innerHTML;  // remove <code></code>
        continue;
      }
    }
    i++;
  }
};
slideshow._releaseMath(document);
</script>
<!-- dynamically load mathjax for compatibility with self-contained -->
<script>
(function () {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src  = 'https://mathjax.rstudio.com/latest/MathJax.js?config=TeX-MML-AM_CHTML';
  if (location.protocol !== 'file:' && /^https?:/.test(script.src))
    script.src  = script.src.replace(/^https?:/, '');
  document.getElementsByTagName('head')[0].appendChild(script);
})();
</script>
  </body>
</html>
