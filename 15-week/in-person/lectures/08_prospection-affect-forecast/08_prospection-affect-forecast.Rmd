---
title: "Prospection & Affective Forecasting"
author: "PSYC/PHIL 279, Winget"
output:
  xaringan::moon_reader:
    css: ["default", "default-fonts", "my-style.css", "metropolis-fonts"]
    lib_dir: libs
    nature:
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: false
---
# Imagine this...

+ The telephone rings, and it’s the admissions department. There has been a terrible error, they say, and sadly inform you that you were mistakenly granted admission to the graduate program.

+ How would you feel, and how long would you feel this way?

???

+ Imagine this situation...
+ **Pause for a little bit**
+ This is a variant of a question Gilbert and colleagues used in a classic study
+ First one is probably a bit more relatable, but both questions prompts the same mental processes
  + some/most of you came up with positive experiences
  + others came up with negative experiences

--

#  From Gilbert et al. (1998) 

+ “Imagine one morning your telephone rings and you find yourself speaking to the king of Sweden, who informs you in surprisingly good English that you have been selected as this year’s recipient of the Nobel prize.”

+ How would you feel, and how long would you feel that way?

---
#  Prospection 

+ In each case, in order to make your prediction, you had to simulate the future
+ Such simulations are sometimes called prospection

<br>
.center[
<img src="assets/img/image6.png" height=250>
<img src="assets/img/image7.png" height=250>
]

???

+ 

---
#  Prospection: Imagining the future 

+ Our brain combines incoming information with stored information to build mental representations or internal models of the world
  + Memory = mental representation of the past
  + Perception = mental representation of the present
  + Simulation = mental representation of the future

<br>
.center[
<img src="assets/img/image25.png" height=160>
<img src="assets/img/image26.jpg" height=160>
<img src="assets/img/image27.jpg" height=160>
]

???

+ Whenever we are engaged in prospection, our brains are integrating information
+ In these situations, we are taking in new information but also trying to combine it with the information we already have...our prior knowledge
+ The goal here is to build what are called mental representations of the world
+ Mental representations are basically internal models of how we think the world works
+ There are different ways we can build mental representations
  + List memory, perception, and simulation
+ These are all examples of what Gilbert and Wilson call prospection

---
#  Prospection: Imagining the future 

+ One way to predict the future hedonic consequences of events is to simulate it

<br>
.center[<img src="assets/img/image20.png" width=500>]

???

+ One way to predict the future consequences of events is to simulate it
  + We try to build a representation of what the future will look like
+ We close our eyes, imagine the event, and pre-experience their hedonic consequences (Gilbert & Wilson, 2007)
  + Hedonic consequences = pleasurable consequences
+ We use these immediate hedonic reactions to simulations as guides to what we would feel if event actually happens

---
# Imagine

+ How would you feel if...
  + you ate a liver popsicle?
  + you won the lottery?
  + you lost the use of your legs?

.center[<img src="assets/img/image5.png" height=150>
.pull-right[<img src="assets/img/image13.png" height=350>]
]
.center[<img src="assets/img/image10.png" height=150>]

???

+ For example...
+ In all of these cases, we imagine the event, and try to experience how pleasurable/desirable participating in that even would be
+ We then use our reaction to guide how we think we would feel if the event actually happened

---
#  Decisions, Decisions, Decisions 

+ People’s actions are based, to a large extent, on their implicit and explicit predictions of the emotional consequences of future events (Gilbert et al., 1998; Kahneman & Snell, 1990; Mellers et al., 2001)

<br>
.center[<img src="assets/img/image28.jpg" width=650>]

???

+ These reactions to the hedonic consequences of the event tend to be emotionally loaded
+ We're basically using our predictions of the emotional consequences of future events to decide how we should act
+ We decide to marry or divorce, become an academic or lawyer, or go to dinner at one restaurant or another because we predict that one or the other option will bring us greater emotional rewards
+ Sometimes these emotions are explicit and we're completely aware of them
+ Other times, they may be more implicit and out of conscious awareness

---
#  Dimensions of affective forecasting 

+ Valence
  + Will it make us feel good or bad?
+ Intensity
  + How good or bad will it make us feel?
+ Duration
  + How long will we feel this way?

<br>
<br>
.center[
<img src="assets/img/image31.jpg" height=125>
<img src="assets/img/image30.png" height=125>
<img src="assets/img/image29.png" height=125>
]

???

+ This process of predicting how we would feel in a future situation is called affective forecasting
+ There are 3 dimensions of affective forecasting
  + List and describe dimensions...

---
#  Emotion and memory 

+ Emotions are not stored in memory (Kahneman, 1999; Loewenstein, 1996; Robinson & Clore, 2002)
+ For this reason, we must simulate both past and future emotional reactions

<br>
.center[<img src="assets/img/image32.jpeg" height=300>]

???

+ Now, although emotions are used for mental simulations, they are not stored in memory
  + We simply cannot store an emotion in memory
  + We can recall them, we can describe how we felt in a memory, but we cannot re-experience the emotion from memory alone
+ We can remember the fact that riding a roller coaster was thrilling, but we cannot retrieve that actual experience
+ Indeed, if we could do so, there would be little point in riding a roller coaster or having sex more than once

---
#  Errors of prospection 

+ We feel certain that we know what would make us happy
+ Research on affective forecasting, however, suggests that when we imagine how a particular circumstance will make us feel, we often make mistakes

<br>
<br>
.right[<img src="assets/img/image33.png" height=300>]

???

+ Prospectiion is a very useful process
  + It allows us to consider situations that haven't happened yet and consider how we might behave without actually being in the situation
  + Probably really helpful for our ancestors to figure out how to trap food and evade dangerous situations without placing themselves in danger
+ And, you'd like it'd be a fairly accurate process...we know what makes us happy or not, so we feel pretty certain we could imagine what would make us happy or not, right?
+ But, research suggests we often make mistakes when doing so

---
class: center top

#  Prospection: Errors in affective forecasting
## Gilbert and Wilson (2007) 

<img src="assets/img/image18.png" width=270>
<img src="assets/img/image1.png" width=270>
<img src="assets/img/image2.png" width=270>

???

+ If one sunny day a man was on his way to his favorite cafe and imagined eating the chocolate cake, he would probably experience a positive premotion and expect to enjoy eating the cake when he arrived
  + Premotion is basically the imagined emotion in prospection
+ If he arrived while the sun was still shining and ate the chocolate cake he had imagined, there is every reason to believe that he would enjoy the experience just as he predicted (figure 1)
+ On the other hand, if he arrived at the café only to find that (a) the chocolate cake was gone and mincemeat pie was being served in its place (figure 2) or (b) the weather had turned cold and nasty (figure 3), then he might not enjoy his experience as much as he expected to
+ The fact is that premotions accurately predict emotions when the content and context of the preview are similar to the content and context of the view, and the reason why errors in emotional prediction occur is that these two criteria often go unmet

---
#  Mispredictions 

+ Valence
  + We are relatively accurate in predicting the identities of our future feelings
+ Intensity and duration
  + We are less accurate in predicting the intensity and/or duration of future emotional events

<br>
.center[<img src="assets/img/image34.png" height=300>]

???

+ So these mispredictions in affective forecasting come from the 2 of the 3 dimensions of affective forecasting
+ We're usually pretty accurate when it comes to valence
  + We tend to know what will make us feel good or bad
  + So we're not that bad at predicting what will make us feel good/bad
+ The problems tend to come from the intensity/duration dimensions
  + We're not nearly as good as predicting *how* good/bad something will make us feel...or how *long* we'll feel that way
  + Such inaccuracy in intensity and duration is more often an overestimation than underestimation of intensity/duration

---
#  Impact bias 

+ Tendency to overestimate the intensity and duration of emotional reactions to future events

<br>
<br>
.center[
<img src="assets/img/image30.png" height=200>
<img src="assets/img/image29.png" height=200>
]

???

+ This inaccuracy in intensity and duration actually has its own name
+ The impact bias is the tendency to overestimate the intensity and duration of emotional reactions to future events
+ For example...
  + College professors overestimate how unhappy they would be 5 years after being denied tenure
  + College students overestimate how happy they will be the week after their university football team wins a big game
  + People overestimate the duration of sadness following the breakup of romantic relationship

---
#  Why? Focalism 

+ Focalism (Wilson & Gilbert, 2005, p. 132)
  + “The tendency to overestimate how much we will think about the event in the future and to underestimate the extent to which other events will influence our thoughts and feelings”

<br>
.center[<img src="assets/img/image35.jpg" width=450>]

???

+ So the question is why does this happen
+ A lot of the research suggests the impact bias is due to focalism
  + Tendency to *overestimate* how much we will think about the event in the future and to *underestimate* the extent to which other events will influence our thoughts and feelings”
+ When we imagine winning the lottery, for example, we focus on the money and what we can buy with it, and neglect other events that go along with winning (e.g., greedy family members begging for money, etc.)

---
#  Dunn, Wilson, & Gilbert (2003) 

+ Students at university (Harvard) are randomly assigned to spend next three years in 1 of 12 dorms

+ Each student also enters the lottery along with roommates they've chosen as well as a group of 15 other friends, called blockmates

???

+ In one real-world study, researchers examined whether participants overestimated how happy they would be living in a desirable house as well as how unhappy they would be living in an undesirable house
+ This was the first attempt demonstrate the impact bias using a multi-year longitudinal design with random assignment to a major life outcome
  + Really tested this outside of lab
+ Students were then randomly assigned to 1 of the 12 dorms
+ Each of the students also entered a lottery system that determined who they would live with
  + Students get to pick some friends they'd like to live with...called blockmates
  + However, they were also paired with other roommates they did not select
  + So mixture of blockmates and roommates

---
#  Dunn, Wilson, & Gilbert (2003) 

+ Dorms differ in their quality
+ Some are seen as desirable (e.g., good location) or undesirable (e.g., small rooms)

<br>
.pull-left[<img src="assets/img/image24.png" height=275>]
.pull-right[<img src="assets/img/image23.png" height=275>]

???

+ The physical quality of the dorms differed from one another
+ Some had more perks than others

---
#  Dunn, Wilson, & Gilbert (2003) 

+ But social factors are the same across houses
  + Relationships with roommates
  + Relationships with blockmates

<br>
.center[<img src="assets/img/image4.png" width=600>]

???

+ But the social aspects of house life (e.g.,sense of community) vary less across the houses
+ Indeed,because one enters the housing lottery with a group of roommates and blockmates, the quality of one’s relationships with these close others should be roughly equivalent regardless of house assignment
+ Social relationship are a critical determinant of happiness

---
#  Houses and happiness 

+ People estimated how happy they would be if they were assigned to desirable or undesirable houses

+ Then reported their happiness 1 and 2 years later (when they were 2nd and 3rd year students)

<br>
<br>
<br>
.center[<img src="assets/img/image36.png" width=400>]

???

+ In the spring of their 1st year (Time 1), shortly before they learned which house they would live in for the subsequent 3 years, college students predicted how happy they would be 1 year later 
+ 1 and 2 years later (sophomores and juniors), they asked participants to report their actual happiness
+ Researchers predicted that people would overestimate how happy they would be if they were assigned a desirable house and underestimate how happy they would be if they were assigned an undesirable house

---
class: inverse
background-image: url('assets/img/image22.png')
background-size: contain

???

+ And that's basically what they found...

---
#  Why? Focalism 

+ People focused on wrong features
+ They focused on physical features that varied across houses, but neglected the quality of social life across houses (which was quite similar)

<br>
<br>
.center[<img src="assets/img/image35.jpg" width=450>]

???

+ Focalism again...people focused on the wrong factors
+ People tended to base their forecasts more on the physical features that varied greatly between houses and less on social features that varied relatively little...even though social features would be more related to actual happiness than physical features
+ We focus on the long walk to campus or the boring colors in the dorm room, but ignore everyday events that contribute to our daily moods, etc

---
#  Does living in California make people happy? 

+ No, but people think living in California will make them happier

<br>
<br>
<br>
.pull-left[<img src="assets/img/image14.png" height=250>]
.pull-right[<img src="assets/img/image11.png" height=250>]

???

+ This same process is at work when we think about moving to warm, tropical climates during Chicago winters
+ We might ask, why don't I live in Cali...I'd be much happier
+ Turns out, living in Cali won't actually make you happier

---
#  Schkade & Kahneman (1998) 

+ Ps from Midwestern or West Coast schools

+ Asked to rate importance for self and other (i.e., people at opposite schools) of various aspects of life to well-being

+ People across regions thought the same things were import to themselves and others

???

+ Researchers recruited participants from with Midwestern school (UM, OSU) or West Coast school (UCLA, UC Irvine)
+ Once in the study, Ps were aske to rate the importance of various aspects of life to well-being
  + Life satisfaction overall, job prospects, academics, finances, climate, summer/winter weather, etc.
  + Asked to rate self and other (people at other schools)
+ Results showed people across regions thought the same things were import to themselves and others

---
class: inverse
background-image: url('assets/img/image9.png')
background-size: contain

???

+ Explain graph...
+ People in California and Midwest reported similar satisfaction with their life overall
+ In other words, living in CA doesn’t make one happier overall

---
#  Focusing illusion 

+ But, both Californians and Midwesterners predicted that similar others in CA would be happier than those from MW
+ Why?
  + Both groups had an incorrect focus on climate, an easily observable difference between the two regions
  + However, climate does not loom large in people’s overall life satisfaction (i.e., it is not a major contributing factor)

<br>
<br>
.center[<img src="assets/img/image37.png">]

???

+ Even though these happiness ratings didn't actually differ between CA and MW, all participants *thought* it would
+ Both Californians and Midwesterners predicted that similar others in CA would be happier than those from MW
+ Why did they think this when they objectively weren't any happier than one another?
  + Well, both groups had an incorrect focus on climate
  + Climate is an obvious difference between the two regions, so that's why people focused on
  + But, climate isn't at the forefront of our lives when we consider our life satisfaction (i.e., it is not a major contributing factor)
  + We care more about our job, financial security, safety, and social relationships
+ Who can tell me the name of this process...where we tend to focus on the event and ignore other factors when imagining the future?
  + Hint: focus/focusing 

---
#  Why? Psychological immune system 

+ Psychological immune system
  + We often fail to recognize just how easily we make sense of novel or unexpected events once they happen
  + This sense-making causes emotional adaptation thereby reducing intensity and duration of emotional events
  + This is largely an automatic and unconscious process, which is why we don’t take into account when making affective forecasts
  
.center[<img src="assets/img/image38.jpg" height=300>]

???

+ So why do we have this persistent bias to focus on certain features of an event and discount others?
+ Well, it's actually a part of something called the psychological immune system
+ Much like the physiological immune system that fights threats to physical health, people have a psychological immune system that fights threats to emotional well-being
+ According to this theory, the same way there are people who are practically immune to viruses and rarely get sick, there are also those who can face the worst tragedies with greater fortitude, while others fall apart, get sad or stressed out in front of the smaller problems
+ We all have a psych immune system
+ So, when we enter into an uncertain situation, the psychological immune system is in charge of building a safety net to protect us from the effects of chronic stress and gives us the strength to endure the most terrible events
+ While the biological immune system keeps us alive protecting us from diseases, the psychological immune system mitigates the impact of emotional shock and allows us to move forward
+ And because the emotional shock is dampened, the intensity and duration of emotion events is also dampened
+ For the most part, this all occurs in System 1 processing, which is why we don't consider it when making affective forecasts

---
#  The pleasures of uncertainty 

+ Sense making reduces the displeasure we experience from negative events, *but it also reduces the pleasure we derive from positive events*
+ If so, we should be able to prolong people’s pleasure by inhibiting their ability to make sense of some event

.center[<img src="assets/img/image39.png" width=550>]

.right[*Wilson et al. (2005)*]

???

+ This is extremely helpful for negative events, especially traumatic ones
+ But this sense making processes doesn't discriminate on valance...so it also reduces the pleasure we derive from positive events
+ So if this psych immune theory is true, then we should be able to prolong pleasure by inhibiting their ability to make sense of some event
  + Because doing so would prevent the immune system from operating

---
class: inverse
background-image: url('assets/img/image15.png')
background-size: contain

???

+ In series of experiments, researchers tested this very idea
+ Experimenters approached people around campus (e.g., a library)
+ Handed them an index card with a dollar coin attached (see slide)
  + certain vs. uncertain manipulation
  + participants in the certain condition (who received the card with questions) would process the information in a relatively mindless fashion and be less likely to spend time questioning who the card was from and why they had received it, because the questions seemed to be adequately answered
  + Participants in the uncertain condition (who received the card without questions) were expected to question whom the card was from and why they had received it, thereby prolonging their positive emotion
+ Five minutes later approached by a second experimenter and asked to complete a survey about their mood

---
#  Pleasures of uncertainty 

<br>
<br>
.center[<img src="assets/img/image8.png" width=700>]

???

+ Walk through table
+ After: Importantly, forecasters did not predict that they would feel happier in the uncertain condition

---
#  Experiment 2 

+ Participants watched an abridged version of the movie *Rudy*

<br>
.center[<img src="assets/img/image17.png" height=400>]

???

+ In the same series of studies, researchers had participants watch a shortened version of the movie Rudy
+ If you haven't seen the movie it's a true story about a a person's dreams attending Notre Dame and playing on the football team
+ He eventually gets on the on team thru grit, but isn't the most athletic, so he ends up on the bench
+ But, at the end of the movie, Rudy's dreams really do come true
+ it's a very uplifting and positive movie

---
#  Experiment 2 

+ After the movie, they read two versions of what happened to Rudy after college

+ Both versions were positive

+ Participants in the certain condition were told which version was true, and those in the uncertain condition were not

+ Measured happiness right after movie, and then again 5 minutes later after filler task

???

+ After watching the film, participants read two versions of what happened to Rudy after he graduated from college, which described positive outcomes that differed in their details
+ Participants in the certain condition were told which version was true of Rudy, but participants in the uncertain were not
+ Measured happiness right after movie, and then again 5 minutes later after filler task

---
#  Experiment 2 

+ Uncertain and certain experiencers were equally happy right after film

+ But, uncertain experiencers were significantly happier at Time 2

+ Forecasters predicted they would be happier right after the movie if certain, and 91% said they would rather know the ending than remain in the dark about the fate of Rudy

???

+ Results showed uncertain and certain experiencers were equally happy right after film
+ But, only uncertain experiencers were significantly happier at Time 2
+ Forecasters predicted they would be happier right after the movie if certain, and 91% said they would rather know the ending than remain in the dark about the fate of Rudy

---
#  Simulation vs. Surrogation 

+ When predicting their future affective reactions, people often rely on simulation

+ However, another strategy, surrogation, may be better (at least in some circumstances)

+ Here one simply asks a similar person what their experience was (e.g., did it make you happy, for how long, etc.)

???

+ When predicting their future affective reactions, people often rely on simulation
+ However, another strategy, surrogation, may be better (at least in some circumstances)
+ Here one simply asks a similar person what their experience was (e.g., did it make you happy, for how long, etc.)

---
#  Simulation vs. Surrogation 

+ Why would this be helpful?
  + It sidesteps the issue of accuracy because it doesn’t rely on mental simulation
+ But wait, aren’t we all unique people, making surrogation a stupid idea?
  + Nope

<br>
.center[
<img src="assets/img/image12.png" height=175>
<img src="assets/img/image19.png" height=175>
<img src="assets/img/image21.png" height=175>
]

???

+ Why would this be helpful?
  + It sidesteps the issue of accuracy because it doesn’t rely on mental simulation
+ But wait, aren’t we all unique people, making surrogation a stupid idea?
  + Nope
  
---
#  Speed dating study 

+ Men and women had brief “getting acquainted” conversations

+ First woman interacted with man, provided affective report (how enjoyable, etc.)

+ Second woman given either:
  + Simulation information (e.g., profile and photo)
  + Surrogation information (e.g., affective report of another participant)
  
+ Second woman asked to predict how much she would enjoy speed date with man

???

+ In one test of the surrogation process, researchers devised a speed dating study
+ Men and women had brief “getting acquainted” conversations
+ First woman interacted with man, provided affective report (how enjoyable, etc.)
+ Second woman given either:
  + Simulation information (e.g., profile and photo)
  + Surrogation information (e.g., affective report of another participant)
+ Second woman asked to predict how much she would enjoy speed date with man

---
#  Speed dating study 

+ Then second woman went on speed date

+ Afterward she provided affective report and reported whether she believed simulation or surrogation information would have helped her prediction

<br>
<br>
<br>
.right[<img src="assets/img/image16.png" width=350>]

???

+ Then second woman went on speed date
+ Afterward she provided affective report and reported whether she believed simulation or surrogation information would have helped her prediction

---
#  Results  

+ Women were more accurate when they used surrogation information than simulation information (error was reduced by 49%)

+ However, 75% of women believed that simulation information would have led to more accurate predictions

???

+ Women were more accurate when they used surrogation information than simulation information (error was reduced by 49%)
+ However, 75% of women believed that simulation information would have led to more accurate predictions
