<!DOCTYPE html>
<html lang="" xml:lang="">
  <head>
    <title>Group Decision Making</title>
    <meta charset="utf-8" />
    <meta name="author" content="PSYC/PHIL 279, Winget" />
    <link href="libs/remark-css-0.0.1/default.css" rel="stylesheet" />
    <link href="libs/remark-css-0.0.1/default-fonts.css" rel="stylesheet" />
    <link href="libs/remark-css-0.0.1/metropolis-fonts.css" rel="stylesheet" />
    <script src="libs/kePrint-0.0.1/kePrint.js"></script>
    <link rel="stylesheet" href="my-style.css" type="text/css" />
  </head>
  <body>
    <textarea id="source">
class: center, middle, inverse, title-slide

# Group Decision Making
### PSYC/PHIL 279, Winget

---

#  Groups 

+ A group consists of 2 or more people who share a common identity and behave in accordance with that identity (Hogg &amp; Vaughan, 2005)
+ Common belief that working in a group will lead to more creative ideas or better decisions
+ This can be true, but often, groups fail to live up to their potential

&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
.right[&lt;img src="assets/img/image1.png"&gt;]

???

+ Until now, we’ve largely considered the decision making process from the perspective of a single decision maker
+ But, many decisions are made as a part of a group
  + Committees, juries, executive boards, etc.
+ Group = 2 or more people who share a common identity and behave in accordance with that identity (Hogg &amp; Vaughan, 2005)
  + e.g., have different political ideologies (liberal v. conservative) and those within an ideology (liberal) follow “liberal” values more than they do “conservative” values
+ Group decision making is very common within the workplace
+ There’s this common idea that interactions within a group will lead to more creative ideas or better decisions
  + This optimism is reflected in the saying two heads are better than one
  + But, there’s a bit of disillusionment about how well groups actually perform
      + Evidence suggests groups can be effective decision-making units but often fail to live up to their potential

---
#  Benefits of group decision making 

+ Compared to individuals working along, groups are better able to…
  + Reach correct solutions to problems (Laughlin, 1980, 2011)
  + Make more accurate hiring decisions (Tindale, 1989)
  + Receive better negotiation outcomes (Mogan &amp; Tindale, 2002)
  + Provide more accurate forecasts (Kerr &amp; Tindale, 2011)
  + Generate more creative ideas (Paulus &amp; Nijstad, 2003)
  + Receive higher scores on academic tests (Michaelson, Watson, &amp; Black, 1989)
  + Recall information more accurately (Hinsz, 1990)

???

+ But, let’s start with the good things about groups, because there are a lot of benefits to making decisions as a group
+ Generally speaking, the fact groups are involved in many decision contexts is a good thing
+ Groups, as compared to individuals working alone, are better able to 
  + reach correct solutions to problems 
  + make more accurate hiring decisions 
  + receive better negotiation outcomes 
  + provide more accurate forecasts 
  + generate more creative ideas
  + receive higher scores on academic tests
  + and recall information more accurately
+ So, decisions made by groups tend to lead to better outcomes on a number of criteria

---
#  Same process, different outcomes 

+ Also evidence that groups aren’t always better than individuals (Janis, 1982; Kerr, MacCoun, &amp; Kramer, 1996; Simmons, Nelson, Galak, &amp; Frederick, 2011)
  + Challenger explosion
+ Traditional research focused on different processes for good and bad group decisions
+ Recent research shows good and bad outcomes stem from the same processes (Kerr &amp; Tindale, 2004; Tindale, Smith, Dykema‐Engblade, &amp; Kluwe, 2012)

???

+ However, there is also evidence that groups aren’t always better than individuals (Janis, 1982; Kerr, MacCoun, &amp; Kramer, 1996; Simmons, Nelson, Galak, &amp; Frederick, 2011)
+ In many cases, groups have made bad decisions with deadly consequences (Nijstad, 2009)
  + Challenger explosion (newer example needed?)
+ Traditionally, group decision making theories either (1) explained the good decisions/actions of groups OR (2) explained the poor decisions/actions of groups
  + They used to argue different processes lead to these different outcomes
  + “good” processes = “good” outcomes
+ But, more recent work shows group decision making processes will often lead to good decisions, but in some situations, can make individual‐level biases worse and lead to less accurate decisions (Kerr &amp; Tindale, 2004; Tindale, Smith, Dykema‐Engblade, &amp; Kluwe, 2012)
  + So, in other words, both good and bad group decisions likely stem from the same underlying process 
  + one process = good and bad outcomes

---
#  How do they do it? 

+ Group decisions can be made in many different ways
+ Two main factors
  1. Degree of interaction/information exchange among group members
  1. How the final judgment/decision is actually achieved

&lt;br&gt;
.center[
&lt;img src="assets/img/image2.png" height=225&gt;
&lt;img src="assets/img/image3.png" height=225&gt;
]

???

+ Collective decisions can be made in many ways using a number of different procedures
+ The 2 main factors that determine how groups tend to make decisions are 
  + The degree of interaction/information exchange taking place among the group members 
  + And, how the final decision or judgment is actually achieved
+ For example, a manager may ask their employees about their availabilities to help determine how best to make the weekly schedule
  + In this case, members of the work group may be unaware of how others are involved, and the final decision may have nothing to do with their preferences 
  + Those of you who have ever been forced to work a shift at a bad time can probably relate to this
+ On the other hand, many government officials are chosen through elections where citizens vote
  + Here, there may be little interaction among the voters, but their collective choice will influence the final outcome
+ A number of common processes underlie virtually all group decision contexts, but we’ll also take a look at where different processes arise and how they influence the types of decisions groups make
+ We’ll start by covering groups whose members do not interact and will move toward those with more interaction and decision control

---
#  Simple aggregation 

+ Occurs without any interaction
+ Members’ preferences are aggregated by one person and aggregate is used as the decision
  + e.g., an election
+ Good at producing accurate judgments (Ariely et al., 2000; Armstrong, 2001; Hastie &amp; Kameda, 2005; Larrick &amp; Soll, 2006)

&lt;br&gt;
&lt;br&gt;
.right[&lt;img src="assets/img/image4.png" height=300&gt;]

???

+ The first type of group decision occurs without individuals ever interacting at all
+ Although group decision making is often thought of as individuals discussing and reaching a consensus on some course of action, many group decisions are not made this way
+ Often, members’ preferences are simply aggregated by one member (or a person outside the group), and the aggregate is used as the group’s position or choice
  + e.g., elections or surveys are often used to guide decision making in larger organizations where face‐to‐face interaction among all the members would be impossible or difficult to arrange
+ Usually such systems are justified based on fairness or equal representation of groups, but most of the research to date has shown that this approach is also good at producing accurate judgments (Ariely et al., 2000; Armstrong, 2001; Hastie &amp; Kameda, 2005; Larrick &amp; Soll, 2006)

---
#  Wisdom of the crowds 

+ Collective judgment is more accurate than judgments made by an individual/expert
  + Larrick &amp; Soll, 2006; Surowiecki, 2004

.center[&lt;img src="assets/img/image5.jpg" width=650&gt;]

???

+ This accuracy, relative to judgments made by individuals, was referred to by Surowiecki as “the wisdom of crowds,” a phenomenon that has now been replicated many times in a number of diverse problem domains (Larrick &amp; Soll, 2006; Surowiecki, 2004)
+ This effect is when the collective opinion of a group of individuals is actually more accurate than that of a single person...even an expert
+ Sir Francis Galton was actually the first to make this discovery
  + He attended a fair where he was intrigued by a weight guessing contest
  + The goal was to guess the weight of an ox when it was butchered and dressed
  + Around 800 people entered the contest and wrote their guesses on tickets
  + The person who guessed closest to the butchered weight of the ox won a prize
  + After the contest Galton took the tickets and ran a statistical analysis on them
  + He discovered that the average guess of all the entrants was remarkably close to the actual weight of the butchered ox
  + In fact it was under by only 1lb for an ox that weighed 1,198 lbs
  + This collective guess was not only better than the actual winner of the contest but also better than the guesses made by cattle experts at the fair

---
#  Your aggregate wisdom

.pull-left[
+ Actual total: 471
+ Our class average: 299
  + Range: 60 - 700
+ Average across 3 classes: 305
  + Range: 60 - 789
]

.pull-right[
&lt;img src="assets/img/image12.png"&gt;
]

???

+ Unfortunately, our little demonstration didn't produce any wisdom of the crowd effects
+ Explain results
+ Does anyone have any ideas why we're not seeing the effects emerge here?
+ Well, wisdom of the crowd effects need very large samples to work
+ This is because they take advantage of something called bracketing

---
#  Simple aggregation 

+ Advantage of this approach is due to “bracketing” (Larrick &amp; Soll, 2006)
  + Assuming *independent* judgments, different members will make estimates above the true score, others below
  + Estimates “bracket” the true score
  + Even when the true score is not bracketed by the estimates, the group average will do no worse than the average individual judge

???

+ Larrick and Soll (2006) have explained the advantage of simple averages over individual judgments using the concept of “bracketing” 
  + Assuming that group member judgments are independent (members are not interacting or able to see each other’s opinions in any way), different members will make somewhat different estimates
  + Some of the estimates above the “true score”, and others will be below it
  + Thus, the estimates “bracket” the true score 
  + (can draw this on board even, or compare to a normal distribution)
+ When judges are independent, it can be mathematically shown that the average of the multiple estimates will always be more accurate than the average individual judge
+ If the true score is well bracketed by the multiple estimates (near the median or average), the aggregate accuracy will be far superior to that of the typical individual judge
+ However, even if the true score is closer to one of the tails of the distribution, the average will still outperform the typical individual, though not to the same degree
+ Larrick and Soll (2006) also show that even when the true score is not bracketed by the estimates, the group (average) will do no worse than the typical individual judge

---
#  Aggregation w/ limited information exchange 

+ Simple aggregation does lead to accurate decisions, but little to no information is exchanged
  + Difficult for members’ with important information to have influence (Kerr &amp; Tindale, 2011)
+ Full group deliberation overcomes this, but there most influential members are not always the most accurate (Littlepage, Robison, &amp; Reddington, 1997)
+ Aggregation with limited exchange strategies blend these approaches

???

+ Although simple aggregation tends to produce fairly accurate decisions, there is little chance for members to share information or defend their positions
+ This is important because it is difficult for members with particular insights or important information to have influence without some type of interchange among group members (Kerr &amp; Tindale, 2011)
+ Obviously, full group deliberation (a topic discussed later) would allow members to share and defend their position
+ However, there’s also some evidence that the most influential members in interacting groups are the most accurate or correct...mostly because influence is driven by status or confidence (Littlepage, Robison, &amp; Reddington, 1997)
+ So, other group decision making approaches try to allow for some information exchange, but try to minimize pressures toward conformity and social influence
  + Called aggregation with limited information exchange

---
#  The Delphi technique

+ Dalkey (1969); Rowe &amp; Wright (1999, 2001)

.center[&lt;img src="assets/img/image6.png" width=750&gt;]

???

+ The Delphi technique is probably the most famous of these procedures (Dalkey, 1969; Rowe &amp; Wright, 1999, 2001)
+ The procedure starts by having a group of (typically) experts make a series of estimates, rankings, idea lists, etc. on some topic of interest to the group or facilitator
+ The facilitator then compiles the list of member responses and summarizes them in a meaningful way (e.g., mean rank or probability estimate, list of ideas with generation frequencies, etc.)
+ The summaries are given back to the group members, and they are allowed to revise their initial estimates
+ The group members are typically anonymous, and the summaries do not specify which ideas or ratings came from each member
+ This procedure allows information from the group to be shared among the group members but avoids conformity pressure or undue influence by high-status members
+ The procedure can be repeated as many times as seems warranted but is usually ended when few if any revisions are recorded
+ The final outcome can range from a frequency distribution of ideas to a choice for the most preferred outcome or the central tendency (mean or median) estimate
+ Overall, the purpose of these procedures is to allow for some information exchange while holding potential distortions due to social influence in check
+ Research on the Delphi technique has tended to show positive outcomes
  + Delphi groups do better than single individuals and do at least as well as, if not better than, face‐to‐face groups (Rorhbaugh, 1979)
  + They have also been found to work well in forecasting situations (Rowe &amp; Wright, 1999, 2001)
  + Forecasting = use of historic data to determine the direction of future trends

---
#  Judge-advisor systems 

+ One of the ways managers make decisions is through consultation (Vroom &amp; Yetton , 1973)
+ “Judge-advisor” systems approach
  + Judge makes final decision but seeks out suggestions from advisors
+ Judges should weight advice equal to own opinion unless advisors have more expertise
  + Rarely done in practice
  + Egocentric advice discounting
  
.right[&lt;img src="assets/img/image7.jpg"&gt;]

???

+ Others have argued that one of the ways managers make decisions is through consultation
  + The decision is made by the manager but only after getting advice from important members of the team or organization
+ Sniezek and Buckley (1995) referred to this as the “judge–advisor” systems (JAS) approach to group decision making
  + The judge is responsible for the final decision but they seek out suggestions from various advisors
+ Based on the idea of bracketing that we discussed earlier, the judge’s best strategy is to weight the advice as equal to their own opinion, unless the judge has far more expertise than an advisor
+ However, this is rarely done in practice
  + A vast amount of research shows judges tend to weight their own opinions more than twice as much as the advice they receive (Larrick, Mannes, &amp; Soll, 2012)
  + This has been referred to as “egocentric advice discounting” (Yaniv, 2004; Yaniv &amp; Kleinberger, 2000)

---
#  Egocentric discounting 

+ Explanations
  + Anchoring and adjustment
  + Information advantage judges have about own estimates
  + Another instance of a general egocentric bias
  + Positions are part of the self

&lt;br&gt;
.right[&lt;img src="assets/img/image8.png" height=300&gt;]

???

+ There are many explanations for the egocentric discounting effect
+ One of the earliest was based on the anchoring and adjustment heuristic we’ve already discussed in class (Tversky &amp; Kahneman, 1974)
  + Harvey and Fischer (1997) argued that the judge’s initial estimate served as an anchor and judges simply did not adjust enough once provided with the advice
  + However, studies have shown the discounting effect even when no initial evaluation is present upon which to anchor (Bonaccio &amp; Dalal, 2006)
+ Yaniv (2004) has argued that the effect is due to the information advantage judges have about their own estimates 
  + Judges should know why they chose their initial position, yet they may know very little about why advisors gave the advice they did
  + Yaniv and Choshen‐Hillel (2012) showed that forcing judges to choose initial positions based on virtually no information drastically reduced the discounting effect
  + However, Soll and Larrick (2009) found that varying the amount of information judges had about the advisors’ reasons for their choices had almost no effect
+ Krueger(2003) has argued that the effect is simply another instance of a general egocentric bias that has shown up in many domains of judgment
  + The bias leads people to focus their attention on certain aspects of the self, and they typically perceive themselves as more capable than others on average
+ Larrick, Mannes, and Soll (2012; see also Soll &amp; Mannes, 2011) also argue that judges’ positions are owned by them and become part of the self, thus making them difficult to renounce

---
#  Fully interacting groups 

+ Social sharedness underlies many of the common findings associated with group consensus (Kameda, Tindale, &amp; Davis, 2003)
+ Social sharedness is when common, task-relevant cognitions among group members influence the group
+ The greater the degree of sharedness for a particular cognition, the greater the likelihood it will influence the group decision

???

+ Most of the research on group decision making has focused on groups in which the members meet together face‐to‐face and discuss the particular decision problem until they reach consensus
+ Kameda, Tindale, and Davis (2003) argue the concept of “social sharedness” underlies many of the common findings associated with group consensus
  + Social sharedness is the idea that the common cognitions about the task that group members share have more of an influence on the decision than do similar cognitions that are not shared among the members
+ The cognitions that are shared can vary from choice preferences to information about the choices to heuristic information‐processing strategies that the members cannot even articulate
+ However, the greater the degree of sharedness for a particular task-relevant cognition, the greater the likelihood that it will influence the group decision
+ In general, social sharedness is often adaptive and probably evolved as a useful aspect of living in groups (Kameda &amp; Tindale, 2004)
  + It’s helped us effectively coordinate in groups, which had strength and safety benefits for survival
+ However, when a shared cognition doesn’t apply to the current situation, social sharedness can lead groups to make poorer decisions

---
#  Consensus as combining preferences 

+ Combinatorial approach focuses on the distribution of initial member preferences and how they are combined into a group response
+ Social decision scheme (SDS) theory (Davis, 1973)
  + A set of discrete choices are known to members and each member favors a particular choice at the beginning of discussion
  + Then attempts to describe consensus process using matrix of conditional probabilities

???

+ Early work on group decision making tended to focus on the distribution of initial member preferences and they were combined into a group response (Davis, 1969, 1973)
+ This is known as the “combinatorial approach” to group decision making (Davis, 1982)
+ One of the most widely used frameworks under this approach has been social decision scheme (SDS) theory (Davis, 1973)
  + Assumes that a set of discrete choices are known by the group members, and that each member favors a particular choice at the beginning of discussion
  + SDS theory then attempts to describe the group consensus process using a matrix of conditional probabilities, mapping different member preference distributions to different consensus choices made by the group

---
#  SDS example: Jury decisions 

&lt;br&gt;
&lt;table class="table" style="font-size: 28px; margin-left: auto; margin-right: auto;"&gt;
 &lt;thead&gt;
  &lt;tr&gt;
   &lt;th style="text-align:center;"&gt; Guilty &lt;/th&gt;
   &lt;th style="text-align:center;"&gt; Not guilty &lt;/th&gt;
  &lt;/tr&gt;
 &lt;/thead&gt;
&lt;tbody&gt;
  &lt;tr&gt;
   &lt;td style="text-align:center;"&gt; 6 &lt;/td&gt;
   &lt;td style="text-align:center;"&gt; 0 &lt;/td&gt;
  &lt;/tr&gt;
  &lt;tr&gt;
   &lt;td style="text-align:center;"&gt; 5 &lt;/td&gt;
   &lt;td style="text-align:center;"&gt; 1 &lt;/td&gt;
  &lt;/tr&gt;
  &lt;tr&gt;
   &lt;td style="text-align:center;"&gt; 4 &lt;/td&gt;
   &lt;td style="text-align:center;"&gt; 2 &lt;/td&gt;
  &lt;/tr&gt;
  &lt;tr&gt;
   &lt;td style="text-align:center;"&gt; 3 &lt;/td&gt;
   &lt;td style="text-align:center;"&gt; 3 &lt;/td&gt;
  &lt;/tr&gt;
  &lt;tr&gt;
   &lt;td style="text-align:center;"&gt; 2 &lt;/td&gt;
   &lt;td style="text-align:center;"&gt; 4 &lt;/td&gt;
  &lt;/tr&gt;
  &lt;tr&gt;
   &lt;td style="text-align:center;"&gt; 1 &lt;/td&gt;
   &lt;td style="text-align:center;"&gt; 5 &lt;/td&gt;
  &lt;/tr&gt;
  &lt;tr&gt;
   &lt;td style="text-align:center;"&gt; 0 &lt;/td&gt;
   &lt;td style="text-align:center;"&gt; 6 &lt;/td&gt;
  &lt;/tr&gt;
&lt;/tbody&gt;
&lt;/table&gt;

???

+ For example, in a six‐person group (jury) choosing between two decision alternatives (e.g., guilty vs. not guilty), there are seven ways the group members might initially array themselves across the choices: 
  + Six guilty and zero not guilty, five guilty and one not guilty,...zero guilty and six not guilty
+ The SDS matrix then maps each initial preference distribution to a distribution of group outcomes based on theory or a set of assumptions the depend on how member preferences were combined 
  + These difference consensus processes tend to represent difference decision rules (called decision schemes)
  + e.g., the majority rule describes a situation in which the majority of member preferences for one choice predict the group’s final decision

---
#  Consensus as combining preferences 

+ Majority/plurality models do a good job of representing the data
+ Majority/plurality models reflect social sharedness at the preference level
+ Majority/plurality models can lead to both “good” and “bad” decision outcomes

???

+ One key finding using this framework is that majority/plurality models do a good job of representing many group decision making situations (Kameda, Tindale, &amp; Davis, 2003; Kerr &amp; Tindale, 2004)
  + In most types of decisions, the position with the largest support faction tends to be chosen by the group
+ Kameda et al. (2003) have argued that majority/plurality models reflect social sharedness at the preference level
  + A key aspect of majority/plurality processes is that they tend to make individual level tendencies more pronounced at the group level
+ So, in situations where the outcome of a decision can be defined as “good” or “bad”, a majority/plurality process could lead groups to make better decisions than the average individual when individuals tended toward the “good” response alternative
  + However, exactly the same process could lead groups to make worse decisions than the average individual when individual preferences tended in the “bad” direction
+ Since the basic majority/plurality process pushes the group in the direction initially favored by most of its members, it can lead to either good or poor decisions, depending on how members initially lean

---
class: inverse
background-image: url('assets/img/image9.png')
background-size: contain

???

+ A major theme of much of the work on group decision making and performance over the past few decades started with a paper by Stasser and Titus (1985)
+ Using a paradigm called the “hidden profile,” they showed that information initially shared by all of the group members was much more likely to be brought up during group discussion and was much more influential in the final group decision than was information held by only one member
+ A hidden profile looks like the situation on the screen
  + Each group member receives some information
  + Some of this information is common to all group members, and some of the information is unique
  + The key feature, however, is that all of the shared information (represented in black) leads to a suboptimal outcome
  + But, if group members were able to combine all of their unique information (non-black information), they could actually achieve the optimal outcome
+ Research has since shown that groups do in fact perform better if its members share their unique information (Brodbeck et al., 2007)
+ But, it’s also quite clear that groups do not do this naturally in many settings (Stasser, 1999)
+ The fact that shared, as opposed to unshared, information plays a much larger role in most group decision settings changed the way most researchers thought about groups, and paved the way for understanding how groups process information collectively

---
#  Consensus through information processing 

+ Information asymmetries model of group decision making (Brodbeck et al., 2007)
  + Negotiation focus
  + Discussion bias
  + Evaluation bias
+ Groups make worse decisions when a hidden profile is present
  + But, this isn’t the most common group-decision environment

???

+ Nowadays, most research examines how groups reach consensus through information processing
+ Most of the research has been nicely summarize by Brodbeck et al. (2007) in their information asymmetries model of group decision making
+ The model outlines 3 categories of poor information processing in groups
  + The first category, negotiation focus, encompasses the issues surrounding initial member preferences
      + If groups view the decision making task mainly as a negotiation, members negotiating which decision should be chosen tend to focus on the choices and not on the information underlying them
  + The second category, discussion bias, encompasses issues discussion that tend to favor shared versus unshared information 
      + Items shared by many members are more likely to be discussed
  + The third category, evaluation bias, encompasses the positive perceptions associated with shared information
      + Shared information is more valid, sharing shared information leads to positive evaluations by other group members
+ These categories are good descriptions of typical group decision making, and each can lead to biased group decisions (Brodbeck et al., 2007)
+ A key aspect of the model is that the group information processing only leads to negative outcomes when a hidden profile is present
  + So, all of these categories are possible explanations for the effects observed in the hidden profile paradigm
+ Although hidden profile situations do naturally occur, they are not typical of most group-decision environments
  + e.g., in situations where members have gained their information through experience, the shared information they have is probably highly valid and more useful than unique information or beliefs held by only one member

---
#  Increasing information exchange 

+ Have groups record all of the information during group discussion
+ Set up a norm of information sharing
+ Have a leader encourage/stimulate information exchange throughout discussion
+ Set up transactive memory system
  + Certain members responsible for specific information
+ Structure task so that information is exchanged completely before discussion

???

+ Given the pervasiveness of the shared-information effect, a fair amount of research has focused on how to increase the likelihood that all relevant information is brought up during group discussion
+ One partial remedy is to make sure groups have a record of all of the information present during group discussion (Sheffey, Tindale, &amp; Scott, 1989)
  + Information is less likely to be “hidden”, but still requires attention and sharing
+ Also, setting up a norm of information sharing or having a leader who encourages and stimulates information exchange throughout the discussion have shown promise in increasing information sharing (Larson, Foster‐Fishman, &amp; Franz, 1998)
+ Instructing group members to avoid forming initial impressions or preferences, and not allowing such preferences to be shared early in the discussion has also been shown to be helpful (Larson et al., 1998; Mojzisch &amp; Schulz‐Hardt, 2010)
+ Setting up a transactive memory system (Wegner, 1987) where certain group members are responsible for certain types of information has also been shown to help groups process more information (Stasser, Vaughan, &amp; Stewart, 2000)
+ Groups that structure their tasks so that information is exchanged completely before discussion also tend to perform better (Brodbeck, Kerschreiter, Mojzisch, Frey, &amp; Schulz‐Hardt, 2002)
+ Across all of these interventions, the things that seem to be important are a focus on information rather than preferences, memory aids or reduced information load per group member, and a focus on accuracy over consensus (Brodbeck et al., 2007)

---
#  Shared conceptual system 

+ Group members’ shared conceptual system allows them to realize when a solution is correct within that system (Laughlin, 1980, 2011)
+ Background knowledge allows a minority member with correct answer to have larger influence on group
  + Minority influence
+ Intellective vs. judgmental tasks

&lt;br&gt;
.center[
&lt;img src="assets/img/image10.png" height=225&gt;
&lt;img src="assets/img/image11.png" height=225&gt;
]

???

+ Laughlin (1980, 2011) argued that one of the reasons groups are better problem solvers than individuals is that group members often share a conceptual system that allows them to realize when a proposed solution is correct within that system
+ This shared conceptual system, or background knowledge, is what allows a minority member with a correct answer to influence a larger incorrect faction to change its preference to the correct alternative
  + Called minority influence
  + “Minority” in the sense that they prefer an option that the group majority does not endorse--minority (vs. majority) faction of the group
+ Such situations are well described by SDS models called “truth wins” and “truth supported wins” (Laughlin, 1980)
  + Truth wins predicts that any group that has at least one member with the correct answer will be able to solve the problem correctly (Laughlin, 1980)
  + Truth supported wins argues that at least two members of the group must have the correct answer in order for the group to solve the problem correctly (Laughlin, 1980)
+ For groups with more than four members, both models predict minority influence for minorities with the correct answer
+ Minority influence processes are likely to occur for demonstrable or “intellective” tasks (those that have a demonstrably correct solution, like math questions) and that the shared conceptual system is a key component of demonstrability (Laughlin &amp; Ellis, 1986)
+ For “judgmental” tasks (those without a demonstrably correct solution, like movie preference), majority/plurality processes are more likely to occur
  + Group goes with the majority preference

---
#  Other group dynamics 

+ Groupthink (Janis, 1972)
  + Group members strive for unanimity over realistic courses of action
  + Occurs when involved in a cohesive ingroup

&lt;br&gt;
.center[&lt;iframe width="560" height="315" src="https://www.youtube.com/embed/_rbw8O87LZg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen&gt;&lt;/iframe&gt;]

???

+ Depending on the context, sharedness can also contribute to groupthink
+ Groupthink
  + A mode of thinking people engage in when they are involved in a cohesive ingroup
  + Occurs when group members strive for unanimity over realistically appraising the alternative courses of action
  + In other words, desire for cohesion rather than accuracy/correctness
  + Examples: bay of pigs, challenger explosion

---
#  Other group dynamics 

+ Group polarization ( Moscovici &amp; Zavalloni , 1969)
  + Tendency for groups to make more extreme recommendations than the mean of members’ pre-discussion opinions

&lt;br&gt;
.center[&lt;iframe width="560" height="315" src="https://www.youtube.com/embed/9k0WsU7cmtQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen&gt;&lt;/iframe&gt;]

???

+ Depending on the context, sharedness can lead to both positive and negative group outcomes
+ Having common background knowledge can make getting along with group members easier and with coordinating tasks within the group
+ However, it can also contribute to group polarization
+ Group polarization
  + Tendency for groups to make more extreme recommendations than the mean of members’ pre-discussion opinions, in the direction favored by the mean 
  + Examples: online echo chambers and increased political divides
    </textarea>
<style data-target="print-only">@media screen {.remark-slide-container{display:block;}.remark-slide-scaler{box-shadow:none;}}</style>
<script src="https://remarkjs.com/downloads/remark-latest.min.js"></script>
<script>var slideshow = remark.create({
"highlightStyle": "github",
"highlightLines": true,
"countIncrementalSlides": false
});
if (window.HTMLWidgets) slideshow.on('afterShowSlide', function (slide) {
  window.dispatchEvent(new Event('resize'));
});
(function(d) {
  var s = d.createElement("style"), r = d.querySelector(".remark-slide-scaler");
  if (!r) return;
  s.type = "text/css"; s.innerHTML = "@page {size: " + r.style.width + " " + r.style.height +"; }";
  d.head.appendChild(s);
})(document);

(function(d) {
  var el = d.getElementsByClassName("remark-slides-area");
  if (!el) return;
  var slide, slides = slideshow.getSlides(), els = el[0].children;
  for (var i = 1; i < slides.length; i++) {
    slide = slides[i];
    if (slide.properties.continued === "true" || slide.properties.count === "false") {
      els[i - 1].className += ' has-continuation';
    }
  }
  var s = d.createElement("style");
  s.type = "text/css"; s.innerHTML = "@media print { .has-continuation { display: none; } }";
  d.head.appendChild(s);
})(document);
// delete the temporary CSS (for displaying all slides initially) when the user
// starts to view slides
(function() {
  var deleted = false;
  slideshow.on('beforeShowSlide', function(slide) {
    if (deleted) return;
    var sheets = document.styleSheets, node;
    for (var i = 0; i < sheets.length; i++) {
      node = sheets[i].ownerNode;
      if (node.dataset["target"] !== "print-only") continue;
      node.parentNode.removeChild(node);
    }
    deleted = true;
  });
})();
(function() {
  "use strict"
  // Replace <script> tags in slides area to make them executable
  var scripts = document.querySelectorAll(
    '.remark-slides-area .remark-slide-container script'
  );
  if (!scripts.length) return;
  for (var i = 0; i < scripts.length; i++) {
    var s = document.createElement('script');
    var code = document.createTextNode(scripts[i].textContent);
    s.appendChild(code);
    var scriptAttrs = scripts[i].attributes;
    for (var j = 0; j < scriptAttrs.length; j++) {
      s.setAttribute(scriptAttrs[j].name, scriptAttrs[j].value);
    }
    scripts[i].parentElement.replaceChild(s, scripts[i]);
  }
})();
(function() {
  var links = document.getElementsByTagName('a');
  for (var i = 0; i < links.length; i++) {
    if (/^(https?:)?\/\//.test(links[i].getAttribute('href'))) {
      links[i].target = '_blank';
    }
  }
})();
// adds .remark-code-has-line-highlighted class to <pre> parent elements
// of code chunks containing highlighted lines with class .remark-code-line-highlighted
(function(d) {
  const hlines = d.querySelectorAll('.remark-code-line-highlighted');
  const preParents = [];
  const findPreParent = function(line, p = 0) {
    if (p > 1) return null; // traverse up no further than grandparent
    const el = line.parentElement;
    return el.tagName === "PRE" ? el : findPreParent(el, ++p);
  };

  for (let line of hlines) {
    let pre = findPreParent(line);
    if (pre && !preParents.includes(pre)) preParents.push(pre);
  }
  preParents.forEach(p => p.classList.add("remark-code-has-line-highlighted"));
})(document);</script>

<script>
slideshow._releaseMath = function(el) {
  var i, text, code, codes = el.getElementsByTagName('code');
  for (i = 0; i < codes.length;) {
    code = codes[i];
    if (code.parentNode.tagName !== 'PRE' && code.childElementCount === 0) {
      text = code.textContent;
      if (/^\\\((.|\s)+\\\)$/.test(text) || /^\\\[(.|\s)+\\\]$/.test(text) ||
          /^\$\$(.|\s)+\$\$$/.test(text) ||
          /^\\begin\{([^}]+)\}(.|\s)+\\end\{[^}]+\}$/.test(text)) {
        code.outerHTML = code.innerHTML;  // remove <code></code>
        continue;
      }
    }
    i++;
  }
};
slideshow._releaseMath(document);
</script>
<!-- dynamically load mathjax for compatibility with self-contained -->
<script>
(function () {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src  = 'https://mathjax.rstudio.com/latest/MathJax.js?config=TeX-MML-AM_CHTML';
  if (location.protocol !== 'file:' && /^https?:/.test(script.src))
    script.src  = script.src.replace(/^https?:/, '');
  document.getElementsByTagName('head')[0].appendChild(script);
})();
</script>
  </body>
</html>
