---
title: "History of Judgment & Decision Making"
author: "PSYC/PHIL 279, Winget"
output:
  xaringan::moon_reader:
    css: ["default", "default-fonts", "my-style.css", "metropolis-fonts"]
    lib_dir: libs
    nature:
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: false
---
#  Thinking and deciding 

“Life is the art of drawing sufficient conclusions from insufficient premises.” 
.pull-right[*-Samuel Butler*]

<br>
<div align="center">
<img src="assets/img/image1.png" width = 600>
</div>

???

+ Humans today evolved from ancestors hundreds of thousands of years ago who lived in small groups and spend most of their waking hours foraging for food
+ When we weren’t searching for food, we were looking for safe spaces to live, choosing romantic partners, and protecting ourselves from dangerous animals
+ Our success at these tasks did not come from our striking physical capacities (no claws, sharp teeth, armor, etc.), but rather from our distinctive capacity to make good decisions

---
#  Thinking and deciding 

+ This course is about judgment and decision making
  + But, it is not about *what* to choose
  + It is about *how* we choose
+ We will discuss research from psychology, economics, and philosophy
+ An important finding is that diverse people in very different situations often think about their decisions in similar ways

???

+ So our ability to make decisions is a pretty big deal, and we'll spend the entire semester unpacking this process
+ We won't talk about *what* to chose when making decisions
  + That is, we're not going to talk about the content of decisions (one person's good decision could be anothers bad decision)
+ What we will be talking about is *how* people make decisions
  + That is, we'll cover the processes and biases that lead people to choose one decision over another
+ Even though we all have different preferences and beliefs, we have a common set of cognitive processes that we use to make decisions
  + But we also all have a common set of limitations on our thinking skills that can make our choices far from optimal
  + So, we'll discuss what we do well and where we fall short when making decisions

---
#  Thinking and deciding 

+ Choosing wisely is a skill, which like any skill can be improved with practice
+ As an analogy, consider swimming

<br>
<br>
<div align="center">
<img src="assets/img/image2.png" height = 200 width = 350>
<img src="assets/img/image3.png" height = 200 width = 350>
</div>

???

+ Making wise decisions is a skill, and like any skill, it can be improved with practice
+ As an analogy consider swimming
+ When most of us enter the water for the first time, we do so with a set of muscular skills that we use to keep us from drowning
+ We also have one important bias: We want to keep our heads above the water
  + That bias leads us to assume a vertical position in the water...which is a good way to drown
  + We end up spending way too much energy trying to do this, and we get tired easily
  + It is not an optimal choice
+ Even if we know better, in moments of panic we attempt to keep our heads above the water, despite the effort compared to lying flat on the water
  + Once we overcome the heads up bias, we can survive for hours by simply lying face forward on the water with arms and legs dangling, and lifting our head only when we need a breath
+ Ordinary skills can thus be modified to cope effectively with the situation by removing a pernicious bias
+ Biases in thinking and deciding can also be overcome with practice

---
#  What is thinking and deciding 

+ Thinking is the creation of mental representations of what is not in the immediate environment
+ Seeing a green wall is not thinking, that is perceiving
  + However, imagining what the wall would look like painted blue is thinking

<div align="center">
<img src="assets/img/image4.png" width = 700>
</div>

???

+ So, what is thinking?
  + In short, it's the creation of mental representations of what *is not* in the immediate environment
+ Seeing a green wall is not thinking...that's perceiving
  + But, imagining what that wall would look like if it were repainted blue (or if it had pictures on it) is thinking
+ Noting a patient's skin is yellow is not thinking
  + Hypothesizing that the patient may suffer from liver damage is
+ Thinking tends to be primarily visual or verbal
  + But there's a lot of variation--some people use verbal mode more
---
#  Dual-process models 

+ A broad group of theories suggesting that judgments and decisions are generally carried out via two distinct kinds of mental processes
+ **Automatic (or System 1)** = fast, intuitive, parallel (i.e. simultaneous), automatic, emotion-driven, and/or not always consciously accessible processing
+ **Controlled (or System 2)** = cognitively taxing, deliberate, serial (i.e., sequential), controlled, reason-driven, and/or consciously accessible processes (Evans 2008; Sloman 1996; Stanovich & West 2000)

???

+ To simply things a bit, there are basically 2 types of thought processes
  + Automatic (system 1) and controlled (system 2)
+ These terms kind of summarize the difference between the 2
  + Automatic...
  + Controlled...

---
#  Dual-process models 

+ A prototype of automatic thinking is the thinking involved when we drive a car
+ A prototype of controlled thinking is scientific reasoning
+ It is important to recognize that much of our thinking and deciding is a blend of both automatic and controlled processes

???

+ A good example of automatic thinking is the thinking we use when driving a car
+ When driving, we respond to stimuli *not* present in the environment
+ For example, we might have an expectation that the light will be red before we get to an upcoming intersection
  + Our responses to this situation (taking foot off accelerator, preparing to brake, etc.) are so automatic that we are usually unaware of them
  + Especially true for well traveled routes (commuting to school/work)...might miss an exit if not paying attention
  + It is only when we first learn to drive that we are aware of all of the thought that goes into driving (monitoring everything)
    + But now, you can probably carry on conversations, listen to music, etc. on your drives
+ In contrast, a good example of controlled thought is scientific reasoning
  + Ideas are subject to rigorous investigation by consideration of *alternative explanations* that are plausible
+ Although we have these 2 systems of thought, most (all?) thinking is a mixture of both automatic and controlled processes
  + e.g., scientist might have an intuitive, automatic idea, but then systematically tests it with experimentation and observation

---
#  Ramifications of dual-process theory

<div align="center">
<iframe src="https://embed.theguardian.com/embed/video/commentisfree/video/2011/nov/21/daniel-kahneman-psychology-video" width="560" height="315" frameborder="0" allowfullscreen></iframe>
</div>

???

+ Daniel Kahneman explains some ramifications of dual-process theory in this video interview (4:22) with The Guardian

---
#  History of judgment and decision making 

+ Pre-1950s research was dominated by psychoanalytic theory and behaviorism

<br>
<div align="center">
<img src="assets/img/image5.png" height=325 width=425>
<img src="assets/img/image6.png" height=350 width=280>
</div>

???

+ Most of the research discussed in this class has been done in the last 60 years or so
+ Why? 
  + Because until the 1950s, psychology was dominated by two traditions:
  + Psychoanalytic theory and behaviorism
+ Neither treated thought as an important determinant of human behavior, and judgment and decision making
  + For psychoanalytic theory: Unconscious needs and desires are the primary drivers of behavior and judgment
  + Eventually, psychoanalytic theory gave way to behaviorism
    + According to behavorists, the reinforcing properties of rewards and punishments that follow a behavior are what determine whether we do something again

---
#  1950s to the present 

+ The modern field of judgment and decision making was born in the 1950s
+ The birth of the field can be traced back to three distinct research traditions
  + Behavioral economics (Simon)
  + Behavioral decision theory (Edwards)
  + Heuristics and biases (Kahneman & Tversky)

???

+ The modern field of judgment and decision making was born in the 1950s, and it took off with the cognitive revolution in psychology in the 1960s
+ What we know call the field of judgment and decision making was influenced by 3 distinct research tradtions
  + Behavioral economics, developed by Herbert Simon 
  + Behavioral decision theory, developed by Ward Edwards
  + And, heuristics and biases, put forth by Daniel Kahneman and Amos Tversky

---
#  The first behavioral economist 

+ Herbert Simon
+ Nobel Prize winner in economics
+ Bounded rationality
  + People attempt to make rational decisions, yet decision makers often lack important information, time, and cognitive capacity
+ Thus they satisfice, forgoing the “best” solution in favor of one that is acceptable or reasonable

<div align="right">
<img src="assets/img/image7.png">
</div>

???

+ The first behavioral economist, Herbert Simon, was trained in public administration and was initially interested in modeling how organizations worked (large companies, etc.)
  + What he found was that people within organizations sort of just “muddled through” 
  + They seemed to rely on a limited set of heuristics...or judgment short-cuts
  + This went against the common view in economics that people were rational agents that make ideal, profit-maximizing decisions
+ What Simon asserted was that full economic rationality was simply an unrealizable model for human judgment and decision making
+ He argued that people cannot--and do not want to--carry out the complex and time-consuming calculations that are needed to determine the ideal choice out of all possible choices
  + Instead, they simplify the choice process by searching for a satisfactory outcome (Simon, 1957)...they satisfied
+ Rather than examining all possible alternatives, people simply search until they find a solution that meets certain acceptable (rather than optimal) levels of performance

---
#  Satisficing 

+ Satisficing generally consists of three elements
  + Search for local or easy options before looking further afield
  + A stopping rule that specifies an aspiration level that must be met and therefore how long search should go
  + A simplified assessment of future value that provides a vague sense of the actual value of the choice
+ Simon emphasized that this simplified method of decision making does surprisingly well and that “bounded rationality” was an evolutionary successful adaptation

???

+ In general, satisficing consists of 3 factors
  + First, a person looks for easy/obvious choice options before doing a more thorough search
  + Second, a stopping rule needs to be specified that determines how long the person should look for more information/choices
  + Lastly, a simplified assessment of a future value of the choices provides a sense of the value of the current choices
+ Simon argued that this method of decision making leads to surprisingly good outcomes and had evolutionary advantages (conserves mental effort/time)
+ We will discuss Simon and the idea of bounded rationality in more detail in a few lectures

---
#  The first behavioral decision theorist 

+ Ward Edwards in 1954 published a review paper that introduced formal theories of decision making to psychology
+ He introduced the field to the subjective expected utility (SEU) model of decision making

???

+ Around the same time Simon was working on his model of bounded rationality, Ward Edwards published a paper that introduced formal theories of decision making
+ The first of these theories was the subjective expected utility (SEU) model of decision making

---
#  Subjective expected utility 

+ The model distinguishes between objective value (e.g., money) and subjective utility, and between objective probability and subjective probability (e.g., personal beliefs about what will occur)
+ The model also introduced the idea of predicted utility associated with a given choice
+ We will discuss SEU and other formal models of decision making later this semester

???

+ SEU makes a distinction between objective value (money) and subjective utility (happiness)
+ It also makes a distinction between objective probability (probability something will actually happen) and subjective probability (personal beliefs about what will happen)
+ It also introduced the notion of the predicted utility (predicted subjective value) a choice might have
+ Although the idea of utility had been a part of economics for centuries, it had traditionally referred to the pleasure of pain a person experienced from a choice (experienced utility) 
  + SEU modified this so that utility now referred to the predicted subjective value associated with a given choice
+ We'll spend more time discussing SEU and other formal models of JDM later in the semester

---
#  The first behavioral decision theorist 

+ Edwards not only provided influential reviews of formal decision models, he and his colleagues conducted empirical research aimed at reconciling formal models and actual behavior
+ Examined how gamblers used probabilities in Las Vegas
+ Chronicled a long list of failures of the formal models to match what gamblers actually did

???

+ In additional to introducing formal models of decision making, Edwards and his colleagues also conducted empirical research to test these models
+ Most of this work examined the probabilities gamblers used in Las Vegas
+ He recorded a long list of failures of the formal models to match actual judgment and decision making, and noted that people reacted differently to gains and losses 
  + He also noticed that people seemed to be most responsive to comparative values rather than absolute values (people care whether they get more than others, etc.)
  + Playing slot machine and win 50..fell worse if person next to you won 100 vs 10

---
#  Heuristics and biases theorists 

+ Kahneman and Tversky in the late 1960s began a program of research that has come to define judgment and decision making
+ They argued, rather than starting with formal models of decision making, one should start with the principles of perception and extend them to the kind of processing necessary to make decisions

???

+ A few years after Simon and Edwards approaches were introduced, Kahneman and Tversky introduced a program of research that has since dominated the field of JDM
+ Rather than use these formal models of decision making, they argued we should use the principles of perception to understand decision making processes

---
#  Heuristics and biases theorists 

+ Guiding paradigm for research on judgment and decision making = visual illusions
+ The logic of studying perceptual illusions is that failures of a system are often more diagnostic of the rules the system follows that are its successes

<br>
<div align="center">
<img src="assets/img/image8.png" height=325 width=400>
<img src="assets/img/image9.png" height=325 width=325>
</div>

???

+ To do this, they made use of visual illusions that were commonplace in the field of perception...much like the ones you see here
+ The idea was that by studying the failures of a particular system, we could gain more information about the rules of that system than by studying its successes
  + e.g., when asked to judge which of the 2 lines in longer in the LL picture, people tend to say the top line is longer
  + However, if we cover the arrows on the ends of the lines, it's quite easy to tell they're actually the same length
  + This error is more diagnostic of the rules of our perceptual system than if the person was able to accurately judgment the line lengths
+ The purpose of this approach is not to emphasize the errors of a system over its accuracy, but rather to find the clearest testing grounds for diagnosing the underlying processes or heuristics that people habitually employ

---
#  Models of decision making 

+ Descriptive models
  + Attempt to describe how people *actually* judge and decide, without necessarily saying what we do is good or bad
  + Dual-process models fall into this category
+ Normative models
  + Reflect optimal or ideal decision-making
  + For example, a normative decision process should be logical, consistent with one’s past decisions and preferences, and take all known relevant data into account

???

+ These research approaches have lead to roughly 3 classes of decision making models
+ The first class is descriptive models
  + Descriptive models attempt to describe how people *actually* makde judgments and decisions
  + They don't say anything about what is optimal vs. suboptimal
  + Dual-process theories fall into this category
  + Most of the models we will discuss in this course are descriptive, although we will cover a few normative models as well
+ The second class is normative models
  + Normative models reflect optimal decision making
  + Takes all known data into account
  + If a person hopes to achieve a long and healthy life, a normative model for making this happen might include doing everything research shows to improve health and well-being

---
#  Models of decision making  

+ Prescriptive models
  + Recommend a particular way in which people ought to judge and decide; it may or may not be the normative way of making decisions, but it is thought to be an improvement over what people are currently doing
+ Prescriptive models may often reflect a compromise between normative and descriptive models (Bell et al. 1988)

???

+ The third class of decision making model is prescriptive models
  + Prescriptive models make recommendations about how people should judge and decide
  + May not be normative, but usually an improvement over the current/common approach
  + Often reflect a compromise between normative and descriptive models
+ Normative models tell us what would be best, descriptive models tell us what people actually do, and prescriptive models attempt to make a reasonable, realistic recommendation for action
+ They outline how we can put our knowledge of judgment and decision making to good use in daily life to make improvements

---
#  Rationality, or what makes a “good” decision? (Baron, 2008) 

+ The best kind of thinking (i.e., rational thinking) is whatever kind of thinking best helps people achieve their goals
+ Rationality is therefore not the same as accuracy, and irrationality is not the same as error
+ Rationality is a matter of degree

???

+ Now, you might be wondering which of these approach is the "best" to use
+ Honestly, the best kind of thinking is whatever helps people achieve their goals
  + Remember, thinking and deciding was initially an adaption to achieve survival goals
+ So rationality is not synonymous with accuracy, and irrationality is not the same as error
  + We can use good methods and reach wrong conclusions
  + We can use poor methods and reach a correct answer
+ And rationality is a matter of degree
  + It makes sense to say that one way of thinking is more rational or less rational than another

---
#  Rationality and luck 

+ Good decisions are those that make effective use of available information
+ Good outcomes are ones we like
+ Good outcomes need not result from good decisions (or thinking)
+ Good outcomes can come from good fortune, following a bad decision

<br>
<br>
<br>
<div align="right">
<img src="assets/img/image10.png" width=300>
</div>

???

+ Rational or "good" decision make use of the information that's available, and good outcomes tend to be outcomes that we like
+ But sometimes, we can achieve a good outcome from a bad decision through sheer luck
  + e.g., maybe there's a good chance it's going to thunderstorm, but you refuse to carry around an umbrella with you all day
  + you might still stay dry if the rain holds off for the few moments you're outside while walking from the train to class
+ On the other hand, a decision to perform surgery could have been a rational one, even if the patient is the one in a thousand who dies on the operating table from that operation
+ When we think a decision was made badly, we try to learn some lesson from it for our future decisions
+ In general though, good decisions lead to good outcomes
+ But it is important to keep in mind that through bad luck, good decisions can lead to poor outcomes

---
#  Objections to rationality 

+ Rational thinking is not the same as cold calculation of self-interest
+ Rational thinking need not be cold
  + Emotions, in fact, are one type of evidence
  + A bad feeling about a choice is one reason not to make it
+ Rationality need not be self-interested
  + Moral goals, like concern for others, are goals we have
+ Finally, rational thinking is not the same as thinking too much
  + The amount of thinking we do should be tailored to the situation

???

+ Now, there's a common misconception that rational thinking is a cold calculation of self-interest
+ First, rational thinking doesn't need to be "cold" in the sense that no emotions are involved
  + Feelings and emotions are in fact a type of evidence/information
  + It may be rational to give weight to our bad feelings about a particular choice...feeling bad is a reason not to make the choice
+ Rationality also doesn't have to be concerned with self-interest
  + Moral goals (being fair to others) are goals all of us have
  + So a rational choice may violate self-interest to help others
+ Lastly, rational thinking is not the same as thinking a lot
  + The situation often determines the amount of thinking that's necessary
  + e.g., it wouldn't be rational to take your time to do a cost-benefit analysis if you are driving and a pedestrian is in the crosswalk
  + If you did this, you'd probably end up hitting the person when instead you should make the automatic decision to brake 

---
#  Rationality and emotion 

+ Rationality is often contrasted with emotion
+ However, emotions enter into thinking in various ways
+ Emotions can serve as evidence in decision making, and they may serve as goals of behavior
+ The point, here, is that the relation between rational thinking and emotion is more complex than a simple contrast between the two

???

+ Just to touch on this idea of emotion and rationality a bit more...
+ The 2 are often contrasted, but emotions enter into thinking in many different ways
+ Emotions can serve as evidence in decision making
  + e.g., negative emotions might tell you something is off
  + positive might tell you to continue with your current approach
+ Emotions may serve as goals of behavior, too
  + People generally want to feel good and avoid feeling bad
+ The main point is that the relation between rational thinking and emotion is more complex than a simple contrast between the two
  + We'll spend a lot of time unpacking this relationship throughout the semester
