<!DOCTYPE html>
<html lang="" xml:lang="">
  <head>
    <title>Hindsight Bias, Counterfactuals, &amp; Regret</title>
    <meta charset="utf-8" />
    <meta name="author" content="PSYC/PHIL 279, Winget" />
    <link href="libs/remark-css-0.0.1/default.css" rel="stylesheet" />
    <link href="libs/remark-css-0.0.1/default-fonts.css" rel="stylesheet" />
    <link href="libs/remark-css-0.0.1/metropolis-fonts.css" rel="stylesheet" />
    <link rel="stylesheet" href="my-style.css" type="text/css" />
  </head>
  <body>
    <textarea id="source">
class: center, middle, inverse, title-slide

# Hindsight Bias, Counterfactuals, &amp; Regret
### PSYC/PHIL 279, Winget

---

#  Hindsight: Reconstructing the past  

+ Memory for complex events is basically a reconstructive process
	+ Our recall is organized in ways that make sense of the present
	+ When we do this we often feel like “we knew it all along”

???

+ 

---
#  What is hindsight bias and how is it studied? 

+ Informally: the erroneous judgment that one “knew all along” that a particular outcome would occur
	+ Formally: there are several important components of the definition of hindsight bias, as Fischhoff (1975) first described
		+ Retroactive judgments of the probability of a specific outcome shift upwards once that outcome has been achieved
		+ Once a specific outcome has occurred, people tend to judge that the outcome was relatively inevitable
		    + Creeping determinism (Fischhoff 1975)
		+ People tend not to be consciously aware that their judgments have changed after the fact

???

+ Retroactive judgments of the probability of a specific outcome (e.g., how likely Golden State was to win the NBA championship) shift upwards once that outcome has actually been achieved
+ In other words, when we know the actual outcome, and are trying to remember our past judgment of how likely that outcome was, our remembered judgments tend to drift toward the actual outcome
+ We do the same thing when we are trying to make that judgment from the perspective of someone who does not know the actual outcome—we can’t seem to shake free of what we personally know to be the actual outcome
+ Also, once a specific outcome has occurred people tend to judge that the outcome was relatively inevitable—that it was always going to happen that way. This is called creeping determinism
+ Lastly, people don’t seem to be aware that their judgments have changed after the fact

---
#  Classic studies of hindsight bias 

.center[&lt;img src="assets/img/image1.png" width=600&gt;]

???

+ In a series of ingenious experiments, Baruch Fischhoff showed that people who know the nature of events falsely overestimate the probability with which they would have predicted them
+ In this work, Fischhoff simply asked people to predict what would happen before particular events occurred, in this case what would happen after Nixon’s trip to China in 1972
+ They rated how likely it was that various outcomes would take place (e.g., that Nixon and Mao would meet together at least once, the UIS would normalize relations with China, etc.)
+ Then, a couple of weeks later, they were asked to remember their earlier answers to the same questions (i.e., what they predicted). They were also asked to report what they believed had actually happened (e.g., did Nixon meet Mao at least once?)
+ Results showed that recall was biased in the direction of having predicted what actually happened. People’s recollections of their original likelihood ratings shifted after the fact to be closer to what they now believed to be the actual outcome. 

---
#  Classic studies of hindsight bias  

+ Previous work used a memory paradigm—ask people to recall the likelihood judgments they made before knowing the outcome
	+ Other work uses a hypothetical paradigm
	
.center[&lt;img src="assets/img/image2.png" width=600&gt;]

???

+ 

---
# Hindsight bias

.center[
&lt;iframe width="560" height="315" src="https://www.youtube.com/embed/DUUcsR-c46k" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen&gt;&lt;/iframe&gt;
]

???

+ On the investment website *The Motley Fool*, Michael Maboussin discusses how to use a journal to fight hindsight bias after seeing how one’s investments have turned out (2:25)

---
#  Explanations of hindsight bias 

+ Memory accounts
	+ **S**elective **A**ctivation, **R**econstruction and **A**nchoring 
	  + (SARA; Pohl, Eisenhauer &amp; Hardt, 2003)

&lt;br&gt;
.center[&lt;img src="assets/img/image3.png" width=600&gt;]

???

+ In general memory models suggest hindsight bias occurs because people are not always able to directly retrieve memories of judgments they made before the fact
+ Instead they sometimes need to reconstruct those memories, and as is well documented, the reconstruction process is prone to distortions
+ According to the SARA model, when people are asked to reconstruct their earlier estimate in hindsight, this reconstructed estimate is more heavily influenced by the pieces of information that are most strongly activated at the moments—that is the information that turned out to align with the correct answer

---
#  Explanations of hindsight bias 

+ Motivational accounts
	+ Reinforce self-image as knowledgeable person
	+ Feel in control of important events
	+ Motivation to understand surprising events

&lt;br&gt;
.center[&lt;img src="assets/img/image4.png" width=500&gt;]

???

+ Hindsight bias is largely caused by cognitive factors
+ However motivation may play a somewhat lesser role

---
#  Hindsight bias over the lifespan 

+ Bernstein et al. (2011) examined hindsight bias among children ages 3-15, young adults ages 18-29, and older adults ages 61-95
	+ They found that the degree to which people showed hindsight bias over the lifespan followed a U-shaped function
	+ Children and older adults showed largest bias, young adults showed smallest bias

???

+ The results for preschoolers are interesting and important because they highlight how hindsight bias can interfere with young children’s assessments of their own rate of learning
+ They have a strong tendency to feel like they “knew it all along” so they may suffer from the illusion that they have relatively little to learn
+ The stronger hindsight bias in older adults likely results from a failure to recall their initial responses or estimates of probability, making it more frequently necessary for them to reconstruct their estimates
+ This reconstruction process is particularly likely to show distortions toward current knowledge (the known outcome)

---
#  Reverse hindsight bias 

+ In certain situations, after learning the outcome of an event, people believe they “never would have seen it coming”
	+ This mostly happens for extremely surprising events

???

+ 

---
#  Counterfactual thinking (Roese, 1997) 

+ “What might have been”
	+ Counterfactual thoughts are mental representations of alternatives to past events, actions, or states

&lt;br&gt;
&lt;br&gt;
.center[&lt;img src="assets/img/image5.png"&gt;]

???

+ In everyday life, an individual’s counterfactual musings often take the form of a conditional proposition, in which the antecedent corresponds to an action and the consequent corresponds to an outcome (e.g., “If only I had studied, I would have passed the exam”)
+ Counterfactual thinking emerges quite early in development, with 2 year-olds entertaining ideas of “if only.”
+ Crucially, counterfactual thoughts are often evaluative, specifying alternatives that are in some tangible way better or worse than actuality. Better alternatives are termed upward counterfactuals...worse alternatives are termed downward counterfactuals

---
#  Time-course of counterfactual thinking 

+ According to Roese (1997) counterfactual thinking occurs in two steps or stages.
	+ **Activation** of counterfactual thought (often caused by negative affect)
	+ **Content** of counterfactual thought (determined by normality or unusualness of event, etc.)

.center[&lt;img src="assets/img/image6.png" width=600&gt;]

???

+ Activation refers to whether the process of Counterfactual generation is initially switched on or off (e.g., Bargh, 1996; Higgins, 1996), whereas content refers to the specific makeup of the resulting Counterfactual thought
+ For example, Violet has just failed an exam
+ Whether she wonders at all if she might have performed better or instead focuses only on what was reflects the issue of mere activation
+ If counterfactual processing is indeed activated, she might then muse that' 'If only I had studied harder, I would have passed.
+ The content of this particular Counterfactual focuses on an alteration to her study habits; but once activated, counterfactual content may take many forms
+ Violet may as easily have wondered whether curtailing her drinking or moving in with her aunt might have resulted in a better exam score
+ Activation and content are related but conceptually distinct aspects of counterfactual generation
+ Temporal order is an important aspect of this distinction
+ Initial activation is a necessary condition for any content effects to occur but not vice versa
+ Moreover, different variables influence these two stages of activation and content.
+ In general, affect is the main determinant of activation, whereas "normality" (i.e., whether circumstances surrounding the outcome are "normal" or unusual) is the main determinant of content

---
#  Counterfactual thinking 

+ When do we engage in counterfactuals?
	+ Exceptional or rare events
	+ Negative outcomes
	+ Closeness of outcome (e.g., just missed train)

.center[&lt;img src="assets/img/image7.png" width=500&gt;]

???

+ Outcome closeness refers to the perceived nearness of achieving a goal
+ A clear example of this is when we just missed our train or a flight
+ When this happens, thoughts of “if only I had not stopped for coffee on the way to the airport, or if I only woke up on time, etc” pop into our heads

---
#  What good is counterfactual thinking? 

+ They are functional (Epstude &amp; Roese , 2008)

&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
.center[&lt;img src="assets/img/image8.png" width=750&gt;]

???

+ According to the functional view, counterfactual thinking may be seen primarily as a useful, beneficial, and utterly necessary component of behavior regulation
+ Accordingly, counterfactual thoughts are closely connected to goal cognitions
+ Counterfactual thoughts are typically activated by a failed goal, and they specify what one might have been done to have achieved that goal (Markman et al., 1993; Roese, Hur, &amp; Pennington, 1999)
+ By imagining what might have been when something bad happens or we fail to achieve a goal, we can learn what we should do if we want to achieve it in the future or not have the bad thing happen again
+ I should have gotten up on time, I should have set my alarm earlier; I should not have started smoking; I should have taken my medication, etc
+ All of these thoughts help us develop the intention to achieve a goal or change our behavior

---
#  Counterfactual reasoning and harmful events 

+ In the context of harmful events, counterfactual reasoning entails thinking about how deleterious outcomes could have been avoided
	+ Research suggests that in considering alternatives to unfortunate outcomes, observers focus on unusual, unexpected, or exceptional circumstances (Kahneman &amp; Miller, 1986)

???

+ 

---
#  Counterfactual reasoning and harmful events 

+ Miller and McFarland (1986) examined the role of counterfactual reasoning in a legal setting
	+ Participants learned that a man was shot and injured while visiting his usual convenience store or a different one (mutable feature)
	+ Results showed that more compensation was recommended when the man was shot in the unusual store
	+ “If only the actor had gone to his usual store, he wouldn’t have been shot”

???

+ An early study by Miller and McFarland (1986, Study 1) exemplifies counterfactual reasoning explanations of harmful events. Participants in this experiment learned that a man was shot and injured while visiting his usual convenience store or a different one. The primary response measure was the amount of money participants awarded to the victim. Results showed that more compensation was recommended when the man was shot in the unusual than in the usual store. The counterfactual reasoning explanation of these results can be understood in terms of Roese’s two-stage process. First, harmful events evoke negative affect and instigate counterfactual investigations. In Miller and McFarland’s scenario, the innocent victim’s ordeal activates the counterfactual reasoning process. Second, the content of counterfactual reasoning is determined by the event’s most mutable features. The distinguishing mutable feature in these scenarios is the victim’s decision to visit a different convenience store. In this condition, counterfactual reasoning is expressed in ruminations such as “If only the actor had gone to his usual store, he wouldn’t have been shot.” Counterfactual thoughts presumably lead observers to sympathize with the victim and to award him more compensation

---
#  Counterfactual thinking: Outcomes 

+ When upward counterfactuals focus on personal choice, the resulting emotion is termed **regret**

&lt;br&gt;
.center[&lt;img src="assets/img/image9.png" width=600&gt;]

???

+ 

---
#  Misprediction of regret (Gilbert et al., 2004) 

+ Regret is a combination of disappointment and self-blame
	+ We base many of our decisions on anticipated regret (Zeelenberg et al., 1998)
	+ “What if I find a better deal on this car?”
	+ “If I don’t study I will regret it”
	
.center[&lt;img src="assets/img/image10.png" width=550&gt;]

???

+ 

---
#  Misprediction of regret (Gilbert et al., 2004) 

+ We expect regret to be increased by factors that highlight our personal responsibility for a negative outcome
	+ Missing an airplane by a few minutes, or a gold medal by a few milliseconds

&lt;br&gt;
.center[	
&lt;img src="assets/img/image11.png" height=225&gt;
&lt;img src="assets/img/image12.png" height=225&gt;
]

???

+ This is why bronze medalists are often happier than silver medalists; they smile more on the medal platform. The reason is that it is easier for the silver medalist to imagine that they could have won the gold if only they had done x, y, or z. It is easier for them to come up with a counterfactual situation in which they would have won, whereas the bronze medalist has a much more difficult time doing so because they were not as close to winning. Etc.

---
#  Misprediction of regret 

+ People expect to blame themselves more when they fail by a narrow margin than a wide margin
	+ If so, people should expect to feel more regret in the former than latter situation
	+ But, we tend to rationalize or avoid self-blame quite easily
	+ If so, margin should have little impact on actual experience of regret

???

+ 

---
#  Misprediction of regret 

.pull-left[&lt;img src="assets/img/image15.png"height=450&gt;]
.right[
&lt;img src="assets/img/image13.png" height=225 width=325&gt;
&lt;img src="assets/img/image14.png" height=225 width=325&gt;
]

???

+ 

---
#  Regret on the subway 

+ Ps were approached as they entered the track at a subway station
	+ Those that arrived 30-90s after the train left told that they had missed it by 1min (narrow)
	+ Those that arrived 3.5-6min after train left were told they missed by 5min (wide)
	+ Forecasters asked to predict how they would feel if they missed the train by 1min or 5min

???

+ 

---
#  Regret 

+ Size of margin influenced forecasts of regret, with a narrow margin believed to produce more regret than a wide margin
	+ Size of margin did not influence actual experience of regret
	+ Forecasters also overestimated how disappointed they would be (margin had no influence on this, as disappointment doesn’t involve self-blame

???

+
    </textarea>
<style data-target="print-only">@media screen {.remark-slide-container{display:block;}.remark-slide-scaler{box-shadow:none;}}</style>
<script src="https://remarkjs.com/downloads/remark-latest.min.js"></script>
<script>var slideshow = remark.create({
"highlightStyle": "github",
"highlightLines": true,
"countIncrementalSlides": false
});
if (window.HTMLWidgets) slideshow.on('afterShowSlide', function (slide) {
  window.dispatchEvent(new Event('resize'));
});
(function(d) {
  var s = d.createElement("style"), r = d.querySelector(".remark-slide-scaler");
  if (!r) return;
  s.type = "text/css"; s.innerHTML = "@page {size: " + r.style.width + " " + r.style.height +"; }";
  d.head.appendChild(s);
})(document);

(function(d) {
  var el = d.getElementsByClassName("remark-slides-area");
  if (!el) return;
  var slide, slides = slideshow.getSlides(), els = el[0].children;
  for (var i = 1; i < slides.length; i++) {
    slide = slides[i];
    if (slide.properties.continued === "true" || slide.properties.count === "false") {
      els[i - 1].className += ' has-continuation';
    }
  }
  var s = d.createElement("style");
  s.type = "text/css"; s.innerHTML = "@media print { .has-continuation { display: none; } }";
  d.head.appendChild(s);
})(document);
// delete the temporary CSS (for displaying all slides initially) when the user
// starts to view slides
(function() {
  var deleted = false;
  slideshow.on('beforeShowSlide', function(slide) {
    if (deleted) return;
    var sheets = document.styleSheets, node;
    for (var i = 0; i < sheets.length; i++) {
      node = sheets[i].ownerNode;
      if (node.dataset["target"] !== "print-only") continue;
      node.parentNode.removeChild(node);
    }
    deleted = true;
  });
})();
(function() {
  "use strict"
  // Replace <script> tags in slides area to make them executable
  var scripts = document.querySelectorAll(
    '.remark-slides-area .remark-slide-container script'
  );
  if (!scripts.length) return;
  for (var i = 0; i < scripts.length; i++) {
    var s = document.createElement('script');
    var code = document.createTextNode(scripts[i].textContent);
    s.appendChild(code);
    var scriptAttrs = scripts[i].attributes;
    for (var j = 0; j < scriptAttrs.length; j++) {
      s.setAttribute(scriptAttrs[j].name, scriptAttrs[j].value);
    }
    scripts[i].parentElement.replaceChild(s, scripts[i]);
  }
})();
(function() {
  var links = document.getElementsByTagName('a');
  for (var i = 0; i < links.length; i++) {
    if (/^(https?:)?\/\//.test(links[i].getAttribute('href'))) {
      links[i].target = '_blank';
    }
  }
})();
// adds .remark-code-has-line-highlighted class to <pre> parent elements
// of code chunks containing highlighted lines with class .remark-code-line-highlighted
(function(d) {
  const hlines = d.querySelectorAll('.remark-code-line-highlighted');
  const preParents = [];
  const findPreParent = function(line, p = 0) {
    if (p > 1) return null; // traverse up no further than grandparent
    const el = line.parentElement;
    return el.tagName === "PRE" ? el : findPreParent(el, ++p);
  };

  for (let line of hlines) {
    let pre = findPreParent(line);
    if (pre && !preParents.includes(pre)) preParents.push(pre);
  }
  preParents.forEach(p => p.classList.add("remark-code-has-line-highlighted"));
})(document);</script>

<script>
slideshow._releaseMath = function(el) {
  var i, text, code, codes = el.getElementsByTagName('code');
  for (i = 0; i < codes.length;) {
    code = codes[i];
    if (code.parentNode.tagName !== 'PRE' && code.childElementCount === 0) {
      text = code.textContent;
      if (/^\\\((.|\s)+\\\)$/.test(text) || /^\\\[(.|\s)+\\\]$/.test(text) ||
          /^\$\$(.|\s)+\$\$$/.test(text) ||
          /^\\begin\{([^}]+)\}(.|\s)+\\end\{[^}]+\}$/.test(text)) {
        code.outerHTML = code.innerHTML;  // remove <code></code>
        continue;
      }
    }
    i++;
  }
};
slideshow._releaseMath(document);
</script>
<!-- dynamically load mathjax for compatibility with self-contained -->
<script>
(function () {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src  = 'https://mathjax.rstudio.com/latest/MathJax.js?config=TeX-MML-AM_CHTML';
  if (location.protocol !== 'file:' && /^https?:/.test(script.src))
    script.src  = script.src.replace(/^https?:/, '');
  document.getElementsByTagName('head')[0].appendChild(script);
})();
</script>
  </body>
</html>
