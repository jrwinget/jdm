<!DOCTYPE html>
<html lang="" xml:lang="">
  <head>
    <title>Risk Perception</title>
    <meta charset="utf-8" />
    <meta name="author" content="PSYC/PHIL 279, Winget" />
    <link href="libs/remark-css-0.0.1/default.css" rel="stylesheet" />
    <link href="libs/remark-css-0.0.1/default-fonts.css" rel="stylesheet" />
    <link href="libs/remark-css-0.0.1/metropolis-fonts.css" rel="stylesheet" />
    <link rel="stylesheet" href="my-style.css" type="text/css" />
  </head>
  <body>
    <textarea id="source">
class: center, middle, inverse, title-slide

# Risk Perception
### PSYC/PHIL 279, Winget

---

#  Risks versus benefits 

+ Risk-benefit analyses (cost-benefit analyses)
  + Potential harms are weighed against potential benefits to determine whether a particular action should be taken
+ Starr (1969) estimated societal benefits in terms of the monetary value of the activity (money spent on or earned from an activity), and risks in terms of the number of deaths expected per hour of exposure to that activity

&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
.right[&lt;img src="assets/img/image12.png" width=350&gt;]

???

+ Everyday, we’re bombarded with headlines of negative events that could happen again at any time
  + e.g., hurricanes, shootings, terrorist attacks
+ And so researchers who study risk perception are interested in how people judge risk for these events
  + how do we try to control our uncertain futures
  + how much risk are we willing to accept
  + how do our risk perceptions align with reality
+ One way of going about this is to conduct a risk-benefit analysis
  + Basically, when making a decision, we weight the potential harms of an outcome against its potential benefits
  + If the risks outweigh the benefits, it's considered a bad decision
  + e.g., by increasing speed limit, commuters would reach destinations more quickly but a small number of additional fatal accidents are likely to occur
+ Starr (1969) was the first to suggest risk-benefit analyses could be used to balance societal benefits of activity against accidental deaths of partaking in that activity
  + societal benefits = monetary value of activity (e.g., $ spent on or earned from activity)
  + risks = # of deaths expected per hour of exposure to that activity
  + e.g., benefit of earning a salary by working in a coal mine can be weighed against the risk of dying in an accident in that coal mine

---
#  Risks versus benefits 

+ Starr’s (1969) analyses suggest as risk increases for an activity, people are exponentially more willing to spend money (or require exponentially more pay) to engage in that activity
+ The analyses also suggest people were willing to take on about 1000x more risk for voluntary activities than for involuntary ones with the same societal benefits

&lt;br&gt;
.center[
&lt;img src="assets/img/image13.jpg" height=200&gt;
&lt;img src="assets/img/image14.jpg" height=200&gt;
]

???

+ Starr (1969) found that as risk increased for a particular activity, people were much more willing to spend money to engage in that activity
+ What’s even more interesting is that people were willing to take on about 1000x more risk for voluntary activities (e.g., ones we do for fun, like downhill skiing) than for involuntary ones (e.g., ones we do to survive...eating food with preservatives)...even though both activities had the same societal benefits per hour of exposure
+ But, things are a bit more complicated than this...
  + Starr’s approach was good because it used actual behaviors as estimates of social benefits and risks (measurements of what people are really paying/earning for activity and actual death rate)...these are more accurate than judgments of behavior
  + But the drawback is that it doesn’t tell us how people’s attitudes toward an activity would change if they were presented with new information
      + e.g., suppose a person is working in a coal mine at a certain level of risk of death, but this person doesn’t realize there are additional safety measures that could cut that risk of death in half
      + once the option of additional safety is presented, the worker might change their attitude on how safe their workplace actually is
  + So, to overcome the limitations of Starr’s approach, researchers asked people directly to make judgments of the benefits and risks of a variety of activities 

---
#  Direct questioning of risks and benefits 

+ This approach allows one to determine people’s beliefs about the level of safety for different activities
+ And their preferences regarding implementation of new rules and regulations to reduce risk

&lt;br&gt;
&lt;br&gt;
.right[&lt;img src="assets/img/image15.png" height=300&gt;]

???

+ Research using this new approach found that most people wanted most risks to be lowered by deliberately increasing rules and regulations for activities
  + mandatory top speed limits, mining safeguards, etc.
+ People basically wanted the riskiest activity to be no more than ten times as risky as the safest activity

---
#  Risks versus benefits 

+ Slovic's (1987) basic dimensions of risk:
  1. Unknown (vs. known) risk
  1. Dread risk

&lt;br&gt;
.center[&lt;img src="assets/img/image6.png" width=600&gt;]

???

+ There are at least 2 major characteristics of potential dangers people take into account when deciding how much risk they are willing to take
  + The first dimension is called unknown risk
      + Has to do with the degree to which people feel they understand the risk
      + Ranges from known risk to unknown risk
  + The second dimension has to do with how much the potential danger drives people’s feeling of dread
      + dread is basically negative affect or feelings related to the risk
      + Ranges from the absence of dread risk to dread risk

---
background-image: url('assets/img/image8.jpg')
background-size: cover

???

+ Some risks are new, not well understood by science, unobservable, and unknown to those exposed to it
  + risk high on this dimension are called unknown risks
      + e.g., newer technology, diseases not well understood
  + at the other end of this dimension are known risks
      + older, well-researched, observable, and known to those who have been exposed to it
      + e.g., risks posed by choosing to fly on an airplane
+ Dread risk are risks that cause people to feel a lot of "dread" (negative affect) 
  + Have “catastrophic” potential, will result in fatalities, and are seen as being uncontrollable
      + the risk and benefits are not seen as being fairly distributed among individuals
      + e.g., risks from nuclear power, risks from bioterrorism
  + Risks that fall on the low end of this dimension are characterized by the absence of dread
      + e.g., risk posed by falling down the stairs, risk of foregoing annual flu shot
+ For example, deaths from driving would be a non-dread risk
  + even though driving kills many more people per year in the USA, it's not accompanied by feelings of dread so people don’t worry about it as much
  + But, the risk of terrorism is accompanied by feelings of dread, so people think it’s more risky than driving...even though the chances of dying in a terrorist attack are orders of magnitude lower than dying in a car crash
  + You have just as much chance of dying in a terrorist attack as being killed by a falling TV or furniture

---
#  Dread risk (Slovic, 1987) 

+ Primarily in lay people, who lack statistical knowledge, use emotion as a guide to action, and tend to err on the side of safety
+ They simply avoid dread-risk situations that they feel they don’t understand
+ In a statistical sense, experts are more rational, influenced more purely by mortality rates

???

+ In their work on dread risk, Slovic and colleagues found that the more dread people felt toward an activity, the more risky they believed it had
+ What's interesting is the effects of dread risk seems to mostly in lay people 
+ People who know about risk assessments or have statistical knowledge ("experts") are influenced more by mortality rates
  + in other words, experts tend to be more rational and statistically driven in their perceptions of risk
+ Lay people, on the other hand, tend to be swayed more by the psychological perception of dread
  + lacking statistical knowledge, they use emotion (dread) as a guide to action and err on the side of safety
  + so, they simply avoid dread-risk situations that they feel they don’t understand

---
#  Slovic (1987) 

.pull-left[&lt;img src="assets/img/image4.png"&gt;]
.pull-right[&lt;img src="assets/img/image3.png"&gt;]

???

+ You can see this discrepancy between lay people and experts in this table from Slovic’s 1987 article
+ In the table, different groups of lay people and experts rated the risk of various activities
  + Point out discrepancy 
+ Nuclear power was a big deal at the time because of the 1979 Three Mile Island accident and the Chernobyl disaster 
  + If you haven't seen it, the mini-series on HBO is good by the way!

---
#  Analyses: Avoiding dread risks of terrorism 

+ Elevated highway deaths following 9/11 by people avoiding flying
+ Was this avoidance *voluntary*?
  + Analysis of subway use following the 2005 London Underground bombing
  + Accounted for structural damage, weather, summer holidays, etc.
  + Still found avoidance of dread risks

&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
.right[*Gigerenzer (2004); Prager et al. (2011)*]

???

+ Researchers have pointed out that our extreme avoidance of dread risks leads us to engage in activities that are actually riskier than the ones we're avoiding
+ Take the 9/11 terrorist attacks
  + The attack involved planes flying into buildings and was plastered all over the news for months
  + This heightened the fear of dread risk involved with flying, and subsequently caused people to drive instead of flying
  + This increased driving, which is actually *more* risky than flying, caused more deaths
  + According to the research, an additional 350 Americans lost their lives
+ A similar effect happened after the London Underground bombing, with people avoiding the subway system and instead driving their cars

---
#  Theory: Global warming (Weber, 2006) 

+ Science has reached a consensus on global warming (Ding et al., 2011)
+ Weber: Why have so few governments addressed it?
  + Most lay people learn about it in terms of statistics (processed by System 2)
  + But, System 1 reasoning motivates action

&lt;br&gt;
.center[&lt;img src="assets/img/image16.jpg" width=600&gt;]

???

+ So negative emotions may be a strong motivating factor in our tendency to overprotect ourselves from dread risks
+ An interesting flip side of this is global warming, or climate change that is caused by humans
+ In this case, many people don't feel negative affect toward this particular risk, and tend to be relatively apathetic about it
+ And we're currently watching this unfold...scientists have long reached an overwhelming consensus on the dangers of global warming but few governments have taken steps to address it until very recently
+ In most cases, lay people are only exposed to information about global warming in abstract, statistical formats
  + They hear things like..."the earth will warm by X degrees by year X"
+ Such information tends to by processed by System 2, rather than System 1 (which processes dread risks)
+ System 2 is less likely to motivate people to take drastic action for change than System 1
+ Climate change experts, on the other hand, have directly observed the melting glaciers and other effects of climate change, so they see it as a dread risk in dire need of drastic change
  + The information climate change experts receive includes direct, vivid experiences, which influence System 1 reasoning and are most likely to motivate action
  + But, most members of the public have not had this direct experience, and their System 1 reasoning isn’t triggered

---
#  Study: Have you personally experienced global warming? 

+ Those who felt they had estimated greater risk than those who didn’t feel they had  (Akerlof et al., 2012)
  + Controlled for political affiliation, demographics, personal beliefs, etc.
  + Personal experiences were supported by actual climate data recorded by the U.S. National Oceanic and Atmospheric Administration

???

+ Additional support for this claim is that people who actually have had direct, personal experiences with global warming tend to have elevated risk estimates of global warming
  + These are people that have seen changes in seasons, weather, animals, hurricanes, etc.
+ Their judgments of risk tend to be more similar to experts than people with no personal experience

---
#  Understanding risk by numbers 

+ What if we just give people the correct numbers, surely this would make them behave more rationally?

+ Nope, not really

&lt;br&gt;
.center[&lt;img src="assets/img/image17.png" height=375&gt;]

???

+ So a reasonable question, then, is "What if we just give people the correct numbers, surely this would make them behave more rationally?"
  + And the answer is "Nope, not really"
+ The problem is that people don’t know what to do with the numbers when they are given to them
+ They find them difficult to use, and sometimes even completely ignore them
+ Some of the most striking examples of this come from studies examining how people reason about medical screening tests
+ As we will see, people’s perception of risk are strongly influenced by the format in which numerical risk information is presented

---
#  Hypothetical scenario: “Hannah” 

+ Down syndrome screening in utero
+ Hannah’s risk:
  + “Elevated risk”
  + “8 times higher than the average woman of her age”
  + “1 in 125 chance”
+ All technically true, but each format creates a different affective reaction

&lt;br&gt;
&lt;br&gt;
.right[&lt;img src="assets/img/image18.jpg" height=250&gt;]

???

+ For example, consider this hypothetical scenario...
+ Hannah is pregnant for the first time and her doctor offers a routine screening for Down Syndrome in utero
+ Average risk of a woman her age is 1 in 1000, and Hannah's results suggest her baby has less than a 1% chance of having Down Syndrome
+ The results Hannah's risk can be described in a few ways...
  + “Elevated risk”
  + “8 times higher than the average woman of her age”
  + “1 in 125 chance”
+ While they are all technically true, each format creates a very different affective reaction, which guides people’s judgments about the levels of risk involved
+ So they way we present information matters

---
#  Ways of presenting statistical information 

+ Absolute versus relative risks
+ The contraceptives/blood clot scare in mid-1990s Britain
  + Relative risk increase (twofold increase)
  + Absolute risk increase (2 in 7000 vs 1 in 7000 risk)

???

+ People have an easier time judging health risks when presented with absolute risks instead of relative risks
  + absolute risk = level of risk overall...not taking any other factors into account
  + relative risk = level of risk in relation to some other level of risk
+ For example, in the mid-1990s, research showed for every 7,000 women taking a *new* contraceptive, 2 women developed dangerous, potentially fatal blood clots in the legs or lungs
  + For comparison, the *old* contraceptive risk was 1 in 7000
  + UK committee made an emergency announcement based on these findings...said it led to a “twofold” increase in the incidence of life-threatening blood clots
  + This is true, but when public was given only the “twofold increase” statistic without knowing the overall rarity of these clots, the result was pretty much widespread panic
  + the relative risk ("twofold increase") is much scarier than the absolute risk (“2 in 7,000 vs. 1 in 7,000”)

---
#  Conditional probabilities 

+ P(X|Y)
  + Probability of X given Y has occurred
+ P(Dilly committed the crime | Her partner said she did it)
+ Important, but people are bad at this

&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
.right[&lt;img src="assets/img/image19.jpg" width=350&gt;]

???

+ People also have an easier time judging risks when they are presented with natural frequencies instead of conditional probabilities
+ A conditional probability is the probability of X given that Y has occurred
+ denoted as P(X|Y)
  + e.g., P(Dilly committed the crime | Her partner said she did it) = the probability Dilly committed the crime given Dilly’s partner said Dilly did it
+ Conditional probabilities provide extremely useful information to assess risk, but time and time again, research shows that we're bad at understanding these
+ So, let’s break things down in an example...

---
#  Breast cancer screening 

+ Suppose you are given the following information:
  + 1% of women being screened have breast cancer
  + Among women who have breast cancer, there is an 90% chance of a positive test (successful detection)
  + Among women who do not have breast cancer, there is a 9% chance of a false positive
+ Now, suppose a woman has a positive mammogram
  + What is the probability she really has breast cancer?

&lt;br&gt;
.center[&lt;img src="assets/img/image20.png" height=250&gt;]

???

+ Read slide
+ When this scenario was given to physicians, 95% of them estimated the probability between 70-80%
+ It seems like they most likely confused the probability of really having breast cancer with the probability of having a positive mammogram. 
+ But, the correct answer is dramatically lower...NEXT SLIDE

---
#  Bayes' Theorem 

+ Must be used to calculate conditional probabilities, taking base rates into account
  + Most important: P(X|Y) is **NOT** the same as P(Y|X)!!!
  + However, people rarely realize this

&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
.center[&lt;img src="assets/img/image9.png" width=700&gt;]

???

+ To actually calculate the correct probability, we need to use Bayes Theorem
+ This whole problem is essentially a conditional probability
+ And to calculate a conditional probability, we need to take into two things
  + First, we need to know the base rate of the phenomenon we’re interested in 
  + Second, we need to be aware the probability of P(Cancer|Positive) is not the same as P(Positive|Cancer)! 
      + This is a common misunderstanding
+ Briefly walk through formula
  + "~" is equivalent to “not” or “the absence of”

---
background-image: url('assets/img/image11.png')
background-size: contain

???

+ If you do the math, the correct answer is 9% or .09
+ If you find this line of reasoning and these calculations easy to understand intuitively, then you're the exception and should definitely pursue a career in statistics
+ Bc the vase majority of people do not find reasoning with conditional probabilities at all intuitive
+ Even domain experts, like physicians, fail at this even though they routinely communicate to their patients what results of screening tests mean in terms of risk, etc.

---
#  Base rate neglect: Taxicab problem

+ Two cab companies operate in River City, the Blue and the Green, named according to the colors of the cabs they run
  + 85% of the cabs are Blue and the remaining 15% are Green
+ A cab was involved in a hit-and-run accident at night
+ An eyewitness later identified the cab as Green
+ The court tested the witness ’ s ability to distinguish between Blue and Green cabs under nighttime visibility conditions (identical to the time of the accident)
  + The witness was able to identify each color correctly 80% of the time, but confused it with the other color 20% of the time
+ *What do you think is the probability that the cab in the accident was Green, given what the witness claimed?*

&lt;br&gt;
.center[
&lt;img src="assets/img/image21.png" height=150&gt;
&lt;img src="assets/img/image22.png" height=150&gt;
]

???

+ Another example of how we do not really understand conditional probabilities is the taxicab problem
  + Read slide
+ Let class try to answer the problem...if time allows
  + If someone gets it right, have them explain their reasoning

---
#  Base rate neglect: Taxicab problem 

+ Most people say 80%, but this is incorrect
  + Bayes ’ Theorem must be applied
+ P(X|Y) is *not* the same thing as P(Y|X)
  + The probability that the Cab was really Green given that the Witness said it was Green (which is what the last sentence is asking) is not the same thing as the probability that the Witness said it was Green given that the Cab is really Green (which is where the 80% figure comes from)

&lt;br&gt;
.center[
&lt;img src="assets/img/image22.png" height=150&gt;
&lt;img src="assets/img/image23.png" height=150&gt;
]

&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
.right[*Bar-Hillel (1980)*]

???

+ When trying to figure out the probability that the cab in the accident was Green, given what the witness claimed, it seems easiest to take info about how accurate witness tended to be (80%) and assume that to be the correct answer
  + Problem with this approach: ignores fact that there are many more Blue cabs (85%) in the city than Green (15%)--base rates of cabs
+ Remember, P(X|Y) is not the same as P(Y|X)
+ So, we should use Bayes’ Theorem...

---
background-image: url('assets/img/image10.png')
background-size: contain

???

+ Remind them of Bayes’ Theorem (in square box at top of slide)
  + Substitute for variables in the current problem
+ Trying to solve for P(G|”g”), the probability the cab was Green given the probability the witness correctly said it was Green
  + Starting with the numerator, the first term is the probability the witness correctly called the cab Green given the probability the cab was Green...P(“g”|G)
      + This is basically the probability the witness accurately gives the right color = 80% = .80
  + The next term in the numerator, P(G), is the probability the cab is Green
      + This is the base rate of the Green cabs in the city = 15% = .15
  + Moving to the denominator, the first term is P(“g”|G)
      + This is the same as the first term in the numerator, and this represents the probability the witness accurately gives the right color = 80% = .80
  + Next, we have another familiar term, P(G), is the probability the cab is Green
      + Base rate of Green cabs in the city = 15% = .15
  + Next, we have a new term, P(“g”|~G), the probability the witness correctly recalled a Green cab given the probability the cab was not Green
      + This represents the probability the witness incorrectly identified a Green cab = 20% = .20
  + Finally, the last term in the denominator, P(~G), is the probability the cab was not Green
      + This represents the base rate of non-Green (i.e., Blue) cabs in the city = .85% = .85
  + When we input these probabilities into the theorem, we find there is a .41 probability the cab involved in the accident was Green given what the witness claimed

---
#  Using natural frequencies to solve the same problem 

+ Use a diagram
  + Imagine that there are 100, or 1000 people (to make calculations easy)
  + Break down the base rates first
  + Then, the conditional probabilities

+ *For example…*

???

+ One of the key issues with these problems seems to be noticing and using base rates
+ Using natural frequencies is a bit more user-friendly to see why base rates matter without have to use Bayes’ Theorem or potentially even a calculator
+ One easy approach is to make a diagram...

---
#  Alternate method: Diagrammatic summary 

&lt;br&gt;
.center[
&lt;img src="assets/img/image2.png"&gt;
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;

The witness says "Green" 12 + 17 times out of 100

Correct 12 times, so P(G|"g") = 12 / (12 + 17) = .41
]

???

+ Start with the total number of taxicabs = 100
  + out of every 100 cabs in city, 15 are Green and 85 are Blue
  + If witness gives correct color 80% of the time and confuses color 20% of time, then when viewing the Green cabs, they will correctly say 15 x .80 = 12 are Green and incorrectly that 15 x .20 = 3 are Blue
  + Out of the 85 Blue cabs, they will correctly say 85 x .80 = 68 are Blue and incorrectly say 85 x .20 = 17 are Green
  + Important to keep track of what is a correct vs. incorrect identification
      + For green cabs, when witness says green, that’s when .80 probability is applied
      + For blue cabs, when witness says blue, that’s when .80 probability is applied
  + So, out of 100 cabs, they will identify 12 + 17 = 29 as Green 
      + And they will be correct only 12 of those times
  + Because 12 / 29 = .41, the probability that the cab in the accident is Green is 41%

---
#  Breast cancer problem 

+ The prevalence of breast cancer is 1% for women over age 40
+ A widely used test, mammography, gives a positive result in 9% of women without breast cancer, and in 90% of women with breast cancer
+ What is the probability that a woman in this age class who tests positive actually has breast cancer?

&lt;br&gt;
+ **Results:**
  + 95% practicing physicians said 70-80%
  + Incorrect by a wide margin (actual prob. is 9%)

&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
.right[*Eddy (1982)*]

???

+ In a similar study, Eddy (1982) presented physicians with the following information:
  + 1% of women taking a routine breast cancer screening actually have breast cancer
  + Among women who do not have breast cancer, there is an 80% chance of a positive mammogram (cancer will successfully be detected)
  + Among women who do not have breast cancer, there is a 10% chance of a positive mammogram (false positive result)
  + Then, physicians were asked to suppose a woman has a mammogram and to estimate the probability that she really had breast cancer
+ In the study 95 of 100 physicians estimated this probability between 70-80%, most likely confusing the probability of having breast cancer with the probability of of having a positive mammogram
  + The correct answer is 7.5%
+ But, we know Bayes’ Theorem and natural frequencies now, so we can check this for ourselves...

---
background-image: url('assets/img/image1.png')
background-size: contain

???

+ On the right is the Bayes Theorem version that we saw a few slides ago, and on the right is the problem posed as natural frequencies
+ Both give the right answer, but people are much better at getting the correct answer when they are given natural frequencies
+ The frown is there because that is the hard way, and the smile is there because that is the easy (or easier) way

---
#  Natural frequencies 

+ In real life, we see examples of people, not percentages
+ So, we are better at using frequencies than percentages
+ Yet, few physicians have been trained to convert percentages to natural frequencies (Wegwarth &amp; Gigerenzer, 2011)

???

+ Because we often see examples of natural frequencies of people rather than probabilities or percentages, we’re better at understanding and using them
+ In fact, research has shown that teaching physicians to convert conditional probabilities to natural frequencies before thinking about diagnostic steps has improved diagnosis rates
+ However, few physicians are actually trained to do this despite suggestions to medical schools by researchers

---
#  “If we’re so dumb, how come we’re so smart?” 

+ Heuristics allow for fast decisions without thinking hard
+ Usually, an approximately correct solution is good enough
+ Fast and frugal heuristics
  + Emphasizes how heuristics are adaptive

.center[&lt;img src="assets/img/image24.jpg" height=250&gt;]

.right[*(Glymour, 2001)*]

???

+ With all these judgmental errors, many concluded that people were ill equipped to reason under uncertainty
+ But, the human capacity for judgment under uncertainty is actually quite impressive...especially when comparing it to AI or something of the sort
  + People, but not computers, routinely make successful uncertain inferences on a wide range of complex real-world tasks
+ As Glymour (2001) asked, “If we’re so dumb, how come we’re so smart?”
+ The answer lies in heuristics
  + They allow us to make quick, often complex decisions without much cognitive effort
  + In most cases, they get us close to the correct answer and this is often good enough
  + For this reason, heuristics are often called fast and frugal, because they are adapative strategies they help us navigate our complex worlds

---
#  High-stakes decisions 

+ Deviations from optimal decisions:
  1. Assume probability of adverse events is zero
  1. Overweight the present, ignore the future
  1. Affect heuristic: base judgment on emotion
  1. Stress predicts use of simplifying heuristics
  1. Reliance on social cues and procedures
  1. Status quo bias: choose to do nothing

&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
.right[*(Kunreuther et al., 2002)*]

???

+ Many of the risks we’ve talked about have very low base rates and potentially extremely serious outcomes...like terrorism
+ These are also known as high-stakes decisions
+ There are 6 ways people’s high-stake decisions deviate from regular decisions
  1. People behave as though rare outcomes with high-stakes losses have a probability of 0, resulting in a lack of preparation
  1. Decision makers tend to be shortsighted, overweighting concerns about the immediate future and paying relatively little attention to the extended future
  1. Affect is a major player in swaying high-stakes decisions, especially when the decision is complex or when the optimal course of action isn’t clear
  1. High-stakes decisions induce stress, which has been shown to alter decision-making 
  1. Because high-stakes decisions are so rare, people rarely have direct prior experience reasoning about them and tend to rely on social cues
  1. When faced with high-stake decisions, people often succumb to the status quo bias
+ Very little room to learn in these situations
  + Events are extremely rare
  + People rarely receive feedback

---
#  Wrap-up 

+ When the risks of an activity go up, people believe the benefits should go up exponentially
+ If risks and benefits do not have this relationship, people believe regulations should be put into place to ensure adequate safety to offset risks
+ We are disproportionately afraid of dread risks
+ We are strongly affected by format in which we are given risk information
+ We are confused by relative risks and conditional probabilities, but are really quite good at understanding absolute risks and natural frequencies
+ When faced with high-stakes decisions, we may often fail to make normative judgments

.right[&lt;img src="assets/img/image24.png" height=200&gt;]

???

+ Summarize slide
    </textarea>
<style data-target="print-only">@media screen {.remark-slide-container{display:block;}.remark-slide-scaler{box-shadow:none;}}</style>
<script src="https://remarkjs.com/downloads/remark-latest.min.js"></script>
<script>var slideshow = remark.create({
"highlightStyle": "github",
"highlightLines": true,
"countIncrementalSlides": false
});
if (window.HTMLWidgets) slideshow.on('afterShowSlide', function (slide) {
  window.dispatchEvent(new Event('resize'));
});
(function(d) {
  var s = d.createElement("style"), r = d.querySelector(".remark-slide-scaler");
  if (!r) return;
  s.type = "text/css"; s.innerHTML = "@page {size: " + r.style.width + " " + r.style.height +"; }";
  d.head.appendChild(s);
})(document);

(function(d) {
  var el = d.getElementsByClassName("remark-slides-area");
  if (!el) return;
  var slide, slides = slideshow.getSlides(), els = el[0].children;
  for (var i = 1; i < slides.length; i++) {
    slide = slides[i];
    if (slide.properties.continued === "true" || slide.properties.count === "false") {
      els[i - 1].className += ' has-continuation';
    }
  }
  var s = d.createElement("style");
  s.type = "text/css"; s.innerHTML = "@media print { .has-continuation { display: none; } }";
  d.head.appendChild(s);
})(document);
// delete the temporary CSS (for displaying all slides initially) when the user
// starts to view slides
(function() {
  var deleted = false;
  slideshow.on('beforeShowSlide', function(slide) {
    if (deleted) return;
    var sheets = document.styleSheets, node;
    for (var i = 0; i < sheets.length; i++) {
      node = sheets[i].ownerNode;
      if (node.dataset["target"] !== "print-only") continue;
      node.parentNode.removeChild(node);
    }
    deleted = true;
  });
})();
(function() {
  "use strict"
  // Replace <script> tags in slides area to make them executable
  var scripts = document.querySelectorAll(
    '.remark-slides-area .remark-slide-container script'
  );
  if (!scripts.length) return;
  for (var i = 0; i < scripts.length; i++) {
    var s = document.createElement('script');
    var code = document.createTextNode(scripts[i].textContent);
    s.appendChild(code);
    var scriptAttrs = scripts[i].attributes;
    for (var j = 0; j < scriptAttrs.length; j++) {
      s.setAttribute(scriptAttrs[j].name, scriptAttrs[j].value);
    }
    scripts[i].parentElement.replaceChild(s, scripts[i]);
  }
})();
(function() {
  var links = document.getElementsByTagName('a');
  for (var i = 0; i < links.length; i++) {
    if (/^(https?:)?\/\//.test(links[i].getAttribute('href'))) {
      links[i].target = '_blank';
    }
  }
})();
// adds .remark-code-has-line-highlighted class to <pre> parent elements
// of code chunks containing highlighted lines with class .remark-code-line-highlighted
(function(d) {
  const hlines = d.querySelectorAll('.remark-code-line-highlighted');
  const preParents = [];
  const findPreParent = function(line, p = 0) {
    if (p > 1) return null; // traverse up no further than grandparent
    const el = line.parentElement;
    return el.tagName === "PRE" ? el : findPreParent(el, ++p);
  };

  for (let line of hlines) {
    let pre = findPreParent(line);
    if (pre && !preParents.includes(pre)) preParents.push(pre);
  }
  preParents.forEach(p => p.classList.add("remark-code-has-line-highlighted"));
})(document);</script>

<script>
slideshow._releaseMath = function(el) {
  var i, text, code, codes = el.getElementsByTagName('code');
  for (i = 0; i < codes.length;) {
    code = codes[i];
    if (code.parentNode.tagName !== 'PRE' && code.childElementCount === 0) {
      text = code.textContent;
      if (/^\\\((.|\s)+\\\)$/.test(text) || /^\\\[(.|\s)+\\\]$/.test(text) ||
          /^\$\$(.|\s)+\$\$$/.test(text) ||
          /^\\begin\{([^}]+)\}(.|\s)+\\end\{[^}]+\}$/.test(text)) {
        code.outerHTML = code.innerHTML;  // remove <code></code>
        continue;
      }
    }
    i++;
  }
};
slideshow._releaseMath(document);
</script>
<!-- dynamically load mathjax for compatibility with self-contained -->
<script>
(function () {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src  = 'https://mathjax.rstudio.com/latest/MathJax.js?config=TeX-MML-AM_CHTML';
  if (location.protocol !== 'file:' && /^https?:/.test(script.src))
    script.src  = script.src.replace(/^https?:/, '');
  document.getElementsByTagName('head')[0].appendChild(script);
})();
</script>
  </body>
</html>
