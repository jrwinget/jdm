---
title: "Systems of Reasoning & Bounded Rationality"
author: "PSYC/PHIL 279, Winget"
output:
  xaringan::moon_reader:
    css: ["default", "default-fonts", "my-style.css", "metropolis-fonts"]
    lib_dir: libs
    nature:
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: false
---
#  Homo Economicus (Economic Man) 

+ Economic person:
  1. The idea that people have complete information about the options available for choice
  1. Perfect foresight of the consequences of choosing those options
  1. And the ability to solve a decision problem (often quite complex) that identifies the best possible option

<br>
.center[<img src="assets/img/image1.png" width=700>]

???

+ The idea that people are perfectly rational agents who are capable of crunching numbers to find the best or optimal solution has been around for a long time
+ John Stuart Mill (1844) proposed people were self-interested individuals who seek to maximize personal utility
  + He called this notion homo economicus...or the economic man
  + Keep in mind this was the 19th century, so the language is a bit sexist
  + List the bullet points on slide
+ Since then, the idea of the economic person has become a foundation of economic thinking about judgment and decision making
+ We will talk in more detail about this idea when we get to expected utility theory
  + What we are going to concentrate on today is how we deviate from this model and how it fails to accurately capture the reality we face when making judgments and decisions

---
#  Bounded rationality 

“Broadly stated, the task is to replace the global rationality of economic man with the kind of rational behavior that is compatible with the access to information and the computational capacities that are actually possessed by organisms, including man, in the kinds of environments in which such organisms exist.” 
.right[*--Simon, 1955*]

???

+ One of the most vocal critics of the economic person was Herbert Simon...who we briefly talked about last week
+ Simon introduced the term ‘bounded rationality’ (Simon 1957: 198) as a shorthand for his stance against this traditional view of economics
+ He said...(read quote to class)
+ So, he was calling for the replacement of the perfect rationality assumptions of "homo economicus" with a definition of rationality that actually matched people's cognitive limitations
+ And this essentially laid the foundation for the notion of bounded rationality
+ ...which has since come to refer to a wide range of descriptive, normative, and prescriptive accounts of effective behavior that depart from the assumptions of perfect rationality

---
#  Bounded rationality 

+ Simon argued that people do **NOT** have:
  1. Complete information about the options available when making decisions
  1. Perfect foresight into the consequences of choosing one option over another
  1. And, the computational ability and time to solve many problems (especially those that are complex)

???

+ When Simon introduced the term bounded rationality, he was really targeting the 3 assumptions of the economic person
+ So, the first point of bounded rationality is that people do not have complete information about the options available when making a decision
+ The second point is that people do not have perfect foresight into the consequences of each choice
  + Even in the best cases when we think we know what the future will be likely for a particular choice, rarely does that foresight match with the reality of the decision
+ The final point is that people need time and cognitive resources to solve problems (and the more complex they are, the more time/resources people need)

---
#  Satisficing 

+ Satisficing is the strategy of considering the options available to you for choice until you find one that is “good enough”

<br>
.center[<img src="assets/img/image4.png" width=500>]

???

+ Based on these principles, Herbert Simon introduced the term satisficing
+ Satisficing is a decision strategy in which the decision maker considers choice options until they find the one that is good enough
  + The word itself is a blend of the words "satisfy" and "suffice" because that's what people are essentially doing with this strategy
  + People examine choice alternatives until a practical solution with an adequate level of acceptability is found (one that satisfies and/or suffices)

---
#  Bounded rationality 

+ According to Simon (1955) people must operate within the constraints imposed by both their cognitive resources and the task environment
+ As the demands on limited cognitive resources increase, people may employ methods or strategies that reduce the effort they expend on computation
+ These method or strategies are called heuristics, and we will discuss them in detail the next several classes

???

+ Different environments place different limitations on people's cognitive resources
+ Less cognitive resources = less effortful thinking
  + People start using more heuristics

---
#  Systems of reasoning 

.center[<img src="assets/img/image6.png" height=450>]

???

+ Much of the work that Simon did laid the groundwork for what would be later called judgment and decision making
+ In some ways, it also formed the basis for what would later be called systems of reasoning or thinking
  + These models propose that people think in two ways, one faster and one slower
+ In what follows we will talk about the nature and characteristics of the two systems, some experiments that demonstrate the operation of the two systems, and some criticisms of the dual-process approach

---
#  Dual-process models 

+ Dual-process models formally emerged in the 1970s, but the idea that people think in two ways has a long history going back centuries
+ These models suggest people may think in two distinct ways
+ Such models have been applied to a wide variety of topics, including reasoning, person perception, stereotyping, persuasion, judgment and decision making, consumer choice, and much more

???

+ 

---
#  Dual-process models 

+ Labels attached to dual processes

```{r tbl1, echo = FALSE}
tbl1 <- tibble::tribble(
~`Reference`, ~`System 1`, ~`System 2`,
"Schneider & Shiffrin (1977)","Automatic","Controlled",
"Epstein (1994)","Experiential","Rational",
"Chaiken (1980)","Heuristic","Systematic",
"Strack & Deutsch (2004)","Impulsive","Reflective",
"Sloman (1996)","Associative","Rule Based",
"Hammond (1996)","Intuitive","Analytic",
"Evans (2006)","Heuristic","Analytic"
)

kableExtra::kable_styling(knitr::kable(tbl1), font_size = 22)
```

???

+ Over the years, there have been many models developed by different researchers
+ They all seem to use different labels for these two ways of thinking
+ Although as you will see, there is quite a bit of similarity in the labels applied to the two systems of thinking

---
#  Dual-process models (Evans, 2008) 

+ Attributes associated with dual processes

```{r tbl2, echo = FALSE}
tbl2 <- tibble::tribble(
~`System 1`, ~`System 2`,
"Unconscious","Conscious",
"Automatic","Controlled",
"Fast","Slow",
"Associative","Rule Based",
"Parallel (simultaneous)","Serial (sequential)",
"Intuitive","Reflective",
"Low Effort","High Effort"
)

kableExtra::kable_styling(knitr::kable(tbl2), font_size = 22)
```

???

+ The table lists the attributes typically associated with dual-process models
+ The list is not exhaustive and it is not universally agreed upon in the research community
+ Walk through each attribute and provide a few examples so students get a sense of what each one is like
  + Driving example
  + 2 + 2 vs. 5896 + 27941

---
#  Dual-process models (Evans, 2008) 

+ The systems are also thought to have evolved at different times in our history

<br>

```{r tbl3, echo = FALSE}
tbl3 <- tibble::tribble(
~`System 1`, ~`System 2`,
"Evolved early","Evolved late",
"Similar to animal cognition","Distinctively human",
"Implicit knowledge","Explicit Knowledge"
)

kableExtra::kable_styling(knitr::kable(tbl3), font_size = 22)
```

???

+ Discuss evolution perspective
  + Criticisms about the animal vs. human distinction (humans are animals)
  + Plus, some evidence shows animals are able to deliberate and use more System 2 processing
    + e.g., after learning their beaks can't reach food in small places, crows will use sticks as spears
    + e.g., chimps build fishing tools out of twigs to extract termites where hands cannot reach, and hammers from rocks to crack open nuts
    + Unlikely that these were developed using System 1 processing
      + Possible it becomes automatic after learning from parents for example...much like driving for humans

---
#  Interactions between the systems 

+ There are different ways to think about how the two systems interact, but the most common is that System 2 edits the responses of System 1

<br>
.center[<img src="assets/img/image3.png" width=550>]

???

+ Depending on the specific dual-process model, the two systems are argued to interact in different ways
+ But the most common is that System 1 produces a response and then System 2 edits that response
  + The fast System 1 processes produce an initial, intuitive judgment
  + This judgment must be endorsed by System 2 before a response is provided, which is often on autopilot
  + However, high-effort deliberative processing may be applied to inhibit or correct the System 1 response 

---
#  Mindlessness (Langer, 1989) 

+ We are often on autopilot, letting System 1 do most of the work
+ Studies by Langer examined what she called mindlessness, or navigating our daily lives without much attention

<br>
.center[<img src="assets/img/image7.jpeg" width=450>]

???

+ Some of the research on mindlessness echos this idea that, for the most part, we go through life on autopilot and let System 1 do most of the work
+ Langer described mindlessness as the navigation of our daily lives without much conscious attention

---
#  Mindlessness (Langer, 1989) 

+ The study took place in a library at the copier
+ Participants were approached just before they placed money in the machine
+ The experimenter asked the person to use the machine first to copy 5 pages
+ The request was made in one of three ways

<br>
<br>
.center[<img src="assets/img/image8.jpeg" width=375>]

???

+ One of the most famous studies by Langer took place in a library at the copier
+ Participants....
+ Ways of requesting on next slide!

---
#  Mindlessness (Langer, Blank, & Chanowitz, 1978)  

.pull-left[.full-width[
+ **Request only**
    + “Excuse me, I have 5 pages. May I use the Xerox machine?”
+ **Placebic information**
    + “...May I use the machine, because I have to make copies?”
+ **Real information**
    + “...May I use the Xerox machine, because I’m in a rush?”
]]

.pull-right[
```{r gph1, echo=FALSE, fig.height=9, fig.width=8}
library(ggplot2)
library(magrittr)

palette <- c("#E69F00", "#56B4E9", "#009E73")
`Request type` <- c("No info", "Placebic info", "Real info")
`Perc said yes` <- c(60, 93, 94)
gph1 <- data.frame(`Request type`, `Perc said yes`)

gph1 %>% 
  ggplot(aes(`Request type`, `Perc said yes`, fill = `Request type`)) +
  scale_fill_manual(values = palette) +
  geom_col() +
  expand_limits(y = c(0, 100)) + 
  labs( 
    x = "",
    y = "") +
  theme_minimal() +
  theme(legend.position = "none",
        axis.text = element_text(size = 24))
```
]

???

+ Explain conditions and graph

---
#  Cognitive reflection test 

1. A bat and a ball cost $1.10 in total. The bat costs $1.00 more than the ball. How much does the ball cost? 
  + _____ cents
1. If it takes 5 machines 5 minutes to make 5 widgets, how long would it take  100 machines to make 100 widgets? 
  + _____ minutes
1. In a lake, there is a patch of lily pads. Every day, the patch doubles in size. If it takes 48 days for the patch to cover the entire lake, how long would it take for the patch to cover half of the lake? 
  + _____ days

???

+ Take a moment to answer these questions
+ Does anyone want to give any of these a shot?
+ These 3 questions make up a test called the CRT
+ Each question has an intuitive answer that comes to mind, but this intuitive answer is also incorrect
  + The intuitive answer is provided by System 1
  + But the correct answer requires the application of System 2
  + In order to produce the correct response, one must reflect on their own cognitions, hence the name of the test
+ The intuitive incorrect answers are 10, 100, and 24
+ The correct answers are 5 cents, 5 minutes, and 47 days

---
#  “Belief-bias” effect 

+ In this research, participants are instructed to assess the logical validity of arguments
+ The conclusions are a priori believable or unbelievable, and the arguments are either valid or invalid
+ This sets up a conflict between prior beliefs and logical reasoning

.center[<img src="assets/img/image9.jpeg" width=550>]

???

+ A similar line of research examines what's called the belief-bias effect, which is people's tendency to judge the strength of an argument by their belief in the plausibility of the conclusion rather than the actual support of the conclusion
+ A person is more likely to accept an argument that supports a conclusion if it aligns with that person's beliefs
+ In a typical experiment, people are asked to asses the logical validity of an argument
  + The conclusions of the argument are either believable or unbelievable, and the argument itself is either logically valid or invalid
  + Creates 4 possible situations
  + Which sets up a conflict between prior beliefs and logical reasoning

---
#  “Belief-bias” effect 

.center[<img src="assets/img/image5.png" width=600>]

???

+ Here are some examples
  + Typically, syllogisms are presented for evaluation, which fall into one of the four following categories
  + Explain categories
+ Create a conflict between responses based upon a process of logical reasoning and those derived from prior belief about the truth of conclusions
+ One of the key methods for demonstrating dual processes in reasoning tasks
  + System 1 leads to belief bias in conflict situations
  + System 2 leads to valid logical conclusion in conflict situations

---
#  “Belief-bias” effect: % conclusions accepted 

.center[
```{r gph2, echo=FALSE}

valid <- c("Valid", "Invalid", "Valid", "Invalid")
belive <- c("Believable", "Unbelievable", "Unbelievable", "Believable")
accept <- c(90, 10, 55, 70)
gph2 <- data.frame(valid, belive, accept)

gph2 %>% 
  ggplot(aes(valid, accept, fill= belive)) +
  scale_fill_manual(values = palette) +
  geom_col(position = "dodge") +
  expand_limits(y = c(0, 100)) + 
  labs( 
    x = "",
    y = "") +
  theme_minimal() +
  labs(fill = "Believability") +
  theme(axis.text = element_text(size = 24),
        legend.title = element_text(size = 20),
        legend.text = element_text(size = 16))
```
]

???

+ Basically, people should accept only valid conclusions and reject invalid conclusions; but, they don’t
  + They accept believable invalid conclusions
+ In belief-bias experiments, participants are instructed to treat the problem as a logical reasoning task and to endorse only conclusions that necessarily follow from the premises given
+ In spite of this, intelligent adult populations (undergraduate students) are consistently influenced by the prior believability of the conclusion given as well as by the validity of the arguments presented
+ The conclusion endorsement rates from the study of Evans et al. (2003) are shown here
+ It is clear that participants are substantially influenced by both the logic of the argument and believability of its conclusion, with more belief-bias on invalid arguments
+ Dual-process accounts propose that although participants attempt to reason logically in accord with the instructions, the influence of prior beliefs is extremely difficult to suppress and effectively competes for control of the responses made

---
#  Challenges to dual-process models 

+ Dual-process models offer multiple and vague definitions of the attributes of these models
+ Researchers do often mean quite different things when they use the same terms
+ They all carry some baggage that other researchers would disagree with
+ This is mostly a problem when trying to seek similarities across the different models, not necessarily when looking at any one model

???

+ One major criticism is the different labels that are used to describe similar processes
  + Many times the label definitions are vague, which leads to confusion
+ This is especially true when comparing different dual-process models to one another
  + Dual-process theorists often have very different things in mind when talking about their own versus other’s models
+ Also, each model carries some baggage other researchers disagree with
+ Baggage is something like how the two systems interact
  + e.g., are they independent, what are the core features of each system, etc.

---
#  Challenges to dual-process models 

+ Another problem is that the features that make up the cluster for the different systems are not always observed together
  + What features are the defining features of each system? 
  + How correlated should they be with each other?

```{r tbl4, echo=FALSE}
kableExtra::kable_styling(knitr::kable(tbl2), font_size = 22)
```

???

+ Walk through with examples from each system
+ For example, should Unconsciousness be a defining feature of System 1 (I can still explain the 2 + 2 association)
  + How many of these features should correlate strongly with each other to say they are part of System 1 vs. System 2

---
#  Challenges to dual-process models 

+ Dual-process models offer discrete types of thinking
+ Some have argued that there is a continuum of processing styles, not two discrete types
+ Others have argued that single-process models better explain how the mind works

<br>
.pull-left[<img src="assets/img/image10.png" height=275>]
.pull-right[<img src="assets/img/image11.png" height=275>]

???

+ Continuum is like a gradient from System 1 to System 2
+ Single-process models
  + Decision-makers employ one single decision making mechanism 
  + e.g., one SPM is the evidence accumulation model
  + Here, decision-makers continually sample new evidence/information until one choice option surpasses a certain threshold of preference
    + Once this happens, a choice is made in favor of this option

---
#  Summary 

+ Because we often have limited information, cognitive capacity, and time, we make judgments and decisions via satisficing
+ Dual-process models propose that we possess two ways of thinking, one fast and one slow
+ Dual-process models have been fruitfully applied to a variety of important domains
+ However, they are not without their critics

???

+ Summarize slide
