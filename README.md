# PSYC 280: Judgment and Decision Making

This repository contains lecture materials and syllabi for the Judgment and Decision Making course taught by Jeremy R. Winget, as part of the psychology major undergraduate curriculum at LUC.
